<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\commands;

use yii\authclient\clients\Facebook;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class HelloController extends Controller {

	/**
	 * This command echoes what you have entered as the message.
	 *
	 * @param string $message the message to be echoed.
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
			'auth'    => [
				'class'           => 'yii\authclient\AuthAction',
				'successCallback' => [
					$this,
					'successCallback',
				],
			],
		];
	}

	public function successCallback($client) {
		$accessToken           = new Facebook();
		$access_token          = $accessToken->accessToken->token;
		$access_token_duration = date("Y-m-d H:i:s", $accessToken->accessToken->createTimestamp);;
		$attributes = $client->getUserAttributes();
		// user login or signup comes here
		/*
		Checking facebook email registered yet?
		Maxsure your registered email when login same with facebook email
		die(print_r($attributes));
		*/
	}

	public function actionIndex($message = 'hello world') {
		$facebook = new Facebook();
		$facebook->getAccessToken();
//		'http://fb.api.com/index.php?r=site%2Fauth&authclient=facebook';
		//		echo "abc";
	}
}
