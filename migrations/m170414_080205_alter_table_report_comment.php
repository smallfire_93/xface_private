<?php
use yii\db\Migration;

class m170414_080205_alter_table_report_comment extends Migration {

	public function safeUp() {
		$this->dropColumn('report_comment', 'post_id');
		$this->addColumn('report_comment', 'type', 'tinyint DEFAULT NULL');
		$this->addCommentOnColumn('report_comment', 'type', '0: all, 1: cmt cha,2:cmt con');
	}

	public function safeDown() {
		echo "m170414_080205_alter_table_report_comment cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170414_080205_alter_table_report_comment cannot be reverted.\n";

		return false;
	}
	*/
}
