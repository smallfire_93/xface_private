<?php
use yii\db\Migration;

class m170403_094514_alter_product extends Migration {

	public function safeUp() {
		$this->alterColumn('product', 'size', $this->string('30'));
	}

	public function safeDown() {
		echo "m170403_094514_alter_product cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170403_094514_alter_product cannot be reverted.\n";

		return false;
	}
	*/
}
