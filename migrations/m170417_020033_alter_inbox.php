<?php
use yii\db\Migration;

class m170417_020033_alter_inbox extends Migration {

	public function safeUp() {
		$this->alterColumn('inbox', 'id', $this->integer(20).' NOT NULL AUTO_INCREMENT');
		$this->alterColumn('comment', 'id', $this->integer(20).' NOT NULL AUTO_INCREMENT');
	}

	public function safeDown() {
		echo "m170417_020033_alter_inbox cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170417_020033_alter_inbox cannot be reverted.\n";

		return false;
	}
	*/
}
