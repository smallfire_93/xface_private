<?php
use yii\db\Migration;

class m170328_095000_alter_fb_page_id extends Migration {

	public function safeUp() {
		$this->alterColumn('fanpage','fb_page_id',$this->string(120));
		$this->alterColumn('post','fb_post_id',$this->string(120));
		$this->alterColumn('comment','fb_comment_id',$this->string(120));
		$this->alterColumn('comment','fb_user_id',$this->string(120));
		$this->alterColumn('inbox','fb_user_id',$this->string(120));
		$this->alterColumn('inbox','fb_inbox_id',$this->string(120));
		$this->alterColumn('conversation','fb_user_id',$this->string(120));
		$this->alterColumn('customer','fb_user_id',$this->string(120));
		$this->alterColumn('order','fb_user_id',$this->string(120));
		$this->alterColumn('conversation','referen_id',$this->string(120));
		$this->alterColumn('page_config','fb_post_id',$this->string(120));
    }

	public function safeDown() {
		echo "m170328_095000_alter_fb_page_id cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170328_095000_alter_fb_page_id cannot be reverted.\n";

		return false;
	}
	*/
}
