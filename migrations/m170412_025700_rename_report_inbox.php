<?php

use yii\db\Migration;

class m170412_025700_rename_report_inbox extends Migration
{
    public function up()
    {
        $this->renameColumn('report_inbox', 'total_comment', 'total_inbox');
        $this->addColumn('report_inbox','start_time',$this->dateTime()->null());
        $this->addColumn('report_inbox','end_time',$this->dateTime()->null());
    }

    public function down()
    {
        echo "m170412_025700_rename_report_inbox cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
