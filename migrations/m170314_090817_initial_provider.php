<?php
use yii\db\Migration;

class m170314_090817_initial_provider extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('provider', [
			'id'           => $this->primaryKey(),
			'name'         => $this->string(100)->notNull(),
			'address'      => $this->string(120)->null(),
			'phone'        => $this->string(120)->null(),
			'website'      => $this->string(120)->null(),
			'email'        => $this->string(120)->null(),
			'status'       => $this->smallInteger(1)->null()->defaultValue(0),
			'created_date' => $this->timestamp()->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170314_090817_initial_provider cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
