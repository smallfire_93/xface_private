<?php
use yii\db\Migration;

class m170417_092253_add_colums_to_order extends Migration {

	public function safeUp() {
		$this->addColumn('order', 'company_id', $this->integer());
	}

	public function safeDown() {
		echo "m170417_092253_add_colums_to_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170417_092253_add_colums_to_order cannot be reverted.\n";

		return false;
	}
	*/
}
