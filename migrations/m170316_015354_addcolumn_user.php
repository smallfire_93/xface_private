<?php

use yii\db\Migration;

class m170316_015354_addcolumn_user extends Migration
{
    public function up()
    {
        $this->addColumn('user','address',$this->string()->null());
    }

    public function down()
    {
        echo "m170316_015354_addcolumn_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
