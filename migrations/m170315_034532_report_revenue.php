<?php

use yii\db\Migration;

class m170315_034532_report_revenue extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('report_revenue',[
            'id' =>  $this->primaryKey(),
            'created_date'    =>  $this->dateTime()->null(),
            'total_customer'    =>  $this->integer()->null()->defaultValue(0),
            'total_order_finish' =>  $this->integer()->null()->defaultValue(0),
            'total_order_pending'    =>  $this->integer()->null()->defaultValue(0),
            'total_order_feedback'    =>  $this->integer()->null()->defaultValue(0),
            'total_order_cancel'    =>  $this->integer()->null()->defaultValue(0),
            'total_money'    =>  $this->integer()->null()->defaultValue(0),
            'fanpage_id'    =>  $this->integer()->null(),
            'company_id'    =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_032008_initial_report_order_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
