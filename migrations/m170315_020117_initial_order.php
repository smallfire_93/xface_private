<?php

use yii\db\Migration;

class m170315_020117_initial_order extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('order',[
            'id'    =>  $this->primaryKey(),
            'code'  =>  $this->string(30)->null(),
            'user_id_create'    =>  $this->integer()->null(),
            'user_id_process'   =>  $this->integer()->null(),
            'quantity'  =>  $this->integer()->null(),
            'total_money'   =>  $this->integer()->null(),
            'customer_id'   =>  $this->integer()->null(),
            'payment_status'    =>  $this->integer()->null(),
            'shipping_status'   =>  $this->integer()->null(),
            'payment_id'    => $this->integer()->null(),
            'customer_note' =>  $this->string()->null(),
            'fb_user_id'    =>  $this->integer()->null(),
            'tracking'  =>  $this->string(50)->null(),
            'export_date'   =>  $this->dateTime()->null(),
            'receiver_date'  =>  $this->dateTime()->null(),
            'shipping_fee'  =>  $this->integer()->null(),
            'code_fee'   =>  $this->integer()->null(),
            'address'   =>  $this->string(120) ->null(),
            'phone' =>  $this->string(20)->null(),
            'transport_id'  =>  $this->integer()->null(),
            'discount'  =>  $this->integer()->null(),
            'status'    =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_020117_initial_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
