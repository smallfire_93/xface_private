<?php
use yii\db\Migration;

class m170315_022212_initial_page_config extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('page_config', [
			'id'                          => $this->primaryKey(),
			'fanpage_id'                  => $this->integer()->notNull(),
			'fb_post_id'                  => $this->integer()->null(),
			'auto_reply_comment'          => $this->smallInteger(1)->defaultValue(0)->null(),
			'auto_like_comment'           => $this->smallInteger(1)->defaultValue(0)->null(),
			'auto_hidden_comment_all'     => $this->smallInteger(1)->defaultValue(0)->null(),
			'auto_hidden_comment_phone'   => $this->smallInteger(1)->defaultValue(0)->null(),
			'auto_hidden_comment_keyword' => $this->smallInteger(1)->defaultValue(0)->null(),
			'auto_reply_inbox'            => $this->smallInteger(1)->defaultValue(0)->null(),
			'comment_keyword'             => $this->string(1024)->null(),
			'reply_inbox_content'         => $this->string(1024)->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170315_022212_initial_page_config cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
