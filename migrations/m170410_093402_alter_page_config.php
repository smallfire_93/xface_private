<?php
use yii\db\Migration;

class m170410_093402_alter_page_config extends Migration {

	public function safeUp() {
//		$this->renameColumn('page_config', 'fb_post_id', 'post_id');
		$this->dropColumn('page_config', 'start_time');
		$this->dropColumn('page_config', 'end_time');
//		$this->dropColumn('page_config', 'comment_content');
	}

	public function safeDown() {
		echo "m170410_093402_alter_page_config cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170410_093402_alter_page_config cannot be reverted.\n";

		return false;
	}
	*/
}
