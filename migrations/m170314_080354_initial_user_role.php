<?php
use yii\db\Migration;

class m170314_080354_initial_user_role extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->alterColumn('user', 'fb_id', $this->string(120)->null());
		$this->createTable('user_role', [
			'id'            => $this->primaryKey(),
			'user_id'       => $this->integer()->notNull(),
			'permissions'   => $this->text()->notNull(),
			'allow_comment' => $this->smallInteger(1)->notNull()->defaultValue(0),
			'allow_inbox'   => $this->smallInteger(1)->notNull()->defaultValue(0),
		], $tableOptions);
	}

	public function down() {
		echo "m170314_080354_initial_user_role cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
