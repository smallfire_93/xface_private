<?php

use yii\db\Migration;

class m170314_074743_initial_company extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->unique(),
            'address' => $this->string(120)->null(),
            'status'    => $this->smallInteger(1)->notNull()->defaultValue(0),
            'fd_id'     => $this->string(255)->null(),
            'fb_token'  => $this->string(255)->null(),
            'fb_token_expire'    => $this->string(255)->notNull(),
            'user_id'   => $this->integer()->null(),
            'instagram_id'   => $this->string(255)->null(),
            'instagram_token'   => $this->string(255)->null(),
            'instagram_token_expire'    => $this->string(255)->null()

        ], $tableOptions);
    }

    public function down()
    {
        echo "m170314_074743_initial_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
