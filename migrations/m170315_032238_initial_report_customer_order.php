<?php
use yii\db\Migration;

class m170315_032238_initial_report_customer_order extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('report_customer_order', [
			'id'                   => $this->primaryKey(),
			'created_date'         => $this->timestamp()->null(),
			'customer_id'          => $this->integer()->notNull(),
			'customer_name'        => $this->string(120)->notNull(),
			'total_order'          => $this->integer()->defaultValue(0),
			'total_order_pending'  => $this->integer()->defaultValue(0),
			'total_order_finish'   => $this->integer()->defaultValue(0),
			'total_order_feedback' => $this->integer()->defaultValue(0),
			'total_order_cancel'   => $this->integer()->defaultValue(0),
			'fanpage_id'           => $this->integer()->notNull(),
			'company_id'           => $this->integer()->notNull(),
		], $tableOptions);
	}

	public function down() {
		echo "m170315_032238_initial_report_customer_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
