<?php

use yii\db\Migration;

class m170315_071043_initial_alter_table extends Migration
{
    public function up()
    {
        $this->alterColumn('company', 'user_id', $this->integer()->notNull());
        $this->alterColumn('fanpage', 'company_id', $this->integer()->notNull());
        $this->alterColumn('user_fanpage', 'user_id', $this->integer()->notNull());
        $this->alterColumn('user_group', 'user_id', $this->integer()->notNull());
        $this->alterColumn('user_group', 'group_id', $this->integer()->notNull());
        $this->alterColumn('category', 'fanpage_id', $this->integer()->notNull());
        $this->alterColumn('category', 'company_id', $this->integer()->notNull());
        $this->alterColumn('product', 'category_id', $this->integer()->notNull());
        $this->alterColumn('product', 'provider_id', $this->integer()->notNull());
        $this->alterColumn('product', 'provider_id', $this->integer()->notNull());
        $this->alterColumn('product_fanpage', 'product_id', $this->integer()->notNull());
        $this->alterColumn('product_fanpage', 'fanpage_id', $this->integer()->notNull());
        $this->alterColumn('product_post', 'product_id', $this->integer()->notNull());
        $this->alterColumn('product_post', 'post_id', $this->integer()->notNull());
        $this->alterColumn('customer', 'company_id', $this->integer()->notNull());
        $this->alterColumn('order', 'customer_id', $this->integer()->notNull());
        $this->alterColumn('order', 'transport_id', $this->integer()->notNull());
        $this->alterColumn('promotion', 'product_id', $this->integer()->notNull());
        $this->alterColumn('promotion', 'company_id', $this->integer()->notNull());
        $this->alterColumn('order_detail', 'order_id', $this->integer()->notNull());
        $this->alterColumn('order_detail', 'product_id', $this->integer()->notNull());
        $this->alterColumn('order_detail', 'promotion_id', $this->integer()->notNull());
        $this->alterColumn('order_log', 'order_id', $this->integer()->notNull());
        $this->alterColumn('transport', 'company_id', $this->integer()->notNull());
        $this->alterColumn('report_comment', 'fanpage_id', $this->integer()->notNull());
        $this->alterColumn('report_comment', 'company_id', $this->integer()->notNull());
        $this->alterColumn('report_comment', 'post_id', $this->integer()->notNull());
        $this->alterColumn('report_inbox', 'fanpage_id', $this->integer()->notNull());
        $this->alterColumn('report_inbox', 'company_id', $this->integer()->notNull());
        $this->alterColumn('report_order_post', 'post_id', $this->integer()->notNull());
        $this->alterColumn('report_order_post', 'company_id', $this->integer()->notNull());
        $this->alterColumn('report_order_post', 'fanpage_id', $this->integer()->notNull());
        $this->alterColumn('report_order_product', 'product_id', $this->integer()->notNull());
        $this->alterColumn('report_order_product', 'company_id', $this->integer()->notNull());
        $this->alterColumn('report_order_product', 'fanpage_id', $this->integer()->notNull());
    }

    public function down()
    {
        echo "m170315_071043_initial_alter_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
