<?php
use yii\db\Migration;

class m170314_094340_initial_post_kpi extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('post_kpi', [
			'id'            => $this->primaryKey(),
			'post_id'       => $this->integer()->notNull(),
			'type'          => $this->smallInteger(1)->null(),
			'total_comment' => $this->integer()->null(),
			'total_like'    => $this->integer()->null(),
			'total_share'   => $this->integer()->null(),
			'start_time'    => $this->dateTime()->null(),
			'end_date'      => $this->dateTime()->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170314_094340_initial_post_kpi cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
