<?php

use yii\db\Migration;

class m170418_033513_addcolumnt_report_sale extends Migration
{
    public function up()
    {
        $this->addColumn('report_sale', 'transport_id', $this->integer());
    }

    public function down()
    {
        echo "m170418_033513_addcolumnt_report_sale cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
