<?php

use yii\db\Migration;

class m170314_090035_initial_category extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->alterColumn('group', 'permissions' , $this->text()->null());
        $this->createTable('category',[
            'id'    => $this->primaryKey(),
            'name'  =>  $this->string(120)->null(),
            'parent_id' =>  $this->integer()->null(),
            'description'   =>  $this->string()->null(),
            'status'    =>  $this->smallInteger(1)->defaultValue(0),
            'quantity'  =>  $this->integer()->null(),
            'total_quantity_sale'   =>  $this->integer()->null(),
            'create_date'   =>  $this->dateTime()->null(),
            'fanpage_id'    =>  $this->integer()->null(),
            'company_id'    =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170314_090035_initial_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
