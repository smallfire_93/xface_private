<?php

use yii\db\Migration;

class m170418_030704_addcolumnt_report_shipping extends Migration
{
    public function up()
    {
        $this->addColumn('report_shipping', 'total_pending_feedback', $this->integer());
    }

    public function down()
    {
        echo "m170418_030704_addcolumnt_report_shipping cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
