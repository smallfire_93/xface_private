<?php

use yii\db\Migration;

class m170412_032737_rename_company extends Migration
{
    public function up()
    {
        $this->renameColumn('company', 'fd_id', 'fb_id');
    }

    public function down()
    {
        echo "m170412_032737_rename_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
