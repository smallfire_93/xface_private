<?php
use yii\db\Migration;

class m170315_023209_initial_page_config_reply extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('page_config_reply', [
			'id'            => $this->primaryKey(),
			'fanpage_id'    => $this->integer()->notNull(),
			'type'          => $this->smallInteger(1)->defaultValue(1)->notNull(),
			'keyword'       => $this->string(2000)->null(),
			'reply_content' => $this->string(2000)->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170315_023209_initial_page_config_reply cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
