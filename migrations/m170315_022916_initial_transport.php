<?php

use yii\db\Migration;

class m170315_022916_initial_transport extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('transport',[
           'id' =>  $this->primaryKey(),
            'name'  =>  $this->string(64)->null(),
            'phone' =>  $this->string(30)->null(),
            'status'    =>  $this->integer()->null(),
            'image' =>  $this->string()->null(),
            'website'   =>  $this->string()->null(),
            'username'  =>  $this->string(64)->null(),
            'password'  =>  $this->string(120)->null(),
            'api_url'   =>  $this->string()->null(),
            'token' =>  $this->string()->null(),
            'company_id'    =>  $this->integer()->null(),

        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_022916_initial_transport cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
