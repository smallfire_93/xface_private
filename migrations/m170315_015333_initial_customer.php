<?php

use yii\db\Migration;

class m170315_015333_initial_customer extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('customer',[
            'id'    =>  $this->primaryKey(),
            'full_name' =>  $this->string(50)->null(),
            'phone' =>  $this->integer()->null(),
            'address'   =>  $this->string()->null(),
            'email' =>  $this->string(120)->null(),
            'fb_user_id'    =>  $this->integer()->null(),
            'total_order'   =>  $this->integer()->null(),
            'total_quantity'    =>  $this->integer()->null(),
            'create_date'   =>  $this->dateTime()->null(),
            'company_id'    =>  $this->integer()->null(),
            'is_report' =>  $this->integer()->null(),
            'report_content'    =>  $this->string()->null(),

        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_015333_initial_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
