<?php
use yii\db\Migration;

class m170315_074849_add_fk_all extends Migration {

	public function up() {
		$this->renameColumn('user_fanpage', 'page_id', 'fanpage_id');
		$this->alterColumn('user_fanpage', 'fanpage_id', $this->integer()->notNull());
		$this->addForeignKey('fk_category_company_id', '{{%category}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_category_parent_id', '{{%category}}', 'parent_id', 'category', 'id', 'NO ACTION', 'NO ACTION');
		$this->addForeignKey('fk_category_fanpage_id', '{{%category}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_comment_fanpage_id', '{{%comment}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_comment_post_id', '{{%comment}}', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_company_user_id', '{{%company}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_conversation_user_id', '{{%conversation}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_conversation_fanpage_id', '{{%conversation}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->alterColumn('conversation', 'company_id', $this->integer()->notNull());
		$this->addForeignKey('fk_conversation_company_id', '{{%conversation}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->renameColumn('conversation', 'tag_ids', 'tag_id');
		$this->addForeignKey('fk_customer_company_id', '{{%customer}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_fanpage_company_id', '{{%fanpage}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_inbox_fanpage_id', '{{%inbox}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_order_customer_id', '{{%order}}', 'customer_id', 'customer', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_order_detail_order_id', '{{%order_detail}}', 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_order_detail_product_id', '{{%order_detail}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_order_log_order_id', '{{%order_log}}', 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_page_config_fanpage_id', '{{%page_config}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_page_config_reply_fanpage_id', '{{%page_config_reply}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_post_fanpage_id', '{{%post}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_post_kpi_post_id', '{{%post_kpi}}', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_post_schedule_post_id', '{{%post_schedule}}', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_category_id', '{{%product}}', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');
		$this->alterColumn('product', 'provider_id', $this->integer()->null());
		$this->addForeignKey('fk_product_fanpage_product_id', '{{%product_fanpage}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_fanpage_fanpage_id', '{{%product_fanpage}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_post_product_id', '{{%product_post}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_post_post_id', '{{%product_post}}', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_promotion_company_id', '{{%promotion}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_promotion_created_id', '{{%promotion}}', 'created_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_promotion_page_fanpage_id', '{{%promotion_page}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_promotion_page_promotion_id', '{{%promotion_page}}', 'promotion_id', 'promotion', 'id', 'CASCADE', 'CASCADE');
		$this->addColumn('provider', 'company_id', $this->integer()->notNull());
		$this->addForeignKey('fk_report_comment_fanpage_id', '{{%report_comment}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_comment_company_id', '{{%report_comment}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_customer_company_id', '{{%report_customer}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_customer_fanpage_id', '{{%report_customer}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_customer_order_fanpage_id', '{{%report_customer_order}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_customer_order_company_id', '{{%report_customer_order}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_customer_order_customer_id', '{{%report_customer_order}}', 'customer_id', 'customer', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_inbox_fanpage_id', '{{%report_inbox}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_inbox_company_id', '{{%report_inbox}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_order_post_company_id', '{{%report_order_post}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_order_post_post_id', '{{%report_order_post}}', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_order_product_product_id', '{{%report_order_product}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_order_product_company_id', '{{%report_order_product}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_product_company_id', '{{%report_product}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_product_product_id', '{{%report_product}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_product_fanpage_id', '{{%report_product}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_revenue_fanpage_id', '{{%report_revenue}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_revenue_company_id', '{{%report_revenue}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_sale_company_id', '{{%report_sale}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_sale_user_id', '{{%report_sale}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_shipping_fanpage_id', '{{%report_shipping}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_shipping_company_id', '{{%report_shipping}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_report_shipping_transport_id', '{{%report_shipping}}', 'transport_id', 'transport', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_tag_company_id', '{{%tag}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_transport_company_id', '{{%transport}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_company_id', '{{%user}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_fanpage_user_id', '{{%user_fanpage}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_fanpage_fanpage_id', '{{%user_fanpage}}', 'fanpage_id', 'fanpage', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_group_user_id', '{{%user_group}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_group_group_id', '{{%user_group}}', 'group_id', 'group', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_role_user_id', '{{%user_role}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m170315_074849_add_fk_all cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
