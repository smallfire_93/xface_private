<?php

use yii\db\Migration;

class m170314_092333_initial_product_fanpage extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('product_fanpage',[
            'id' =>  $this->primaryKey(),
            'product_id'    =>  $this->integer()->null(),
            'fanpage_id'    =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170314_092333_initial_product_fanpage cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
