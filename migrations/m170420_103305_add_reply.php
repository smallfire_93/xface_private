<?php
use yii\db\Migration;

class m170420_103305_add_reply extends Migration {

	public function safeUp() {
		$this->addColumn('post', 'reply_content', $this->text());
	}

	public function safeDown() {
		echo "m170420_103305_add_reply cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170420_103305_add_reply cannot be reverted.\n";

		return false;
	}
	*/
}
