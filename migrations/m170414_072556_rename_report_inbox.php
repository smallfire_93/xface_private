<?php

use yii\db\Migration;

class m170414_072556_rename_report_inbox extends Migration
{
    public function up()
    {
        $this->renameColumn('report_inbox','comment_date','inbox_date');
    }

    public function down()
    {
        echo "m170414_072556_rename_report_inbox cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
