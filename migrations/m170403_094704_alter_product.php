<?php
use yii\db\Migration;

class m170403_094704_alter_product extends Migration {

	public function safeUp() {
		$this->alterColumn('product', 'retail_price', $this->float());
		$this->alterColumn('product', 'whole_price', $this->float());
		$this->alterColumn('product', 'weight', $this->float());
		$this->alterColumn('product', 'base_price', $this->float());
	}

	public function safeDown() {
		echo "m170403_094704_alter_product cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170403_094704_alter_product cannot be reverted.\n";

		return false;
	}
	*/
}
