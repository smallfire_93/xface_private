<?php
use yii\db\Migration;

class m170314_024809_initial_user extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%user}}', [
			'id'            => $this->primaryKey(),
			'parent_id'     => $this->integer()->null(),
			'username'      => $this->string(64)->notNull()->unique(),
			'password'      => $this->string(120)->notNull(),
			'company_id'    => $this->integer()->null(),
			'code'          => $this->string(30)->defaultValue(null),
			'full_name'     => $this->string(50)->notNull(),
			'phone'         => $this->string(20)->null(),
			'email'         => $this->string(128)->null(),
			'status'        => $this->smallInteger(1)->notNull()->defaultValue(0),
			'online_status' => $this->smallInteger(1)->notNull()->defaultValue(0),
			'fb_id'         => $this->integer()->null(),
			'instagram_id'  => $this->integer()->null(),
			'last_login'    => $this->dateTime()->null(),
			'created_at'    => $this->dateTime()->null(),
			'updated_at'    => $this->dateTime()->null(),
		], $tableOptions);
		$this->insert('{{%user}}', [
			'id'         => '1',
			'username'   => 'admin',
			'password'   => Yii::$app->security->generatePasswordHash('123456'),
			'full_name'  => 'Admin Manager',
			'phone'      => '123456789',
			'email'      => 'admin@gmail.com',
			'status'     => '1',
			'created_at' => date('Y-m-d H:i:s'),
		]);
	}

	public function down() {
		echo "m170314_024809_initial_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
