<?php

use yii\db\Migration;

class m170331_022247_add_column_page_config_all extends Migration
{
    public function up()
    {
        $this->addColumn('page_config','start_time',$this->dateTime()->null());
        $this->addColumn('page_config','end_time',$this->dateTime()->null());
    }

    public function down()
    {
        echo "m170331_022247_add_column_page_config_all cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
