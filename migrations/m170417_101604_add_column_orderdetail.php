<?php
use yii\db\Migration;

class m170417_101604_add_column_orderdetail extends Migration {

	public function safeUp() {
		$this->alterColumn('order_detail', 'type', 'tinyint NULL DEFAULT "0"');
	}

	public function safeDown() {
		echo "m170417_101604_add_column_orderdetail cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170417_101604_add_column_orderdetail cannot be reverted.\n";

		return false;
	}
	*/
}
