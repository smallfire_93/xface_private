<?php

use yii\db\Migration;

class m170316_043555_addcolumn_all_user extends Migration
{
    public function up()
    {
        $this->addColumn('user','start_time',$this->dateTime()->null());
        $this->addColumn('user','end_time',$this->dateTime()->null());
    }

    public function down()
    {
        echo "m170316_043555_addcolumn_all_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
