<?php

use yii\db\Migration;

class m170412_025239_add_column_report_comment extends Migration
{
    public function up()
    {
        $this->addColumn('report_comment','start_time',$this->dateTime()->null());
        $this->addColumn('report_comment','end_time',$this->dateTime()->null());
    }

    public function down()
    {
        echo "m170412_025239_add_column_report_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
