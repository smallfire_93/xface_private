<?php
use yii\db\Migration;

class m170327_042355_create_category extends Migration {

	public function safeUp() {
		$this->dropForeignKey('fk_category_company_id', 'category');
		$this->dropForeignKey('fk_category_parent_id', 'category');
		$this->dropForeignKey('fk_category_fanpage_id', 'category');
		$this->insert('category', [
			'id'         => 1,
			'name'       => 'Quần áo',
			'parent_id'  => 0,
			'status'     => 1,
			'fanpage_id' => 0,
			'company_id' => 0,
		]);
	}

	public function safeDown() {
		echo "m170327_042355_create_category cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170327_042355_create_category cannot be reverted.\n";

		return false;
	}
	*/
}
