<?php

use yii\db\Migration;

class m170414_072321_drop_columnt_report_inbox extends Migration
{
    public function up()
    {
        $this->dropColumn('report_inbox','end_time');
        $this->renameColumn('report_inbox','start_time','comment_date');
    }

    public function down()
    {
        echo "m170414_072321_drop_columnt_report_inbox cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
