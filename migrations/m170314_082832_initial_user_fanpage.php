<?php

use yii\db\Migration;

class m170314_082832_initial_user_fanpage extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{user_fanpage}}',[
            'id'    =>  $this->primaryKey(),
            'user_id'   =>  $this->integer()->null(),
            'page_id'   =>  $this->integer()->null(),
            'comment_quantity'  =>  $this->integer()->null(),
            'inbox_quantity'    =>  $this->integer()->null(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170314_082832_initial_user_fanpage cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
