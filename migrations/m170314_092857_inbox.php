<?php

use yii\db\Migration;

class m170314_092857_inbox extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{inbox}}',[
            'id' => $this->primaryKey(),
            'fanpage_id'    =>  $this->integer()->null(),
            'user_id_manager'   =>  $this->integer()->null(),
            'user_id_reply'  =>  $this->integer()->null(),
            'fb_user_id'   =>  $this->string(255)->null(),
            'content'   =>  $this->integer()->null(),
            'status'   =>  $this->smallInteger(1)->notNull()->defaultValue(0),
            'created_date'   =>  $this->dateTime()->null(),
            'fb_inbox_id'   =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        //echo "m170314_081958_initial_fanpage cannot be reverted.\n";
        $this->dropTable('{{%inbox}}');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
