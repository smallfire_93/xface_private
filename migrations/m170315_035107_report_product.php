<?php

use yii\db\Migration;

class m170315_035107_report_product extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('report_product',[
            'id' =>  $this->primaryKey(),
            'created_date'    =>  $this->dateTime()->null(),
            'product_id'    =>  $this->integer()->null(),
            'product_name' =>  $this->string(50)->null(),
            'in_stock'    =>  $this->integer()->null(),
            'total_order'    =>  $this->integer()->null()->defaultValue(0),
            'total_order_finish'    =>  $this->integer()->null()->defaultValue(0),
            'total_money'    =>  $this->integer()->null()->defaultValue(0),
            'company_id'    =>  $this->integer()->null(),
            'fanpage_id'    =>  $this->integer()->null(),

        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_032008_initial_report_order_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
