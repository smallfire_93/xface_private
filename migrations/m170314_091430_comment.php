<?php

use yii\db\Migration;

class m170314_091430_comment extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{comment}}',[
            'id' => $this->primaryKey(),
            'post_id'    =>  $this->integer()->null(),
            'fb_comment_id'   =>  $this->integer()->null(),
            'fanpage_id'  =>  $this->integer()->null(),
            'content'   =>  $this->string(255)->null(),
            'status'   =>  $this->smallInteger(1)->notNull()->defaultValue(0),
            'created_date'   =>  $this->dateTime()->null(),
            'parent_id'   =>  $this->integer()->null(),
            'user_id_manager'   =>  $this->integer()->null(),
            'user_id_reply'   =>  $this->integer()->null(),
            'type'   =>  $this->integer()->null(),
            'is_hidden'   => $this->integer()->null(),
            'is_like'   =>  $this->integer()->null(),
            'fb_user_id'   =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        //echo "m170314_081958_initial_fanpage cannot be reverted.\n";
        $this->dropTable('{{%comment}}');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
