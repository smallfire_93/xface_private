<?php

use yii\db\Migration;

class m170328_061149_addcolumn_page_config extends Migration
{
    public function up()
    {
        $this->addColumn('page_config','comment_content',$this->string()->null());
    }

    public function down()
    {
        echo "m170328_061149_addcolumn_page_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
