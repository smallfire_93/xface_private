<?php
use yii\db\Migration;

class m170327_101628_add_column_name_to_fanpage extends Migration {

	public function safeUp() {
		$this->renameColumn('fanpage', 'page_id', 'fb_page_id');
		$this->alterColumn('fanpage', 'fb_page_id', $this->integer(20));
		$this->addColumn('fanpage', 'name', $this->string(1000));
	}

	public function safeDown() {
		echo "m170327_101628_add_column_name_to_fanpage cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170327_101628_add_column_name_to_fanpage cannot be reverted.\n";

		return false;
	}
	*/
}
