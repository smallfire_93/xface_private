<?php

use yii\db\Migration;

class m170314_083810_initial_user_group extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{user_group}}',[
            'id'    =>  $this->primaryKey(),
            'user_id'   =>  $this->integer()->null(),
            'group_id'  =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170314_083810_initial_user_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
