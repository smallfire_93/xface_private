<?php
use yii\db\Migration;

class m170315_041509_alter_table_all extends Migration {

	public function up() {
		$this->alterColumn('report_sale', 'company_id', $this->integer()->notNull());
		$this->alterColumn('report_sale', 'fanpage_id', $this->integer()->notNull());
		$this->alterColumn('report_sale', 'user_id', $this->integer()->notNull());
		$this->alterColumn('report_product', 'company_id', $this->integer()->notNull());
		$this->alterColumn('report_product', 'fanpage_id', $this->integer()->notNull());
		$this->alterColumn('report_product', 'product_id', $this->integer()->notNull());
		$this->alterColumn('report_shipping', 'company_id', $this->integer()->notNull());
		$this->alterColumn('report_shipping', 'fanpage_id', $this->integer()->notNull());
		$this->renameColumn('report_shipping','transpost_id','transport_id');
		$this->alterColumn('report_revenue', 'company_id', $this->integer()->notNull());
		$this->alterColumn('report_revenue', 'fanpage_id', $this->integer()->notNull());
	}

	public function down() {
		echo "m170315_041509_alter_table_all cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
