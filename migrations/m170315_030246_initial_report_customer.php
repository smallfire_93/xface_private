<?php
use yii\db\Migration;

class m170315_030246_initial_report_customer extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('report_customer', [
			'id'                 => $this->primaryKey(),
			'created_date'       => $this->timestamp()->null(),
			'total_customer'     => $this->integer()->defaultValue(0)->null(),
			'total_customer_new' => $this->integer()->defaultValue(0)->null(),
			'total_customer_old' => $this->integer()->defaultValue(0)->null(),
			'total_money'        => $this->integer()->defaultValue(0)->null(),
			'company_id'         => $this->integer()->notNull(),
			'fanpage_id'         => $this->integer()->notNull(),
		], $tableOptions);
	}

	public function down() {
		echo "m170315_030246_initial_report_customer cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
