<?php

use yii\db\Migration;

class m170315_032223_initial_order_product extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('report_order_product', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->null(),
            'company_id' => $this->integer()->null(),
            'total_customer' => $this->integer()->null()->defaultValue(0),
            'total_order' => $this->integer()->null()->defaultValue(0),
            'total_order_pending' => $this->integer()->null()->defaultValue(0),
            'total_order_confirm' => $this->integer()->null()->defaultValue(0),
            'total_order_packing' => $this->integer()->null()->defaultValue(0),
            'total_order_shipping' => $this->integer()->null()->defaultValue(0),
            'total_order_received' => $this->integer()->null()->defaultValue(0),
            'total_order_feedback' => $this->integer()->null()->defaultValue(0),
            'total_order_finish' => $this->integer()->null()->defaultValue(0),
            'total_order_cancel' => $this->integer()->null()->defaultValue(0),
            'created_date' => $this->dateTime()->null(),
            'fanpage_id' => $this->integer()->null(),
            'total_money' => $this->integer()->null()->defaultValue(0),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170315_032223_initial_order_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
