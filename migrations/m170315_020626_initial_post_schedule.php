<?php
use yii\db\Migration;

class m170315_020626_initial_post_schedule extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('post_schedule', [
			'id'        => $this->primaryKey(),
			'post_id'   => $this->integer()->notNull(),
			'post_time' => $this->dateTime()->notNull(),
		], $tableOptions);
	}

	public function down() {
		echo "m170315_020626_initial_post_schedule cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
