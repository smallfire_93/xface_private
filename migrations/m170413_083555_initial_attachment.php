<?php
use yii\db\Migration;

class m170413_083555_initial_attachment extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('attachment', [
			'id'      => $this->primaryKey(20),
			'type'    => 'tinyint NULL DEFAULT "0"',
			'status'    => 'tinyint NULL DEFAULT "0"',
			'process'    => 'tinyint NULL DEFAULT "0"',
			'image'    => $this->text(),
			'video'    => $this->text(),
			'post_id' => $this->bigInteger(20),
			'fb_id' => $this->bigInteger(20),
		], $tableOptions);
	}

	public function safeDown() {
		echo "m170413_083555_initial_attachment cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170413_083555_initial_attachment cannot be reverted.\n";

		return false;
	}
	*/
}
