<?php

use yii\db\Migration;

class m170420_031753_add_columnt_report_shipping extends Migration
{
    public function up()
    {
        $this->addColumn('report_shipping', 'total_not_meet', $this->integer());
    }

    public function down()
    {
        echo "m170420_031753_add_columnt_report_shipping cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
