<?php

use yii\db\Migration;

class m170314_081958_initial_fanpage extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{fanpage}}',[
            'id' => $this->primaryKey(),
            'company_id'    =>  $this->integer()->null(),
            'page_id'   =>  $this->integer()->null(),
            'access_token'  =>  $this->string(255)->null(),
            'expire_date'   =>  $this->dateTime()->null(),
            'is_join'   =>  $this->smallInteger(1)->notNull()->defaultValue(0),
            'create'    =>  $this->dateTime()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170314_081958_initial_fanpage cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
