<?php
use yii\db\Migration;

class m170314_093559_initial_post extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('post', [
			'id'                   => $this->primaryKey(),
			'fb_post_id'           => $this->string(120)->null(),
			'content'              => $this->text()->notNull(),
			'total_comment'        => $this->integer()->null(),
			'total_parent_comment' => $this->integer()->null(),
			'total_like'           => $this->integer()->null(),
			'total_share'          => $this->integer()->null(),
			'created_date'         => $this->timestamp()->null(),
			'fanpage_id'           => $this->integer()->notNull(),
			'total_order'          => $this->integer(),
			'is_auto_comment'      => $this->smallInteger(1)->defaultValue(0)->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170314_093559_initial_post cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
