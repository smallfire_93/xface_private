<?php
use yii\db\Migration;

class m170331_095745_alter_status extends Migration {

	public function safeUp() {
		$this->alterColumn('product', 'status', 'tinyint not null DEFAULT "1"');
	}

	public function safeDown() {
		echo "m170331_095745_alter_status cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170331_095745_alter_status cannot be reverted.\n";

		return false;
	}
	*/
}
