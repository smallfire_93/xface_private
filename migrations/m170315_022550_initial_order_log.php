<?php

use yii\db\Migration;

class m170315_022550_initial_order_log extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('order_log',[
            'id'    =>  $this->primaryKey(),
            'order_id'  =>  $this->integer()->null(),
            'user_id_process'   =>  $this->integer()->null(),
            'create_date'   =>  $this->dateTime()->null(),
            'status'    =>  $this->integer()->null(),
            'note'  =>  $this->string()->null()
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_022550_initial_order_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
