<?php

use yii\db\Migration;

class m170314_091258_initial_product extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('product',[
            'id' =>  $this->primaryKey(),
            'category_id'   => $this->integer()->null(),
            'name'  =>  $this->string(120)->null(),
            'code'  =>  $this->string()->null(),
            'image' =>  $this->string()->null(),
            'base_price'    =>  $this->integer()->null(),
            'retail_price'  =>  $this->integer()->null(),
            'Whole_price'   =>  $this->integer()->null(),
            'quantity'  =>  $this->integer()->null(),
            'quantity_sale' =>  $this->integer()->null(),
            'status'    =>  $this->smallInteger(1)->defaultValue(0),
            'weight'    =>  $this->integer()->null(),
            'size'  =>  $this->integer()->null(),
            'color' =>  $this->string(100)->null(),
            'content'   =>  $this->string()->null(),
            'provider_id'   =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170314_091258_initial_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
