<?php

use yii\db\Migration;

class m170314_093947_conversation extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{conversation}}',[
            'id' => $this->primaryKey(),
            'referen_id'    =>  $this->integer()->null(),
            'type'   =>  $this->integer()->null(),
            'fb_user_id'  =>  $this->integer()->null(),
            'user_id'   =>  $this->integer()->null(),
            'fanpage_id'   =>  $this->integer()->null(),
            'post_id'   =>  $this->smallInteger(1)->notNull()->defaultValue(0),
            'company_id'   =>  $this->dateTime()->null(),
            'status'   =>  $this->smallInteger(1)->notNull()->defaultValue(0),
            'tag_ids'   =>  $this->string(255)->null(),
            'tags'   =>  $this->string(255)->null(),
            'created_date'   =>  $this->dateTime()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        //echo "m170314_081958_initial_fanpage cannot be reverted.\n";
        $this->dropTable('{{%conversation}}');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
