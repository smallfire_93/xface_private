<?php
use yii\db\Migration;

class m170328_100927_alter_id_to extends Migration {

	public function safeUp() {
		$this->alterColumn('comment', 'id', $this->bigInteger(20));
		$this->alterColumn('inbox', 'id', $this->bigInteger(20));
	}

	public function safeDown() {
		echo "m170328_100927_alter_id_to cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170328_100927_alter_id_to cannot be reverted.\n";

		return false;
	}
	*/
}
