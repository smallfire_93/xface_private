<?php

use yii\db\Migration;

class m170314_094002_tag extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{tag}}',[
            'id' => $this->primaryKey(),
            'name'    =>  $this->string(255)->null(),
            'company_id'   =>  $this->integer()->null(),
            'type'  =>  $this->integer()->null(),

        ],$tableOptions);
    }

    public function down()
    {
        //echo "m170314_081958_initial_fanpage cannot be reverted.\n";
        $this->dropTable('{{%tag}}');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
