<?php
use yii\db\Migration;

class m170315_021933_initial_promotion_page extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('promotion_page', [
			'id'           => $this->primaryKey(),
			'promotion_id' => $this->integer()->notNull(),
			'fanpage_id'   => $this->integer()->notNull(),
		], $tableOptions);
	}

	public function down() {
		echo "m170315_021933_initial_promotion_page cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
