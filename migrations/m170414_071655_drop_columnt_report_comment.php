<?php

use yii\db\Migration;

class m170414_071655_drop_columnt_report_comment extends Migration
{
    public function up()
    {
        $this->dropColumn('report_comment','end_time');
        $this->renameColumn('report_comment','start_time','comment_date');
    }

    public function down()
    {
        echo "m170414_071655_drop_columnt_report_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
