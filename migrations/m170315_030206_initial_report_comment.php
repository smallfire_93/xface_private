<?php

use yii\db\Migration;

class m170315_030206_initial_report_comment extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('report_comment',[
           'id' =>  $this->primaryKey(),
            'fanpage_id'    =>  $this->integer()->null(),
            'company_id'    =>  $this->integer()->null(),
            'total_comment' =>  $this->integer()->null()->defaultValue(0),
            'hour_1'    =>  $this->integer()->null(),
            'hour_2'    =>  $this->integer()->null(),
            'hour_3'    =>  $this->integer()->null(),
            'hour_4'    =>  $this->integer()->null(),
            'hour_5'    =>  $this->integer()->null(),
            'hour_6'    =>  $this->integer()->null(),
            'hour_7'    =>  $this->integer()->null(),
            'hour_8'    =>  $this->integer()->null(),
            'hour_9'    =>  $this->integer()->null(),
            'hour_10'    =>  $this->integer()->null(),
            'hour_11'    =>  $this->integer()->null(),
            'hour_12'    =>  $this->integer()->null(),
            'post_id'   =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_030206_initial_report_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
