<?php
use yii\db\Migration;

class m170417_025251_add_column_order extends Migration {

	public function safeUp() {
		$this->addColumn('order', 'update_time', $this->dateTime());
	}

	public function safeDown() {
		echo "m170417_025251_add_column_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170417_025251_add_column_order cannot be reverted.\n";

		return false;
	}
	*/
}
