<?php

use yii\db\Migration;

class m170315_021955_initial_order_detail extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('order_detail',[
           'id' =>  $this->primaryKey(),
            'order_id' =>  $this->integer()->null(),
            'product_id'    =>  $this->integer()->null(),
            'create_date'   =>  $this->dateTime()->null(),
            'price' =>  $this->integer()->null(),
            'quantity'  =>  $this->integer()->null(),
            'total_price'   =>  $this->integer()->null(),
            'promotion_id'  =>  $this->integer()->null(),
            'promotion_amount'  =>  $this->integer()->null(),
            'type'  =>  $this->integer()->null(),
        ],$tableOptions);
    }

    public function down()
    {
        echo "m170315_021955_initial_order_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
