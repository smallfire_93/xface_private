<?php
use yii\db\Migration;

class m170317_023607_rename_product extends Migration {

	public function up() {
		$this->renameColumn('product', 'Whole_price', 'whole_price');
	}

	public function down() {
		echo "m170317_023607_rename_product cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
