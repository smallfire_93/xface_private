<?php
use yii\db\Migration;

class m170315_021244_initial_promotion extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('promotion', [
			'id'=>$this->primaryKey(),
			'name'=>$this->string(120),
			'start_time'=>$this->dateTime()->notNull(),
			'end_time'=>$this->dateTime()->notNull(),
			'promotion_type'=>$this->smallInteger(1)->notNull(),
			'discount_amount'=>$this->integer()->null(),
			'product_id'=>$this->integer()->null(),
			'product_id_promotion'=>$this->integer()->null(),
			'promotion_quantity'=>$this->integer()->null(),
			'product_quantity'=>$this->integer()->null(),
			'created_date'=>$this->timestamp()->null(),
			'created_id'=>$this->integer()->notNull(),
			'company_id'=>$this->integer()->notNull(),
		], $tableOptions);

	}

	public function down() {
		echo "m170315_021244_initial_promotion cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
