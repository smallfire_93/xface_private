<?php
use yii\db\Migration;

class m170417_021239_addcolumn_order extends Migration {

	public function safeUp() {
		$this->addColumn('order', 'post_id', $this->bigInteger());
	}

	public function safeDown() {
		echo "m170417_021239_addcolumn_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170417_021239_addcolumn_order cannot be reverted.\n";

		return false;
	}
	*/
}
