<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Fanpage;
use app\models\OrderDetail;
use app\models\Product;
use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView1()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView2()
    {
        return $this->render('view2');
    }

    public function actionView3()
    {
        return $this->render('view3');
    }

    public function actionView4()
    {
        return $this->render('view4');
    }

    public function actionView5()
    {
        return $this->render('view5');
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	public function actionList() {
		$searchModel  = new OrderSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('list', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = \Yii::$app->session;
        $pageId = $session['fanpage'];
        $fanpage = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        $model = new Order();
        $post = Yii::$app->request->post();
        $post = $post['Order'];
        $fb_user_id = $post['fb_user_id'];
        $customer = Customer::find()->where("fb_user_id = '{$fb_user_id}'")->one();
        if (!$customer) {
            $customer = new Customer();
        }
        $items = $session['cart'];
        $i = 0;
        $totalAmount = 0;
        $totalQuantity = 0;
        foreach ($items as $key => $value) {
            $product = Product::findOne($key);
            if ($product) {
                $product_order[$i] = [
                    'id' => $product->id,
                    'price' => $product->retail_price,
                    'image' => $product->image,
                    'name' => $product->name,
                    'code' => $product->code,
                    'quantity' => $value['quantity'],
                ];
            }
            $i++;
        }
        if (is_array($items)) {
            if (isset($product_order)) {
                foreach ($product_order as $item) {
                    $totalAmount = $totalAmount + ($item['price'] * $item['quantity']);
                    $totalQuantity = $totalQuantity + $item['quantity'];
                }
            }
        }

        $customer->full_name = $post['customer_name'];
        $customer->phone = $post['phone'];
        $customer->address = $post['address'];
        $customer->fb_user_id = $post['fb_user_id'];
        $customer->create_date = date('Y-m-d H:i:s');
        $customer->company_id = Yii::$app->user->identity->company_id;
        $customer->fanpage_id = $fanpage->id;
        if($customer->save()) {
            $model->user_id_create = Yii::$app->user->id;
            $model->quantity = $totalQuantity;
            $model->total_money = $totalAmount;
            $model->customer_id = $customer->id;
            $model->payment_id = $post['payment_id'];
            $model->customer_note = $post['customer_note'];
            $model->fb_user_id = $post['fb_user_id'];
            $model->address = $customer->address;
            $model->phone = $customer->phone;
            $model->discount = $post['discount'];
            $model->status = 1;
            $model->created_date = date('Y-m-d H:i:s');
            $model->fanpage_id = $customer->fanpage_id;
            $model->company_id = $customer->company_id;
            $model->post_id = $post['post_id'];
            $model->shipping_status = 1;
            $model->payment_status = 1;
            if($model->save()) {
                foreach ($items as $key => $value) {
                    $orderDetail = new OrderDetail();
                    $product = Product::findOne($key);
                    if ($product) {
                        $orderDetail->order_id = $model->id;
                        $orderDetail->product_id = $product->id;
                        $orderDetail->create_date = $model->created_date;
                        $orderDetail->price = $product->retail_price;
                        $orderDetail->quantity = $value['quantity'];
                        $orderDetail->total_price = $product->retail_price * $value['quantity'];
                        if($orderDetail->save()){}else{
                            print_r($orderDetail->getErrors());
                        }
                    }
                }
                unset($session['cart']);
            }else{
                print_r($model->getErrors());
            }
        }else{
            print_r($customer->getErrors());
        }
    }

    public function actionAdd()
    {
        if (Yii::$app->request->isAjax) {
            $session = Yii::$app->session;
            $product_id = $_POST['product_id'];
            $cart = $session['cart'];
//            $cart[$product_id] = [
//                'id' => $product_id,
//            ];

            if (isset($cart[$product_id]['quantity']) && $cart[$product_id]['quantity'] != null) {
                $cart[$product_id]['quantity'] += 1;
            } else {
                $cart[$product_id] = [
                    'id' => $product_id,
                    'quantity' => 1,
                ];
            }
            $session['cart'] = $cart;
            print_r($session['cart']);
            Yii::$app->end();
        }
    }

    public function actionLoadCartActive()
    {
        $this->layout = "_blank";
        $items = Yii::$app->session['cart'];
        $number = 0;
        $subTotal = 0;
        $totalAmount = 0;
        if ($items) {
            $product_order = [];
            $i = 0;
            foreach ($items as $key => $value) {
                $product = Product::findOne($key);
                if ($product) {
                    $product_order[$i] = [
                        'id' => $product->id,
                        'price' => $product->retail_price,
                        'image' => $product->image,
                        'name' => $product->name,
                        'code' => $product->code,
                        'quantity' => $value['quantity'],
                    ];
                }
                $i++;
            }
            if (is_array($items)) {
                $number = count($items);
                foreach ($product_order as $item) {
                    $subTotal = $subTotal + $item['price'];
                }
            }
            return $this->render('load-cart-active', [
                'number' => $number,
                'product_order' => $product_order,
                'subtotal' => $subTotal,
                'totalAmount' => $totalAmount,
            ]);
        }
    }

    public function actionTotalMoney()
    {
        $items = Yii::$app->session['cart'];
        $i = 0;
        $totalAmount = 0;
        if(isset($items)) {
            foreach ($items as $key => $value) {
                $product = Product::findOne($key);
                if ($product) {
                    $product_order[$i] = [
                        'id' => $product->id,
                        'price' => $product->retail_price,
                        'image' => $product->image,
                        'name' => $product->name,
                        'code' => $product->code,
                        'quantity' => $value['quantity'],
                    ];
                }
                $i++;
            }
            if (is_array($items)) {
                if (isset($product_order)) {
                    foreach ($product_order as $item) {
                        $totalAmount = $totalAmount + ($item['price'] * $item['quantity']);
                    }
                }
            }
            return number_format($totalAmount, '0', ',', '.') . ' ₫';
        }else{
            return "0 ₫";
        }
    }

    public function actionRemove()
    {
        $id = $_POST['id'];
        $session = Yii::$app->session;
        $items = $session['cart'];
        unset($items[$id]);
        $updateCart = array();
        $i = 0;
        foreach ($items as $item) {
            $updateCart[$i] = $item;
            $i++;
        }
        $session['cart'] = $updateCart;
        print_r($session['cart']);
        Yii::$app->end();
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
