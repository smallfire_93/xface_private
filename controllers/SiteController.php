<?php

namespace app\controllers;

use app\components\Controller;
use app\models\Company;
use Yii;
use yii\authclient\clients\Facebook;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use yii\helpers\Url;

class SiteController extends Controller
{

    public $enableCsrfValidation = false;

    public $successUrl = 'Success';

    //hello

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            //			'verbs'  => [
            //				'class'   => VerbFilter::className(),
            //				'actions' => [
            //					'logout' => ['post'],
            //				],
            //			],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [
                    $this,
                    'successCallback',
                ],
            ],
        ];
    }

    public function successCallback(Facebook $client)
    {
        $accessToken = new Facebook();
        $access_token = $accessToken->accessToken->token;
        $access_token_duration = date("Y-m-d H:i:s", $accessToken->accessToken->createTimestamp);;
        $attributes = $client->getUserAttributes();
        // user login or signup comes here
        /*
        Checking facebook email registered yet?
        Maxsure your registered email when login same with facebook email
        die(print_r($attributes));
        */
        $user = User::find()->where(['email' => $attributes['email']])->one();
        if (!empty($user)) {
            Yii::$app->user->login($user);
            $company = Company::findOne(Yii::$app->user->identity->company_id);
            $company->fb_token = $access_token;
            $company->fb_token_expire = $access_token_duration;
            $company->update();
            return $this->redirect(['site/index']);
        } else {
            $user = new User();
            $user->username = $attributes['email'];
            $user->password = $user->setPassword('123456');
            $user->email = $attributes['email'];
            $user->full_name = $attributes['name'];
            $user->fb_id = $attributes['id'];
            $user->status = 1;
            $user->created_at = date('Y-m-d H:i:s');
            $user->save();
            $company = new Company();
            $company->name = $user->full_name;
            $company->fb_id = $attributes['id'];
            $company->fb_token = $access_token;
            $company->fb_token_expire = $access_token_duration;
            $company->user_id = $user->id;
            $company->status = 1;
            $company->save();
            $user->company_id = $company->id;
            $user->update();
            // Save session attribute user from FB
            $session = Yii::$app->session;
            $session['attributes'] = $attributes;
            Yii::$app->user->login($user);
            // redirect to form signup, variabel global set to successUrl
            $this->successUrl = Url::to(['index']);
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionOrder()
    {
        return $this->render('order');
    }

    public function actionCategory()
    {
        return $this->render('category');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        $this->layout = 'login';
        $model_reg = new User();
        $data = [];
        if ($model_reg->load(\Yii::$app->request->post()) && $model_reg->validate()) {
            $data['email'] = $model_reg->email;
            $data['username'] = $model_reg->username;
            $data['password'] = $model_reg->password;
            $data['password_repeat'] = $model_reg->password_repeat;
            $data['invalid'] = $model_reg->invalid;
            // $data['invalid'] =1;
            var_dump($data);
            die();
            if ($model_reg->save()) {
                ?>
                <script>
                    alert("Đăng nhập thành công");
                </script>
                <?php
            }
        } else {
            return $this->render('view_register', [
                'model_reg' => $model_reg,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $user = User::findOne(Yii::$app->user->identity->id);
        $user->last_login = date('Y-m-d H:i:s');
        $user->save();
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionForgetPassword()
    {
        $this->layout = 'reset';
        return $this->render('forget-password');
    }

    public function actionResetPassword()
    {
        $this->layout = 'reset';
        return $this->render('reset-password');
    }
}
