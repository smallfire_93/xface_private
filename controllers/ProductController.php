<?php
namespace app\controllers;

use app\models\Category;
use app\models\Post;
use app\models\search\ProductSearch;
use app\models\UploadExcel;
use Exception;
use Yii;
use app\models\Product;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Product models.
	 * @return mixed
	 */
	public function actionIndex() {
		$params        = Yii::$app->request->queryParams;
		$searchModel   = new ProductSearch();
		$dataProvider  = $searchModel->search($params);
		$total_product = $searchModel->getInfo($params, 'count');
		$total_post    = $searchModel->getInfo($params, 'total_post');
		$total_money   = $searchModel->getInfo($params, 'total_money');
		$total_sale    = $searchModel->getInfo($params, 'total_sale');
		$model         = new UploadExcel();
		if(isset($_POST['UploadExcel']['excel'])) {
			$model->excel = UploadedFile::getInstance($model, 'excel');
			$input        = $model->uploadExcel();
			try {
				$inputFileType  = \PHPExcel_IOFactory::identify($input);
				$objectReader   = \PHPExcel_IOFactory::createReader($inputFileType);
				$objectPHPExcel = $objectReader->load($input);
			} catch(Exception $e) {
				die('Error');
			}
			$sheet         = $objectPHPExcel->getSheet(0);
			$highestRow    = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			Yii::$app->session->setFlash('success', 'Upload thành công');
			for($row = 8; $row <= $highestRow; $row ++) {
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, false, false);
				$product = new Product();
				if($rowData[0][3] != null) {
					$category = Category::findOne(['name' => $rowData[0][3]]);
					if($category == null) {
						$category              = new Category();
						$category->name        = $rowData[0][3];
						$category->company_id  = $this->user->company_id;
						$category->create_date = date('Y-m-d H:i:s');
						$category->parent_id   = Category::findOne([
							'parent_id' => 0,
							'status'    => Category::STATUS_ACTIVE,
						]) ? Category::findOne(['parent_id' => 0])->id : 1;
						$category->status      = Category::STATUS_ACTIVE;
						if($category->save()) {
						} else {
							echo '<pre>';
							print_r($category->errors);
							die;
						}
					}
					$product->category_id = $category->id;
				} else {
				}
				$product->name         = $rowData[0][1];
				$product->code         = $rowData[0][2];
				$product->base_price   = $rowData[0][4];
				$product->retail_price = $rowData[0][5];
				$product->whole_price  = $rowData[0][6];
				$product->quantity     = $rowData[0][7];
				$product->weight       = $rowData[0][8];
				$product->size         = $rowData[0][9];
				$product->color        = $rowData[0][10];
				$product->content      = $rowData[0][11];
				if($product->save()) {
				} else {
					//					echo '<pre>';
					//					print_r($product->errors);
					//					die;
				}
			}
		}
		return $this->render('index', [
			'searchModel'   => $searchModel,
			'dataProvider'  => $dataProvider,
			'total_product' => $total_product,
			'total_post'    => $total_post,
			'total_money'   => $total_money,
			'total_sale'    => $total_sale,
			'model'         => $model,
		]);
	}

	/**
	 * Displays a single Product model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Product();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			//			$model->product_image = UploadedFile::getInstance($model, 'product_image');
			$img = $model->uploadPicture('image', 'product_image');
			if($model->post != null) {
				foreach($model->post as $product_post) {
					$post = Post::findOne($product_post);
					$post->link('products', $model, ['post_id' => $product_post]);
				}
			}
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
			}
			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionImport() {
	}

	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		//		echo '<pre>';
		//		print_r($model->postData);
		//		die;
		$oldImage = $model->image;
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			$img = $model->uploadPicture('image', 'product_image');
			if($img == false) {
				$model->image = $oldImage;
			}
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
			}
			return $this->redirect([
				'index',
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Product::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
