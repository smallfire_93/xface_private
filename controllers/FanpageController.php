<?php
namespace app\controllers;

use Yii;
use app\models\Fanpage;
use app\models\FanpageSearch;
use yii\authclient\clients\Facebook;
use yii\authclient\OAuth2;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FanpageController implements the CRUD actions for Fanpage model.
 */
class FanpageController extends Controller {

	public $enableCsrfValidation = false;

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Fanpage models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new FanpageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionSelectPage() {
		$facebook               = new Facebook();
		$facebook->clientSecret = Yii::$app->authClientCollection->clients['facebook']->clientSecret;
		$fb                     = $facebook->api('me/accounts', 'GET');
		foreach($fb['data'] as $item) {
			$fb_id   = $item['id'];
			$fanpage = Fanpage::find()->where("fb_page_id = '$fb_id'")->one();
			if(!$fanpage) {
				$fanpage               = new Fanpage();
				$fanpage->is_join      = 0;
				$fanpage->company_id   = Yii::$app->user->identity->company_id;
				$fanpage->name         = $item['name'];
				$fanpage->fb_page_id   = $fb_id;
				$fanpage->access_token = $item['access_token'];
				$fanpage->create       = date('Y-m-d H:i:s');
				$fanpage->save();
			} else {
				$fanpage->updateAttributes(['name' => $item['name']]);
			}
		}
		if(Yii::$app->request->post()) {
			foreach(Yii::$app->request->post()['Fanpage'] as $item) {
				$page = Fanpage::findOne(['fb_page_id' => $item['join']]);
				if($page) {
					$page->updateAttributes(['is_join' => $page::JOINED]);
				}
			}
			return $this->redirect(['fanpage']);
		}
		return $this->render('select-page', [
			'fbPages' => $fb,
		]);
	}

	public function actionFanpage() {
		$facebook               = new Facebook();
		$facebook->clientSecret = Yii::$app->authClientCollection->clients['facebook']->clientSecret;
		//		$fb                     = $facebook->api('me/accounts', 'GET');
		$companyId = Yii::$app->user->identity->company_id;
		$fb_pages  = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->all();
		return $this->render('fanpage', [
			'fb_pages' => $fb_pages,
		]);
	}

	/**
	 * Displays a single Fanpage model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Fanpage model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Fanpage();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Fanpage model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDeleteJoin() {
		if($data_post = Yii::$app->request->post()) {
			if(isset($data_post['id'])) {
				$id    = $data_post['id'];
				$model = $this->findModel($id);
				$model->updateAttributes(['is_join' => $model::NOT_JOIN]);
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * Deletes an existing Fanpage model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Fanpage model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Fanpage the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Fanpage::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
