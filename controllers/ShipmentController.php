<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 04/19/2017
 * Time: 3:13 PM
 */
namespace app\controllers;

use app\components\Controller;
use app\models\Order;

class ShipmentController extends Controller{

    public function actionIndex(){
        $company_id = \Yii::$app->user->identity->company_id;
        $models = Order::find()->where("company_id = $company_id")->all();

        return $this->render('index', [
            'models' => $models,
        ]);
    }
}