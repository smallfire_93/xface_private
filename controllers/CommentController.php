<?php

namespace app\controllers;

use app\models\Comment;
use app\models\CommentTag;
use app\models\Company;
use app\models\Conversation;
use app\models\Fanpage;
use app\models\Order;
use app\models\Payment;
use app\models\Post;
use app\models\Product;
use app\models\ProductFanpage;
use app\models\Promotion;
use app\models\PromotionPage;
use app\models\Tag;
use app\models\User;
use yii\authclient\clients\Facebook;
use yii\authclient\OAuthToken;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class CommentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionContact()
    {
        $session = \Yii::$app->session;
        if (isset($session['fanpage'])) {
            $pageId = $session['fanpage'];
        } else {
            $pageId = 0;
        }
        $companyId = \Yii::$app->user->identity->company_id;
        $getFanpages = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->all();

        if ($pageId == 0) {
            $fanpage = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->orderBy('id DESC')->one();
        } else {
            $fanpage = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        }

        $getAllComment = Comment::find()->joinWith(['fanpage'])->where("fanpage.fb_page_id = '{$pageId}' and parent_id is null and is_del = 0")->orderBy('created_date DESC')->all();
        $result = [];
        foreach ($getAllComment as $item) {
            $comment = Comment::find()->where("parent_id = $item->id")->orderBy('created_date DESC')->one();
            if ($comment) {
                $result[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $result[$item->id]['content'] = $comment->content;
                $result[$item->id]['fb_user_id'] = $comment->fb_user_id;
                $result[$item->id]['created_date'] = $comment->created_date;
            } else {
                $result[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $result[$item->id]['content'] = $item->content;
                $result[$item->id]['fb_user_id'] = $item->fb_user_id;
                $result[$item->id]['created_date'] = $item->created_date;
            }
        }

        $commentAll = Comment::find()->joinWith(['fanpage'])->where("fanpage.fb_page_id = '{$pageId}' and parent_id is null and type = 1")->orderBy('created_date DESC')->all();
        $comments = [];
        foreach ($commentAll as $item) {
            $comment = Comment::find()->where("parent_id = $item->id")->orderBy('created_date DESC')->one();
            if ($comment) {
                $comments[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $comments[$item->id]['content'] = $comment->content;
                $comments[$item->id]['fb_user_id'] = $comment->fb_user_id;
                $comments[$item->id]['created_date'] = $comment->created_date;
            } else {
                $comments[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $comments[$item->id]['content'] = $item->content;
                $comments[$item->id]['fb_user_id'] = $item->fb_user_id;
                $comments[$item->id]['created_date'] = $item->created_date;
            }
        }

        $inboxAll = Comment::find()->joinWith(['fanpage'])->where("fanpage.fb_page_id = '{$pageId}' and parent_id is null and type = 2")->orderBy('created_date DESC')->all();
        $inboxs = [];
        foreach ($inboxAll as $item) {
            $comment = Comment::find()->where("parent_id = $item->id")->orderBy('created_date DESC')->one();
            if ($comment) {
                $inboxs[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $inboxs[$item->id]['content'] = $comment->content;
                $inboxs[$item->id]['fb_user_id'] = $comment->fb_user_id;
                $inboxs[$item->id]['created_date'] = $comment->created_date;
            } else {
                $inboxs[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $inboxs[$item->id]['content'] = $item->content;
                $inboxs[$item->id]['fb_user_id'] = $item->fb_user_id;
                $inboxs[$item->id]['created_date'] = $item->created_date;
            }
        }

        $notRead = Comment::find()->joinWith(['fanpage'])->where("fanpage.fb_page_id = '{$pageId}' and parent_id is null and status = 0")->orderBy('created_date DESC')->all();
        $notReads = [];
        foreach ($notRead as $item) {
            $comment = Comment::find()->where("parent_id = $item->id")->orderBy('created_date DESC')->one();
            if ($comment) {
                $notReads[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $notReads[$item->id]['content'] = $comment->content;
                $notReads[$item->id]['fb_user_id'] = $comment->fb_user_id;
                $notReads[$item->id]['created_date'] = $comment->created_date;
            } else {
                $notReads[$item->id]['fb_comment_id'] = $item->fb_comment_id;
                $notReads[$item->id]['content'] = $item->content;
                $notReads[$item->id]['fb_user_id'] = $item->fb_user_id;
                $notReads[$item->id]['created_date'] = $item->created_date;
            }
        }

        return $this->render('contact', [
            'getAllComment' => $getAllComment,
            'getFanpages' => $getFanpages,
            'pageId' => $pageId,
            'result' => $result,
            'comments' => $comments,
            'inboxs' => $inboxs,
            'notReads' => $notReads,
            'fanpage' => $fanpage,
        ]);
    }

    public function actionDeleteComment()
    {
        $session = \Yii::$app->session;
        $userId = \Yii::$app->user->identity->id;
        $comment_id = $_POST['comment_id'];
        $comment = Comment::find()->where("fb_comment_id = '$comment_id'")->one();
        $comment->is_del = 1;
        $comment->save();
        $fb = new Facebook();
        if (isset($session['fanpage'])) {
            $pageId = $session['fanpage'];
        }
        $getToken = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        $token = new OAuthToken();
        $token->setToken($getToken->access_token);
        $fb->setAccessToken($token);
        $fb->api($comment_id, 'DELETE');
        $getTokenCompany = Company::find()->where("user_id = $userId")->one()->fb_token;
        $token->setToken($getTokenCompany);
        $fb->setAccessToken($token);
    }

    public function actionHiddenComment()
    {
        $session = \Yii::$app->session;
        $userId = \Yii::$app->user->identity->id;
        $comment_id = $_POST['comment_id'];
        $comment = Comment::find()->where("fb_comment_id = '$comment_id'")->one();
        $comment->is_hidden = 1;
        $comment->save();
        $fb = new Facebook();
        if (isset($session['fanpage'])) {
            $pageId = $session['fanpage'];
        }
        $getToken = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        $token = new OAuthToken();
        $token->setToken($getToken->access_token);
        $fb->setAccessToken($token);
        $fb->api($comment_id . '?is_hidden=true', 'POST');
        $getTokenCompany = Company::find()->where("user_id = $userId")->one()->fb_token;
        $token->setToken($getTokenCompany);
        $fb->setAccessToken($token);
    }

    public function actionShowComment()
    {
        $session = \Yii::$app->session;
        $userId = \Yii::$app->user->identity->id;
        $comment_id = $_POST['comment_id'];
        $comment = Comment::find()->where("fb_comment_id = '$comment_id'")->one();
        $comment->is_hidden = 0;
        $comment->save();
        $fb = new Facebook();
        if (isset($session['fanpage'])) {
            $pageId = $session['fanpage'];
        }
        $getToken = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        $token = new OAuthToken();
        $token->setToken($getToken->access_token);
        $fb->setAccessToken($token);
        $fb->api($comment_id . '?is_hidden=false', 'POST');
        $getTokenCompany = Company::find()->where("user_id = $userId")->one()->fb_token;
        $token->setToken($getTokenCompany);
        $fb->setAccessToken($token);
    }

    public function actionGetConversation()
    {
        $this->layout = '_blank';
        $session = \Yii::$app->session;
        if (isset($session['fanpage'])) {
            $pageId = $session['fanpage'];
        } else {
            $pageId = 0;
        }
        $companyId = \Yii::$app->user->identity->company_id;
        $getFanpages = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->all();

        if ($pageId == 0) {
            $fanpage = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->orderBy('id DESC')->one();
        } else {
            $fanpage = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        }

        $fb = new Facebook();
        $getComment = $fb->api($fanpage->fb_page_id . '/posts?fields=comments', 'GET');
        return $this->render('get-conversation', [
            'getFanpages' => $getFanpages,
            'getComment' => $getComment,
        ]);
    }

    public function actionConversation()
    {
        $session = \Yii::$app->session;
        unset($session['cart']);
        if (isset($session['fanpage'])) {
            $pageId = $session['fanpage'];
        } else {
            $pageId = 0;
        }
        $companyId = \Yii::$app->user->identity->company_id;
        $getFanpages = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->all();

        if ($pageId == 0) {
            $fanpage = Fanpage::find()->where("company_id = '$companyId' and is_join = 1")->orderBy('id DESC')->one();
        } else {
            $fanpage = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        }
        $this->layout = '_blank';
        $object_id = $_GET['object_id'];
        $conversation_id = $_GET['conversation_id'];
        $getIdPost = Comment::find()->where("fb_comment_id = '$conversation_id'")->one()->post_id;
        $post = Post::findOne($getIdPost);
        $fb = new Facebook();
        $conversation = Comment::find()->where("fb_comment_id = '$conversation_id' and is_del = 0")->one();
        if($conversation){
            $getAllComments = Comment::find()->where("parent_id = $conversation->id and is_del = 0")->all();
            Comment::updateAll(['status' => 1], ['status' => 0, 'parent_id' => $conversation->id]);
        }
        $commentParent = Comment::find()->where("fb_comment_id = '$conversation_id'")->one();
        $customer = $fb->api($conversation_id, 'GET');
        $checkHidden = $fb->api($conversation_id . '?fields=is_hidden', 'GET');
        $checkHidden = $checkHidden['is_hidden'];
        $getFanpage = Fanpage::find()->where("fb_page_id = '$pageId'")->one();
        $model = Conversation::find()->where("company_id = $companyId or type = 1")->all();
        $getTags = CommentTag::find()->select('tag.name as name')->joinWith('tag')->where("comment_id = $conversation->id")->all();
        $order = new Order();
        $promotion = PromotionPage::find()->where("fanpage_id = $pageId")->all();
        $dropPayment = ArrayHelper::map(Payment::find()->where('status = 1')->all(), 'id', 'name');
        $productAll = ProductFanpage::find()->select('product.id as id, product.name as product_name, product.retail_price as price, product.image as image')->joinWith('fanpage')->joinWith('product')->where("fanpage.id = $fanpage->id")->all();
        return $this->render('conversation', [
            'customer' => $customer,
            'conversation_id' => $conversation_id,
            'object_id' => $object_id,
            'pageId' => $pageId,
            'post' => $post,
            'getAllComments' => $getAllComments,
            'parent_id' => $conversation->id,
            'model' => $model,
            'commentParent' => $commentParent,
            'checkHidden' => $checkHidden,
            'getFanpage' => $getFanpage,
            'getTags' => $getTags,
            'order' => $order,
            'promotion' => $promotion,
            'dropPayment' => $dropPayment,
            'productAll' => $productAll
        ]);
    }

    public function actionSetStatus()
    {
        $parentId = $_POST['parent_id'];
        Comment::updateAll(['status = 1'], "status = 0 and parent_id = $parentId");
    }

    public function actionSendMessage()
    {
        $session = \Yii::$app->session;
        $userId = \Yii::$app->user->identity->id;
        $fb = new Facebook();
        $conversation_id = $_POST['conversation_id'];
        $hidden_post_id = $_POST['hidden_post_id'];
        $hidden_parent_id = $_POST['hidden_parent_id'];
        $content = $_POST['content'];
        $pageId = $session['fanpage'];
        $getToken = Fanpage::find()->where("fb_page_id = '{$pageId}'")->one();
        $token = new OAuthToken();
        $token->setToken($getToken->access_token);
        $fb->setAccessToken($token);
        $commentFB = $fb->api($conversation_id . '/comments', 'POST', ['message' => $content]);
        $getTokenCompany = Company::find()->where("user_id = $userId")->one()->fb_token;
        $token->setToken($getTokenCompany);
        $fb->setAccessToken($token);
        $comment = new Comment();
        $comment->post_id = $hidden_post_id;
        $comment->fb_comment_id = $commentFB['id'];
        $comment->fanpage_id = $getToken->id;
        $comment->content = $content;
        $comment->status = 1;
        $comment->created_date = date('Y-m-d H:i:s');
        $comment->parent_id = $hidden_parent_id;
        $comment->type = 2;
        $comment->fb_user_id = $getToken->fb_page_id;
        if ($comment->save()) {

        } else {
            print_r($comment->getErrors());
        }
    }

    public function actionGetFanpage()
    {
        $session = \Yii::$app->session;
        $pageID = $_POST['page_id'];
        $session['fanpage'] = $pageID;
    }

    public function actionSetup()
    {
        $model = new Comment();
        return $this->render('setupComment', ['model' => $model]);
    }

    public function actionGetAllTag()
    {
        $models = Tag::find()->all();
        $items = [];

        foreach ($models as $model) {
            $items[] = $model->name;
        }

        return json_encode($items);
    }

    public function actionGetTag()
    {
        $id = $_GET['id_comment'];
        $models = CommentTag::find()->select('tag.name as name')->joinWith('tag')->where("comment_tag.comment_id = $id")->all();
        $items = [];

        foreach ($models as $model) {
            $items[] = $model->name;
        }

        return json_encode($items);
    }

    public function actionCreateTag()
    {
        $tags = $_POST['tags'];
        $id_comment = $_POST['id_comment'];
        $company = \Yii::$app->user->identity->company_id;
        for ($i = 0; $i < count($tags); $i++) {
            $tagName = $tags[$i];
            $tag = Tag::find()->where("name = '$tagName'")->one();
            if (!$tag) {
                $tag = new Tag();
                $tag->name = $tagName;
                $tag->company_id = $company;
                $tag->type = 2;
                if ($tag->save()) {
                    $commentTag = new CommentTag();
                    $commentTag->tag_id = $tag->id;
                    $commentTag->comment_id = $id_comment;
                    if ($commentTag->save()) {

                    } else {
                        print_r($commentTag->getErrors());
                    }
                } else {
                    print_r($tag->getErrors());
                }
            } else {
                $commentTag = CommentTag::find()->where("tag_id = $tag->id and comment_id = $id_comment")->one();
                if (!$commentTag) {
                    $commentTag = new CommentTag();
                    $commentTag->tag_id = $tag->id;
                    $commentTag->comment_id = $id_comment;
                    if ($commentTag->save()) {
                    } else {
                        print_r($commentTag->getErrors());
                    }
                }
            }
        }

        $getComment = CommentTag::find()->select('tag.name as name')->joinWith('tag')->where("comment_id = $id_comment")->all();
        $str = "";
        $str .= "<ul>";
        foreach ($getComment as $comment){
            $str .= '<li><span class="item tag-color-1">'.$comment->name.'</span></li>';
        }
        $str .= "</ul>";
        return $str;
    }

    public function actionRemoveTag()
    {
        if(isset($_POST['tags'])){
            $tags = $_POST['tags'];
        }else{
            $tags = null;
        }
        $id_comment = $_POST['id_comment'];
        if($tags != null) {
            $str = "";
            foreach ($tags as $item) {
                $str .= "'" . $item . "',";
            }
            $stringTag = rtrim($str, ",");
            $company = \Yii::$app->user->identity->company_id;
            Tag::deleteAll("name NOT IN ($stringTag) and type = 2 and company_id = $company");
            $tag = Tag::find()->where("name IN ($stringTag)")->all();
            $tagId = "";
            foreach ($tag as $item) {
                $tagId .= "'" . $item->id . "',";
            }
            $tagIds = rtrim($tagId, ",");
            CommentTag::deleteAll("tag_id NOT IN ($tagIds)");
        }else{
            CommentTag::deleteAll("comment_id = $id_comment");
        }
        $getComment = CommentTag::find()->select('tag.name as name')->joinWith('tag')->where("comment_id = $id_comment")->all();
        $str = "";
        $str .= "<ul>";
        foreach ($getComment as $comment){
            $str .= '<li><span class="item tag-color-1">'.$comment->name.'</span></li>';
        }
        $str .= "</ul>";
        return $str;
    }
}
