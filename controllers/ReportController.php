<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 12:00 PM
 */
namespace app\controllers;

use app\components\Controller;

class ReportController extends Controller {

	public function actionIndex() {
		return $this->render('index');
	}

	public function actionReportComment() {
		return $this->render('report-comment');
	}

	public function actionReportInbox() {
		return $this->render('report-inbox');
	}

	public function actionReportCustomer() {
		return $this->render('report-customer');
	}

	public function actionReportLogistic() {
		return $this->render('report-logistic');
	}

	public function actionReportOrderpost() {
		return $this->render('report-orderpost');
	}

	public function actionReportOrderproduct() {
		return $this->render('report-orderproduct');
	}

	public function actionReportRevenue() {
		return $this->render('report-revenue');
	}

	public function actionReportSale() {
		return $this->render('report-sale');
	}

	public function actionReportProduct() {
		return $this->render('report-product');
	}
}