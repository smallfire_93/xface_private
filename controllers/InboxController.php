<?php

namespace app\controllers;

use app\models\Inbox;

class InboxController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionSetup()
    {
        $model = new Inbox();
        return $this->render('setupInbox',['model'=>$model]);
    }
}
