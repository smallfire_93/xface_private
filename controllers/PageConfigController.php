<?php

namespace app\controllers;

use app\models\Fanpage;
use app\models\PageConfigReply;
use Yii;
use app\models\PageConfig;
use app\models\PageConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageConfigController implements the CRUD actions for PageConfig model.
 */
class PageConfigController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PageConfig models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionComment()
    {
        $model = new PageConfig();
        if($model->load(Yii::$app->request->post())){
            $fanpageId  =   Yii::$app->user->identity->fanpage_id;
            $comment    =   PageConfigReply::find()->where("fanpage_id  =   '$fanpageId'")->all();
            $comments   =   [];
            foreach($comment    as  $item){
                $comments[$item->id]['type']    =   $item->type =1;
                $comments[$item->id]['keyword']    =   $item->keyword;
                $comments[$item->id]['reply_content']   =   $item->reply_content;
            }
            if($model->save()){
                return $this->redirect(['comment' => $model->id]);
            }
        }

        return $this->render('comment', [
            'model' => $model,

        ]);
    }
    public function actionInbox()
    {
        $model = new PageConfig();
        $searchModel = new PageConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if($model->load(Yii::$app->request->post())){
            if($model->save()){
                return $this->redirect(['inbox' => $model->id]);
            }
        }

        return $this->render('Inbox', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }
    /**
     * Displays a single PageConfig model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PageConfig model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PageConfig();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PageConfig model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PageConfig model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PageConfig model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageConfig the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PageConfig::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
