<?php

namespace app\controllers;

use Yii;
use app\models\ReportProduct;
use app\models\ReportProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReportProductController implements the CRUD actions for ReportProduct model.
 */
class ReportProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReportProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel = new ReportProductSearch();
        $TotalProduct  = $searchModel->getTotalProduct(Yii::$app->request->queryParams);

        $searchModel = new ReportProductSearch();
        $TotalQuantitySale  = $searchModel->getTotalQuantitySale(Yii::$app->request->queryParams);

        $getProductDayByDAy =   ReportProduct::find()->select('total_order , DATE(created_date) as created_date')->where('DAY(created_date) = DAY(CURDATE())')->groupBy('DATE(created_date)')->all();



        return $this->render('index', [
            'searchModel'   =>  $searchModel,
            'dataProvider'  =>  $dataProvider,
            'TotalProduct' =>  $TotalProduct,
            'TotalQuantitySale'    =>  $TotalQuantitySale,
            'getProductDayByDAy'   =>  $getProductDayByDAy,
        ]);
    }

    /**
     * Displays a single ReportProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ReportProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReportProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ReportProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ReportProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReportProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReportProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReportProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
