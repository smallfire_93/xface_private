<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/30/2017
 * Time: 9:59 PM
 */

namespace app\controllers;


use app\components\Controller;

class SocketController extends Controller
{
    public function actionIndex(){
        $this->layout = '_blank';
        return $this->render('index');
    }
}