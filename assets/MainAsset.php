<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 4/20/2017
 * Time: 1:40 PM
 */
namespace app\assets;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle {

	public $basePath = '@webroot';

	public $baseUrl  = '@web';

	public $js       = [
		'js/post.js',
	];

	public $depends  = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];
}