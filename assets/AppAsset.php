<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\assets;

use yii\base\View;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle {

	public $basePath = '@webroot';

	public $baseUrl  = '@web';

	public $css      = [
		'css/bootstrap.min.css',
		'css/font-awesome.min.css',
		'css/ace-fonts.css',
		'css/ace.min.css',
		'css/ace-rtl.min.css',
		'css/ace-skins.min.css',
		'css/font-awesome.min.css',
		'css/font-awesome-ie7.min.css',
		'css/ace-fonts.css',
		'css/ace.min.css',
		'css/ace-rtl.min.css',
		'css/ace-skins.min.css',
		'css/ace-ie.min.css',
		'css/inputTags.css',
		'css/style.css',
		'css/nprogress.css',
		//        'css/jquery.scrollbar.css',
	];

	public $js       = [
		//        'http://localhost:5555/socket.io/socket.io.js',
		//        'js/socket.js',
		//        'js/jquery.nicescroll.js',
		'js/ace-extra.min.js',
		//        'js/bootstrap.min.js',
		'js/typeahead-bs2.min.js',
		'js/jquery-ui-1.10.3.custom.min.js',
		'js/jquery.ui.touch-punch.min.js',
		'js/jquery.slimscroll.min.js',
		'js/jquery.easy-pie-chart.min.js',
		'js/jquery.sparkline.min.js',
		'js/flot/jquery.flot.min.js',
		'js/flot/jquery.flot.pie.min.js',
		'js/flot/jquery.flot.resize.min.js',
		'js/ace-elements.min.js',
		'js/ace.min.js',
		'js/ace-extra.min.js',
		'js/html5shiv.js',
		'js/respond.min.js',
		'js/fb.js',
		'js/main.js',
		'js/jquery.validate.min.js',
        'js/nprogress.js',
	];

	//	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];
}
