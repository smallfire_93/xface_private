<?php
namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer        $id
 * @property integer        $parent_id
_id
 * @property string         $username
 * @property string         $password
 * @property integer        $company_id
 * @property string         $code
 * @property string         $full_name
 * @property string         $phone
 * @property string         $email
 * @property integer        $status
 * @property integer        $online_status
 * @property string         $fb_id
 * @property integer        $instagram_id
 * @property string         $last_login
 * @property string         $created_at
 * @property string         $updated_at
 * @property string         $address
 * @property string         $start_time
 * @property string         $end_time
 *
 * @property Company[]      $companies
 * @property Conversation[] $conversations
 * @property Promotion[]    $promotions
 * @property ReportSale[]   $reportSales
 * @property Company        $company
 * @property Fanpage        $fanpages
 * @property Fanpage        $fanpageActive
 * @property UserFanpage[]  $userFanpages
 * @property UserGroup[]    $userGroups
 * @property UserRole[]     $userRoles
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

	/**
	 * @inheritdoc
	 */
	public $password_repeat;

	public $invalid;

	public static function tableName() {
		return 'user';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['password'],
				'string',
				'min' => 6,
			],
			[
				'password_repeat',
				'compare',
				'compareAttribute' => 'password',
				'message'          => "Mật khẩu không khớp",
			],
			[
				[
					'parent_id',
					'company_id',
					'status',
					'online_status',
					'instagram_id',
				],
				'integer',
			],
			[
				['username'],
				'required',
				'message' => 'Tên đăng nhập không được để trống.',
			],
			[
				['password'],
				'required',
				'message' => 'Mật khẩu cấp 1 không được để trống.',
			],
			[
				['email'],
				'required',
				'message' => 'Email không được để trống.',
			],
			//            [['password_repeat'], 'required','message' => 'Mật khẩu cấp 2 không được để trống.'],
			[
				['username'],
				'required',
				'message' => 'Không được để trống.',
			],
			[
				[
					'last_login',
					'created_at',
					'updated_at',
					'start_time',
					'end_time',
				],
				'safe',
			],
			[
				['username'],
				'string',
				'max' => 64,
			],
			[
				[
					'password',
					'fb_id',
				],
				'string',
				'max' => 120,
			],
			[
				['code'],
				'string',
				'max' => 30,
			],
			[
				['full_name'],
				'string',
				'max' => 50,
			],
			[
				'email',
				'email',
			],
			[
				['phone'],
				'string',
				'max' => 20,
			],
			[
				['email'],
				'string',
				'max' => 128,
			],
			[
				['address'],
				'string',
				'max' => 255,
			],
			[
				['username'],
				'unique',
			],
			[
				['company_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Company::className(),
				'targetAttribute' => ['company_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'parent_id'     => 'Parent ID',
			'username'      => 'Username',
			'password'      => 'Password',
			'company_id'    => 'Company ID',
			'code'          => 'Code',
			'full_name'     => 'Full Name',
			'phone'         => 'Phone',
			'email'         => 'Email',
			'status'        => 'Status',
			'online_status' => 'Online Status',
			'fb_id'         => 'Fb ID',
			'instagram_id'  => 'Instagram ID',
			'last_login'    => 'Last Login',
			'created_at'    => 'Created At',
			'updated_at'    => 'Updated At',
			'address'       => 'Address',
			'start_time'    => 'Start Time',
			'end_time'      => 'End Time',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCompanies() {
		return $this->hasMany(Company::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getConversations() {
		return $this->hasMany(Conversation::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPromotions() {
		return $this->hasMany(Promotion::className(), ['created_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportSales() {
		return $this->hasMany(ReportSale::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCompany() {
		return $this->hasOne(Company::className(), ['id' => 'company_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserFanpages() {
		return $this->hasMany(UserFanpage::className(), ['user_id' => 'id']);
	}

	public function getFanpages() {
		return $this->hasMany(Fanpage::className(), ['id' => 'fanpage_id'])->via('userFanpages');
	}

	public function getFanpageActive() {
		return $this->hasMany(Fanpage::className(), ['id' => 'fanpage_id'])->via('userFanpages')->where(['is_join' => Fanpage::JOINED]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserGroups() {
		return $this->hasMany(UserGroup::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserRoles() {
		return $this->hasMany(UserRole::className(), ['user_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 *
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return static::findOne(['username' => $username]);
		//        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 *
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token) {
		if(!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		return static::findOne([
			'password_reset_token' => $token,
			'status'               => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 *
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token) {
		if(empty($token)) {
			return false;
		}
		$expire    = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts     = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp + $expire >= time();
	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		//		//        return true;
		//		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		//		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 *
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 *
	 * @return string
	 */
	public function setPassword($password) {
		return $this->password = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		//		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	//	public function generatePasswordResetToken() {
	//		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	//	}
	/**
	 * Removes password reset token
	 */
	//	public function removePasswordResetToken() {
	//		$this->password_reset_token = null;
	//	}
}
