<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_revenue".
 *
 * @property integer $id
 * @property string $created_date
 * @property integer $total_customer
 * @property integer $total_order_finish
 * @property integer $total_order_pending
 * @property integer $total_order_feedback
 * @property integer $total_order_cancel
 * @property integer $total_money
 * @property integer $fanpage_id
 * @property integer $company_id
 *
 * @property Company $company
 * @property Fanpage $fanpage
 */
class ReportRevenue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_revenue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date'], 'safe'],
            [['total_customer', 'total_order_finish', 'total_order_pending', 'total_order_feedback', 'total_order_cancel', 'total_money', 'fanpage_id', 'company_id'], 'integer'],
            [['fanpage_id', 'company_id'], 'required'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'total_customer' => 'Total Customer',
            'total_order_finish' => 'Total Order Finish',
            'total_order_pending' => 'Total Order Pending',
            'total_order_feedback' => 'Total Order Feedback',
            'total_order_cancel' => 'Total Order Cancel',
            'total_money' => 'Total Money',
            'fanpage_id' => 'Fanpage ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }
}
