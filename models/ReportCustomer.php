<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_customer".
 *
 * @property integer $id
 * @property string $created_date
 * @property integer $total_customer
 * @property integer $total_customer_new
 * @property integer $total_customer_old
 * @property integer $total_money
 * @property integer $company_id
 * @property integer $fanpage_id
 *
 * @property Company $company
 * @property Fanpage $fanpage
 */
class ReportCustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date'], 'safe'],
            [['total_customer', 'total_customer_new', 'total_customer_old', 'total_money', 'company_id', 'fanpage_id'], 'integer'],
            [['company_id', 'fanpage_id'], 'required'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'total_customer' => 'Total Customer',
            'total_customer_new' => 'Total Customer New',
            'total_customer_old' => 'Total Customer Old',
            'total_money' => 'Total Money',
            'company_id' => 'Company ID',
            'fanpage_id' => 'Fanpage ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }
}
