<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $full_name
 * @property integer $phone
 * @property string $address
 * @property string $email
 * @property integer $fb_user_id
 * @property integer $total_order
 * @property integer $total_quantity
 * @property string $create_date
 * @property integer $company_id
 * @property integer $is_report
 * @property string $report_content
 *
 * @property Company $company
 * @property Order[] $orders
 * @property ReportCustomerOrder[] $reportCustomerOrders
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'fb_user_id', 'total_order', 'total_quantity', 'company_id', 'is_report'], 'integer'],
            [['create_date'], 'safe'],
            [['company_id'], 'required'],
            [['full_name'], 'string', 'max' => 50],
            [['address', 'report_content'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 120],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'phone' => 'Phone',
            'address' => 'Address',
            'email' => 'Email',
            'fb_user_id' => 'Fb User ID',
            'total_order' => 'Total Order',
            'total_quantity' => 'Total Quantity',
            'create_date' => 'Create Date',
            'company_id' => 'Company ID',
            'is_report' => 'Is Report',
            'report_content' => 'Report Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportCustomerOrders()
    {
        return $this->hasMany(ReportCustomerOrder::className(), ['customer_id' => 'id']);
    }
}
