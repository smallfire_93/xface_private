<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promotion_page".
 *
 * @property integer $id
 * @property integer $promotion_id
 * @property integer $fanpage_id
 *
 * @property Fanpage $fanpage
 * @property Promotion $promotionss
 */
class PromotionPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promotion_id', 'fanpage_id'], 'required'],
            [['promotion_id', 'fanpage_id'], 'integer'],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['promotion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promotion::className(), 'targetAttribute' => ['promotion_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promotion_id' => 'Promotion ID',
            'fanpage_id' => 'Fanpage ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(Promotion::className(), ['id' => 'promotion_id']);
    }
}
