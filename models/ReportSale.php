<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_sale".
 *
 * @property integer $id
 * @property string $created_date
 * @property integer $user_id
 * @property string $name
 * @property integer $total_order
 * @property integer $total_order_confirm
 * @property integer $total_order_finish
 * @property integer $total_money
 * @property integer $company_id
 * @property integer $fanpage_id
 *
 * @property Company $company
 * @property User $user
 */
class ReportSale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_sale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date'], 'safe'],
            [['user_id', 'company_id', 'fanpage_id'], 'required'],
            [['user_id', 'total_order', 'total_order_confirm', 'total_order_finish', 'total_money', 'company_id', 'fanpage_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'user_id' => 'User ID',
            'name' => 'Name',
            'total_order' => 'Total Order',
            'total_order_confirm' => 'Total Order Confirm',
            'total_order_finish' => 'Total Order Finish',
            'total_money' => 'Total Money',
            'company_id' => 'Company ID',
            'fanpage_id' => 'Fanpage ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
