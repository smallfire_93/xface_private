<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_log".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id_process
 * @property string $create_date
 * @property integer $status
 * @property string $note
 *
 * @property Order $order
 */
class OrderLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'user_id_process', 'status'], 'integer'],
            [['create_date'], 'safe'],
            [['note'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'user_id_process' => 'User Id Process',
            'create_date' => 'Create Date',
            'status' => 'Status',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
