<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer       $id
 * @property string        $code
 * @property integer       $user_id_create
 * @property integer       $user_id_process
 * @property integer       $quantity
 * @property integer       $total_money
 * @property integer       $customer_id
 * @property integer       $payment_status
 * @property integer       $shipping_status
 * @property integer       $payment_id
 * @property string        $customer_note
 * @property integer       $fb_user_id
 * @property string        $tracking
 * @property string        $export_date
 * @property string        $receiver_date
 * @property integer       $shipping_fee
 * @property integer       $code_fee
 * @property string        $address
 * @property string        $phone
 * @property integer       $transport_id
 * @property integer       $discount
 * @property integer       $status
 *
 * @property Customer      $customer
 * @property User          $userCreate
 * @property User          $userProcess
 * @property OrderDetail[] $orderDetails
 * @property OrderLog[]    $orderLogs
 */
class Order extends Model {

	public $product_array, $customer_name;

	public                 $customer_email;

	public                 $from_date;

	public                 $to_date;

	const PAYMENT_STATUS   = [
		'Chưa thanh toán',
		'Đã thanh toán',
	];

	const PAYMENT_PENDING  = 0;

	const PAYMENT_SUCCESS  = 1;

	const SHIPPING_STATUS  = [
		'Đang giao hàng',
		'Thành công',
		'Hồi hoàn',
	];

	const SHIPPING_PENDING = 0;

	const SHIPPING_SUCCESS = 1;

	const SHIPPING_RETURN  = 2;

	const STATUS           = [
		1 => 'Đặt hàng',
		2 => 'Đã xác nhận',
		3 => 'Kiểm kho và đóng gói',
		4 => 'Vận chuyển',
		5 => 'Giao hàng',
		6 => 'Hoàn thành',
		7 => 'Hàng trả lại',
		8 => 'Hủy đơn',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'order';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id_create',
					'user_id_process',
					'quantity',
					'total_money',
					'customer_id',
					'payment_status',
					'shipping_status',
					'payment_id',
					'fb_user_id',
					'shipping_fee',
					'code_fee',
					'transport_id',
					'discount',
					'status',
				],
				'integer',
			],
			[
				[
					'customer_id',
					'transport_id',
				],
				'required',
			],
			[
				[
					'export_date',
					'receiver_date',
					'customer_email',
				],
				'safe',
			],
			[
				['code'],
				'string',
				'max' => 30,
			],
			[
				['customer_note'],
				'string',
				'max' => 255,
			],
			[
				['tracking'],
				'string',
				'max' => 50,
			],
			[
				['address'],
				'string',
				'max' => 120,
			],
			[
				['phone'],
				'string',
				'max' => 20,
			],
			[
				['customer_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Customer::className(),
				'targetAttribute' => ['customer_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'              => 'ID',
			'code'            => 'Mã đơn hàng',
			'user_id_create'  => 'Người tạo',
			'user_id_process' => 'Người đang xử lý',
			'quantity'        => 'Số lượng',
			'total_money'     => 'Tổng tiền',
			'customer_id'     => 'Customer ID',
			'payment_status'  => 'Thanh toán',
			'shipping_status' => 'Giao hàng',
			'payment_id'      => 'Payment ID',
			'customer_note'   => 'Customer Note',
			'fb_user_id'      => 'Fb User ID',
			'tracking'        => 'Tracking',
			'export_date'     => 'Export Date',
			'receiver_date'   => 'Receiver Date',
			'shipping_fee'    => 'Shipping Fee',
			'code_fee'        => 'Code Fee',
			'address'         => 'Địa chỉ',
			'phone'           => 'Số điện thoại',
			'transport_id'    => 'Transport ID',
			'discount'        => 'Discount',
			'status'          => 'Trạng thái',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCustomer() {
		return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}

	public function getUserCreate() {
		return $this->hasOne(User::className(), ['id' => 'user_id_create']);
	}

	public function getUserProcess() {
		return $this->hasOne(User::className(), ['id' => 'user_id_process']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderDetails() {
		return $this->hasMany(OrderDetail::className(), ['order_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderLogs() {
		return $this->hasMany(OrderLog::className(), ['order_id' => 'id']);
	}

	public function getColorPayment() {
		if($this->payment_status == $this::PAYMENT_SUCCESS) {
			return 'btn-success';
		} elseif($this->payment_status == $this::PAYMENT_PENDING) {
			return 'btn-danger';
		} else {
			return 'btn-warning';
		}
	}

	public function getColorShipping() {
		if($this->shipping_status == $this::SHIPPING_SUCCESS) {
			return 'btn-success';
		} elseif($this->shipping_status == $this::SHIPPING_PENDING) {
			return 'btn-danger';
		} else {
			return 'btn-warning';
		}
	}
}
