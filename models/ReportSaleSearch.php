<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportSale;

/**
 * ReportSaleSearch represents the model behind the search form of `app\models\ReportSale`.
 */
class ReportSaleSearch extends ReportSale
{
    /**
     * @inheritdoc
     */
    public $start_time, $end_time;
    public function rules()
    {
        return [
            [['id', 'user_id', 'total_order', 'total_order_confirm', 'total_order_finish', 'total_money', 'company_id', 'fanpage_id'], 'integer'],
            [['created_date', 'name','start_time','end_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportSale::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'user_id' => $this->user_id,
            'total_order' => $this->total_order,
            'total_order_confirm' => $this->total_order_confirm,
            'total_order_finish' => $this->total_order_finish,
            'total_money' => $this->total_money,
            'company_id' => $this->company_id,
            'fanpage_id' => $this->fanpage_id,
        ]);

        $query->andFilterWhere(['>=','created_date', $this->start_time]);
        $query->andFilterWhere(['<=','created_date', $this->end_time]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
