<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "provider".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $address
 * @property string  $phone
 * @property string  $website
 * @property string  $email
 * @property integer $status
 * @property string  $created_date
 * @property integer $company_id
 */
class Provider extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'provider';
	}

	const STATUS = [
		'Inactive',
		'Active',
	];

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'company_id',
				],
				'required',
			],
			[
				[
					'status',
					'company_id',
				],
				'integer',
			],
			[
				['created_date'],
				'safe',
			],
			[
				['name'],
				'string',
				'max' => 100,
			],
			[
				[
					'address',
					'phone',
					'website',
					'email',
				],
				'string',
				'max' => 120,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'name'         => 'Tên',
			'address'      => 'Địa chỉ',
			'phone'        => 'Số đt',
			'website'      => 'Website',
			'email'        => 'Email',
			'status'       => 'Trạng thái',
			'created_date' => 'Created Date',
			'company_id'   => 'Company ID',
		];
	}
}
