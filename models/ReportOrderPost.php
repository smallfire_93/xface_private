<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_order_post".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $company_id
 * @property integer $total_customer
 * @property integer $total_order
 * @property integer $total_order_pending
 * @property integer $total_order_confirm
 * @property integer $total_order_packing
 * @property integer $total_order_shipping
 * @property integer $total_order_received
 * @property integer $total_order_feedback
 * @property integer $total_order_finish
 * @property integer $total_order_cancel
 * @property string $created_date
 * @property integer $fanpage_id
 * @property integer $total_money
 *
 * @property Company $company
 * @property Post $post
 */
class ReportOrderPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_order_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'company_id', 'fanpage_id'], 'required'],
            [['post_id', 'company_id', 'total_customer', 'total_order', 'total_order_pending', 'total_order_confirm', 'total_order_packing', 'total_order_shipping', 'total_order_received', 'total_order_feedback', 'total_order_finish', 'total_order_cancel', 'fanpage_id', 'total_money'], 'integer'],
            [['created_date'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'company_id' => 'Company ID',
            'total_customer' => 'Total Customer',
            'total_order' => 'Total Order',
            'total_order_pending' => 'Total Order Pending',
            'total_order_confirm' => 'Total Order Confirm',
            'total_order_packing' => 'Total Order Packing',
            'total_order_shipping' => 'Total Order Shipping',
            'total_order_received' => 'Total Order Received',
            'total_order_feedback' => 'Total Order Feedback',
            'total_order_finish' => 'Total Order Finish',
            'total_order_cancel' => 'Total Order Cancel',
            'created_date' => 'Created Date',
            'fanpage_id' => 'Fanpage ID',
            'total_money' => 'Total Money',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
