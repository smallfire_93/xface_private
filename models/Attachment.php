<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attachment".
 *
 * @property string $id
 * @property string $post_id
 * @property string $fb_id
 * @property int $status 1: success -2: Error
 * @property int $type 1: image - 2: video
 * @property string $video
 * @property string $image
 * @property int $process 0: pending - 1: finish
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id'], 'required'],
            [['post_id', 'fb_id', 'status', 'type', 'process'], 'integer'],
            [['video', 'image'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'fb_id' => 'Fb ID',
            'status' => 'Status',
            'type' => 'Type',
            'video' => 'Video',
            'image' => 'Image',
            'process' => 'Process',
        ];
    }
}
