<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inbox".
 *
 * @property integer $id
 * @property integer $fanpage_id
 * @property integer $user_id_manager
 * @property integer $user_id_reply
 * @property string $fb_user_id
 * @property integer $content
 * @property integer $status
 * @property string $created_date
 * @property integer $fb_inbox_id
 *
 * @property Fanpage $fanpage
 */
class Inbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fanpage_id', 'user_id_manager', 'user_id_reply', 'content', 'status', 'fb_inbox_id'], 'integer'],
            [['created_date'], 'safe'],
            [['fb_user_id'], 'string', 'max' => 255],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fanpage_id' => 'Fanpage ID',
            'user_id_manager' => 'User Id Manager',
            'user_id_reply' => 'User Id Reply',
            'fb_user_id' => 'Fb User ID',
            'content' => 'Content',
            'status' => 'Status',
            'created_date' => 'Created Date',
            'fb_inbox_id' => 'Fb Inbox ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }
}
