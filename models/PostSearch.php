<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PortSearch represents the model behind the search form about `app\models\Post`.
 */
class PostSearch extends Post {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'total_comment',
					'total_parent_comment',
					'total_like',
					'total_share',
					'fanpage_id',
					'total_order',
					'is_auto_comment',
				],
				'integer',
			],
			[
				[
					'fb_post_id',
					'content',
					'created_date',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Post::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'                   => $this->id,
			'total_comment'        => $this->total_comment,
			'total_parent_comment' => $this->total_parent_comment,
			'total_like'           => $this->total_like,
			'total_share'          => $this->total_share,
			'created_date'         => $this->created_date,
			'fanpage_id'           => $this->fanpage_id,
			'total_order'          => $this->total_order,
			'is_auto_comment'      => $this->is_auto_comment,
		]);
		$query->andFilterWhere([
			'like',
			'fb_post_id',
			$this->fb_post_id,
		])->andFilterWhere([
			'like',
			'content',
			$this->content,
		]);
		return $dataProvider;
	}

	public function data($params, $page_id) {
		$query = Post::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'sort'       => [
				'defaultOrder' => ['id' => SORT_DESC],
			],
			'pagination' => ['pageSize' => 10],
		]);
		$query->joinWith('fanpages');
		$query->andFilterWhere(['fanpage.id' => $page_id]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'                   => $this->id,
			'total_comment'        => $this->total_comment,
			'total_parent_comment' => $this->total_parent_comment,
			'total_like'           => $this->total_like,
			'total_share'          => $this->total_share,
			'created_date'         => $this->created_date,
			'fanpage_id'           => $this->fanpage_id,
			'total_order'          => $this->total_order,
			'is_auto_comment'      => $this->is_auto_comment,
		]);
		$query->andFilterWhere([
			'like',
			'fb_post_id',
			$this->fb_post_id,
		])->andFilterWhere([
			'like',
			'content',
			$this->content,
		]);
		return $dataProvider;
	}
}
