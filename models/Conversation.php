<?php

namespace app\models;

use dosamigos\taggable\Taggable;
use Yii;

/**
 * This is the model class for table "conversation".
 *
 * @property integer $id
 * @property integer $referen_id
 * @property integer $type
 * @property integer $fb_user_id
 * @property integer $user_id
 * @property integer $fanpage_id
 * @property integer $post_id
 * @property integer $company_id
 * @property integer $status
 * @property string $tag_id
 * @property string $tags
 * @property string $created_date
 *
 * @property Company $company
 * @property Fanpage $fanpage
 * @property User $user
 */
class Conversation extends \yii\db\ActiveRecord
{
    public $tagName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conversation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tagName'], 'safe'],
            [['referen_id', 'type', 'fb_user_id', 'user_id', 'fanpage_id', 'post_id', 'company_id', 'status'], 'integer'],
            [['company_id'], 'required'],
            [['created_date'], 'safe'],
            [['tag_id', 'tags', 'tagName'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referen_id' => 'Referen ID',
            'type' => 'Type',
            'fb_user_id' => 'Fb User ID',
            'user_id' => 'User ID',
            'fanpage_id' => 'Fanpage ID',
            'post_id' => 'Post ID',
            'company_id' => 'Company ID',
            'status' => 'Status',
            'tag_id' => 'Tag ID',
            'tags' => 'Tags',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getTag(){
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('comment', ['referen_id' => 'id']);
    }
}
