<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Category;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class CategorySearch extends Category {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'parent_id',
					'status',
					'quantity',
					'total_quantity_sale',
					'fanpage_id',
					'company_id',
				],
				'integer',
			],
			[
				[
					'name',
					'description',
					'create_date',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Category::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => ['id' => SORT_DESC],
			],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'category.id'         => $this->id,
			'parent_id'           => $this->parent_id,
			'status'              => $this->status,
			'quantity'            => $this->quantity,
			'total_quantity_sale' => $this->total_quantity_sale,
			'create_date'         => $this->create_date,
			'fanpage_id'          => $this->fanpage_id,
			'company_id'          => $this->user->company_id,
		]);
		$query->andFilterWhere([
			'like',
			'name',
			$this->name,
		])->andFilterWhere([
			'like',
			'description',
			$this->description,
		]);
		return $dataProvider;
	}

	public function getInfo($params, $attribute = null) {
		$query = Category::find();
		$query->joinWith('products');
		$query->joinWith('products.orderDetails');
		$this->load($params);
		$query->andFilterWhere([
			'category.id'         => $this->id,
			'parent_id'           => $this->parent_id,
			'status'              => $this->status,
			'quantity'            => $this->quantity,
			'total_quantity_sale' => $this->total_quantity_sale,
			'create_date'         => $this->create_date,
			'fanpage_id'          => $this->fanpage_id,
			'company_id'          => $this->user->company_id,
		]);
		$query->andFilterWhere([
			'like',
			'name',
			$this->name,
		])->andFilterWhere([
			'like',
			'description',
			$this->description,
		]);
		switch($attribute) {
			case 'product':
				$info = $query->count('product.id');
				break;
			case  'money_sale':
				$info = $query->sum('order_detail.total_price') ? $query->sum('order_detail.total_price') : 0;
				break;
			case 'quantity':
				$info = $query->sum('category.quantity') != null ? $query->sum('category.quantity') : 0;
				break;
			case  'quantity_sale':
				$info = $query->sum('category.total_quantity_sale') ? $query->sum('category.total_quantity_sale') : 0;
				break;
			default:
				$info = 0;
		}
		return $info;
	}
}
