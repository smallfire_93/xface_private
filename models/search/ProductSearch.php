<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductrSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'category_id',
					'quantity',
					'quantity_sale',
					'status',
					'provider_id',
				],
				'integer',
			],
			[
				[
					'name',
					'code',
					'image',
					'color',
					'content',
					'size',
					'weight',
					'base_price',
					'retail_price',
					'whole_price',
					'product',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Product::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => ['id' => SORT_DESC],
			],
		]);
		$query->joinWith('posts')->joinWith('orderDetails');
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'            => $this->id,
			'category_id'   => $this->category_id,
			'base_price'    => $this->base_price,
			'retail_price'  => $this->retail_price,
			'whole_price'   => $this->whole_price,
			'quantity'      => $this->quantity,
			'quantity_sale' => $this->quantity_sale,
			'product.status'        => $this->status,
			'weight'        => $this->weight,
			'size'          => $this->size,
			'provider_id'   => $this->provider_id,
		]);
		$query->andFilterWhere([
			'like',
			'product.name',
			$this->name,
		])->andFilterWhere([
			'like',
			'code',
			$this->code,
		])->andFilterWhere([
			'like',
			'image',
			$this->image,
		])->andFilterWhere([
			'like',
			'color',
			$this->color,
		])->andFilterWhere([
			'like',
			'content',
			$this->content,
		]);
		return $dataProvider;
	}

	public function getInfo($params, $attribute) {
		$query = Product::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => ['id' => SORT_DESC],
			],
		]);
		$query->joinWith('posts')->joinWith('orderDetails');
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'            => $this->id,
			'category_id'   => $this->category_id,
			'base_price'    => $this->base_price,
			'retail_price'  => $this->retail_price,
			'whole_price'   => $this->whole_price,
			'quantity'      => $this->quantity,
			'quantity_sale' => $this->quantity_sale,
			'product.status'        => $this->status,
			'weight'        => $this->weight,
			'size'          => $this->size,
			'provider_id'   => $this->provider_id,
		]);
		$query->andFilterWhere([
			'like',
			'product.name',
			$this->name,
		])->andFilterWhere([
			'like',
			'code',
			$this->code,
		])->andFilterWhere([
			'like',
			'image',
			$this->image,
		])->andFilterWhere([
			'like',
			'color',
			$this->color,
		])->andFilterWhere([
			'like',
			'content',
			$this->content,
		]);
		$info = 0;
		switch($attribute) {
			case 'count':
				$total = $query->count();
				break;
			case 'total_sale':
				$total = $query->sum('product.quantity_sale');
				break;
			case  'total_post':
				$total = $query->count('post.id');
				break;
			case 'total_money':
				$total = $query->sum('order_detail.total_price');
				break;
			default :
				$total = 0;
		}
		$info = $total == null ? 0 : $total;
		return $info;
	}
}
