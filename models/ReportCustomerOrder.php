<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_customer_order".
 *
 * @property integer $id
 * @property string $created_date
 * @property integer $customer_id
 * @property string $customer_name
 * @property integer $total_order
 * @property integer $total_order_pending
 * @property integer $total_order_finish
 * @property integer $total_order_feedback
 * @property integer $total_order_cancel
 * @property integer $fanpage_id
 * @property integer $company_id
 *
 * @property Company $company
 * @property Customer $customer
 * @property Fanpage $fanpage
 */
class ReportCustomerOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_customer_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date'], 'safe'],
            [['customer_id', 'customer_name', 'fanpage_id', 'company_id'], 'required'],
            [['customer_id', 'total_order', 'total_order_pending', 'total_order_finish', 'total_order_feedback', 'total_order_cancel', 'fanpage_id', 'company_id'], 'integer'],
            [['customer_name'], 'string', 'max' => 120],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'customer_id' => 'Customer ID',
            'customer_name' => 'Customer Name',
            'total_order' => 'Total Order',
            'total_order_pending' => 'Total Order Pending',
            'total_order_finish' => 'Total Order Finish',
            'total_order_feedback' => 'Total Order Feedback',
            'total_order_cancel' => 'Total Order Cancel',
            'fanpage_id' => 'Fanpage ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }
}
