<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_fanpage".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $fanpage_id
 *
 * @property Fanpage $fanpage
 * @property Product $product
 */
class ProductFanpage extends \yii\db\ActiveRecord
{
    public $product_name, $price, $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_fanpage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'fanpage_id'], 'required'],
            [['product_id', 'fanpage_id'], 'integer'],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'fanpage_id' => 'Fanpage ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
