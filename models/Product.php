<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer              $id
 * @property integer              $category_id
 * @property string               $name
 * @property string               $code
 * @property string               $image
 * @property integer              $base_price
 * @property integer              $retail_price
 * @property integer              $whole_price
 * @property integer              $quantity
 * @property integer              $quantity_sale
 * @property integer              $status
 * @property integer              $weight
 * @property integer              $size
 * @property string               $color
 * @property string               $content
 * @property integer              $provider_id
 *
 * @property OrderDetail[]        $orderDetails
 * @property Category             $category
 * @property ProductFanpage[]     $productFanpages
 * @property ProductPost[]        $productPosts
 * @property Post[]               $posts
 * @property Post[]               $postData
 * @property ReportOrderProduct[] $reportOrderProducts
 * @property ReportProduct[]      $reportProducts
 */
class Product extends Model {

	/**
	 * @inheritdoc
	 */
	const STATUS = [
		'Hết hàng',
		'Còn hàng',
		'Gần hết hàng',
	];

	public $post;

	public $product_image;

	public static function tableName() {
		return 'product';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'category_id',
					'status',
					'quantity',
					'name',
				],
				'required',
			],
			[
				[
					'category_id',
					'quantity',
					'quantity_sale',
					'status',
					'provider_id',
				],
				'integer',
			],
			[
				['name'],
				'string',
				'max' => 120,
			],
			[
				[
					'post',
					'base_price',
					'retail_price',
					'whole_price',
					'weight',
				],
				'safe',
			],
			[
				[
					'code',
					'content',
					'image',
				],
				'string',
				'max' => 255,
			],
			[
				['product_image'],
				'file',
				//				'skipOnEmpty' => false,
				'extensions' => 'jpg, gif, png',
			],
			[
				['color'],
				'string',
				'max' => 100,
			],
			[
				['category_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Category::className(),
				'targetAttribute' => ['category_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'category_id'   => 'Danh mục',
			'name'          => 'Tên sản phẩm',
			'code'          => 'Mã sản phẩm',
			'product_image' => 'Ảnh',
			'base_price'    => 'Giá nhập',
			'retail_price'  => 'Giá bán lẻ',
			'whole_price'   => 'Giá bán buôn',
			'quantity'      => 'Số lượng',
			'quantity_sale' => 'Số lượng đã bán',
			'status'        => 'Trạng thái',
			'weight'        => 'Khố lượng',
			'size'          => 'Kích cỡ',
			'color'         => 'Màu sắc',
			'content'       => 'Mô tả',
			'provider_id'   => 'Nhà cung cấp',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderDetails() {
		return $this->hasMany(OrderDetail::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory() {
		return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductFanpages() {
		return $this->hasMany(ProductFanpage::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductPosts() {
		return $this->hasMany(ProductPost::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportOrderProducts() {
		return $this->hasMany(ReportOrderProduct::className(), ['product_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportProducts() {
		return $this->hasMany(ReportProduct::className(), ['product_id' => 'id']);
	}

	public function getPosts() {
		return $this->hasMany(Post::className(), ['id' => 'post_id'])->viaTable('product_post', ['post_id' => 'id']);
	}

	public function getPostData() {
		return $this->hasMany(Post::className(), ['id' => 'post_id'])->via('productPosts');
	}
}
