<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportShipping;

/**
 * ReportShippingSearch represents the model behind the search form of `app\models\ReportShipping`.
 */
class ReportShippingSearch extends ReportShipping
{
    /**
     * @inheritdoc
     */
    public $start_time, $end_time;
    public function rules()
    {
        return [
            [['id', 'total_customer', 'total_order_finish', 'total_order_pending', 'total_order_feedback', 'total_money', 'fanpage_id', 'company_id', 'transport_id', 'total_order_cod', 'total_order_cod_finish', 'total_order_cod_feedback', 'total_money_debt', 'total_money_real','total_not_meet'], 'integer'],
            [['created_date', 'transport_name','start_time','end_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportShipping::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'total_customer' => $this->total_customer,
            'total_order_finish' => $this->total_order_finish,
            'total_order_pending' => $this->total_order_pending,
            'total_order_feedback' => $this->total_order_feedback,
            'total_not_meet'    =>  $this->total_not_meet,
            'total_money' => $this->total_money,
            'fanpage_id' => $this->fanpage_id,
            'company_id' => $this->company_id,
            'transport_id' => $this->transport_id,
            'total_order_cod' => $this->total_order_cod,
            'total_order_cod_finish' => $this->total_order_cod_finish,
            'total_order_cod_feedback' => $this->total_order_cod_feedback,
            'total_money_debt' => $this->total_money_debt,
            'total_money_real' => $this->total_money_real,
        ]);

        $query->andFilterWhere(['>=','created_date', $this->start_time]);
        $query->andFilterWhere(['<=','created_date', $this->end_time]);

        $query->andFilterWhere(['like', 'transport_name', $this->transport_name]);

        return $dataProvider;
    }
}
