<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportInbox;

/**
 * ReportInboxSearch represents the model behind the search form of `app\models\ReportInbox`.
 */
class ReportInboxSearch extends ReportInbox
{
    /**
     * @inheritdoc
     */
    public $start_time,$end_time;
    public function rules()
    {
        return [
            [['id', 'fanpage_id', 'company_id', 'total_inbox', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7', 'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12'], 'integer'],
            [['inbox_date'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportInbox::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fanpage_id' => $this->fanpage_id,
            'company_id' => $this->company_id,
            'total_inbox' => $this->total_inbox,
            'hour_1' => $this->hour_1,
            'hour_2' => $this->hour_2,
            'hour_3' => $this->hour_3,
            'hour_4' => $this->hour_4,
            'hour_5' => $this->hour_5,
            'hour_6' => $this->hour_6,
            'hour_7' => $this->hour_7,
            'hour_8' => $this->hour_8,
            'hour_9' => $this->hour_9,
            'hour_10' => $this->hour_10,
            'hour_11' => $this->hour_11,
            'hour_12' => $this->hour_12,
            'inbox_date'    =>  $this->inbox_date,
        ]);
        $query->andFilterWhere(['>=','inbox_date', $this->start_time]);
        $query->andFilterWhere(['<=','inbox_date', $this->end_time]);
        
        return $dataProvider;
    }
}
