<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id_create',
					'user_id_process',
					'quantity',
					'total_money',
					'customer_id',
					'payment_status',
					'shipping_status',
					'payment_id',
					'fb_user_id',
					'shipping_fee',
					'code_fee',
					'transport_id',
					'discount',
					'status',
				],
				'integer',
			],
			[
				[
					'code',
					'customer_note',
					'tracking',
					'export_date',
					'receiver_date',
					'address',
					'phone',
					'from_date',
					'to_date',
					'customer_email',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Order::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => ['id' => SORT_DESC],
			],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'              => $this->id,
			'user_id_create'  => $this->user_id_create,
			'user_id_process' => $this->user_id_process,
			'quantity'        => $this->quantity,
			'total_money'     => $this->total_money,
			'customer_id'     => $this->customer_id,
			'payment_status'  => $this->payment_status,
			'shipping_status' => $this->shipping_status,
			'payment_id'      => $this->payment_id,
			'fb_user_id'      => $this->fb_user_id,
			'export_date'     => $this->export_date,
			'receiver_date'   => $this->receiver_date,
			'shipping_fee'    => $this->shipping_fee,
			'code_fee'        => $this->code_fee,
			'transport_id'    => $this->transport_id,
			'discount'        => $this->discount,
			'status'          => $this->status,
		]);
		$query->andFilterWhere([
			'like',
			'code',
			$this->code,
		])->andFilterWhere([
			'like',
			'customer_note',
			$this->customer_note,
		])->andFilterWhere([
			'like',
			'tracking',
			$this->tracking,
		])->andFilterWhere([
			'like',
			'address',
			$this->address,
		])->andFilterWhere([
			'like',
			'phone',
			$this->phone,
		]);
		return $dataProvider;
	}
}
