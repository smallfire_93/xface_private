<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_inbox".
 *
 * @property integer $id
 * @property integer $fanpage_id
 * @property integer $company_id
 * @property integer $total_comment
 * @property integer $hour_1
 * @property integer $hour_2
 * @property integer $hour_3
 * @property integer $hour_4
 * @property integer $hour_5
 * @property integer $hour_6
 * @property integer $hour_7
 * @property integer $hour_8
 * @property integer $hour_9
 * @property integer $hour_10
 * @property integer $hour_11
 * @property integer $hour_12
 *
 * @property Company $company
 * @property Fanpage $fanpage
 */
class ReportInbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_inbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fanpage_id', 'company_id'], 'required'],
            [['fanpage_id', 'company_id', 'total_inbox', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7', 'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12'], 'integer'],
            [['inbox_date'],'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fanpage_id' => 'Fanpage ID',
            'company_id' => 'Company ID',
            'total_inbox' => 'Total Inbox',
            'hour_1' => 'Hour 1',
            'hour_2' => 'Hour 2',
            'hour_3' => 'Hour 3',
            'hour_4' => 'Hour 4',
            'hour_5' => 'Hour 5',
            'hour_6' => 'Hour 6',
            'hour_7' => 'Hour 7',
            'hour_8' => 'Hour 8',
            'hour_9' => 'Hour 9',
            'hour_10' => 'Hour 10',
            'hour_11' => 'Hour 11',
            'hour_12' => 'Hour 12',
            'inbox_date'    =>  'Inbox Date'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }
}
