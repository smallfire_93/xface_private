<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "post_kpi".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $type
 * @property integer $total_comment
 * @property integer $total_like
 * @property integer $total_share
 * @property string  $start_time
 * @property string  $end_date
 *
 * @property Post    $post
 */
class PostKpi extends \yii\db\ActiveRecord {

	const KPI_TYPE = [
		'Ngày',
		'Tuần',
		'Tháng',
		'Khoảng thời gian',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'post_kpi';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['post_id'],
				'required',
			],
			[
				[
					'post_id',
					'type',
					'total_comment',
					'total_like',
					'total_share',
				],
				'integer',
			],
			[
				[
					'start_time',
					'end_date',
				],
				'safe',
			],
			[
				['post_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Post::className(),
				'targetAttribute' => ['post_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'post_id'       => 'Post ID',
			'type'          => 'Type',
			'total_comment' => 'Total Comment',
			'total_like'    => 'Total Like',
			'total_share'   => 'Total Share',
			'start_time'    => 'Start Time',
			'end_date'      => 'End Date',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPost() {
		return $this->hasOne(Post::className(), ['id' => 'post_id']);
	}
}
