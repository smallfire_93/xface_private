<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer           $id
 * @property string            $fb_post_id
 * @property string            $content
 * @property integer           $total_comment
 * @property integer           $total_parent_comment
 * @property integer           $total_like
 * @property integer           $total_share
 * @property string            $created_date
 * @property string            $reply_content
 * @property string            $link_share
 * @property integer           $fanpage_id
 * @property integer           $type_attach
 * @property integer           $is_del
 * @property integer           $total_order
 * @property integer           $is_auto_comment
 *
 * @property Comment[]         $comments
 * @property Fanpage[]         $fanpages
 * @property Product[]         $productData
 * @property PostKpi[]         $postKpis
 * @property PostSchedule[]    $postSchedules
 * @property ProductPost[]     $productPosts
 * @property ReportOrderPost[] $reportOrderPosts
 */
class Post extends Model {

	public $product;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'post';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'content',
					'fanpage_id',
				],
				'required',
			],
			[
				[
					'content',
					'reply_content',
					'link_share',
				],
				'string',
			],
			[
				[
					'total_comment',
					'total_parent_comment',
					'total_like',
					'total_share',
					'fanpage_id',
					'total_order',
					'is_auto_comment',
					'type_attach',
				],
				'integer',
			],
			[
				[
					'created_date',
					'product',
				],
				'safe',
			],
			[
				['fb_post_id'],
				'string',
				'max' => 120,
			],
			[
				['fanpage_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Fanpage::className(),
				'targetAttribute' => ['fanpage_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'                   => 'ID',
			'fb_post_id'           => 'Fb Post ID',
			'content'              => 'Nội dung',
			'total_comment'        => 'Tổng số comment',
			'total_parent_comment' => 'Total Parent Comment',
			'total_like'           => 'Tổng lượt like',
			'total_share'          => 'Tổng lượt share',
			'created_date'         => 'Created Date',
			'fanpage_id'           => 'Fanpage ID',
			'total_order'          => 'Tổng số đơn hàng',
			'is_auto_comment'      => 'Is Auto Comment',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getComments() {
		return $this->hasMany(Comment::className(), ['post_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFanpages() {
		return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPostKpis() {
		return $this->hasMany(PostKpi::className(), ['post_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPostSchedules() {
		return $this->hasMany(PostSchedule::className(), ['post_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductPosts() {
		return $this->hasMany(ProductPost::className(), ['post_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportOrderPosts() {
		return $this->hasMany(ReportOrderPost::className(), ['post_id' => 'id']);
	}

	public function getProducts() {
		return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_post', ['product_id' => 'id']);
	}

	public function getProductData() {
		return $this->hasMany(Product::className(), ['id' => 'product_id'])->via('productPosts');
	}

	public function getFanpage() {
		return $this->hasMany(Fanpage::className(), ['id' => 'fanpage_id']);
	}
}
