<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "page_config".
 *
 * @property int     $id
 * @property int     $fanpage_id
 * @property int     $post_id
 * @property int     $auto_reply_comment
 * @property int     $auto_like_comment
 * @property int     $auto_hidden_comment_all
 * @property int     $auto_hidden_comment_phone
 * @property int     $auto_hidden_comment_keyword
 * @property int     $auto_reply_inbox
 * @property int     $type
 * @property string  $comment_keyword
 * @property string  $reply_inbox_content
 *
 * @property Fanpage $fanpage
 */
class PageConfig extends \yii\db\ActiveRecord {

	const TYPE      = [
		1 => 'Page',
		2 => 'post',
	];

	const TYPE_POST = 2;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'page_config';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['fanpage_id'],
				'required',
			],
			[
				[
					'fanpage_id',
					'post_id',
					'auto_reply_comment',
					'auto_like_comment',
					'auto_hidden_comment_all',
					'auto_hidden_comment_phone',
					'auto_hidden_comment_keyword',
					'auto_reply_inbox',
					'type',
				],
				'integer',
			],
			[
				[
					'comment_keyword',
					'reply_inbox_content',
				],
				'string',
				'max' => 1024,
			],
			[
				['type'],
				'integer',
			],
			[
				['fanpage_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Fanpage::className(),
				'targetAttribute' => ['fanpage_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'                          => 'ID',
			'fanpage_id'                  => 'Fanpage ID',
			'post_id'                     => 'Post ID',
			'auto_reply_comment'          => 'Auto Reply Comment',
			'auto_like_comment'           => 'Auto Like Comment',
			'auto_hidden_comment_all'     => 'Auto Hidden Comment All',
			'auto_hidden_comment_phone'   => 'Auto Hidden Comment Phone',
			'auto_hidden_comment_keyword' => 'Auto Hidden Comment Keyword',
			'auto_reply_inbox'            => 'Auto Reply Inbox',
			'comment_keyword'             => 'Comment Keyword',
			'reply_inbox_content'         => 'Reply Inbox Content',
			'comment_content'             => 'Comment Content',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFanpage() {
		return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
	}
}
