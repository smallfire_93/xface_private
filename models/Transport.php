<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transport".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $status
 * @property string $image
 * @property string $website
 * @property string $username
 * @property string $password
 * @property string $api_url
 * @property string $token
 * @property integer $company_id
 *
 * @property ReportShipping[] $reportShippings
 * @property Company $company
 */
class Transport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'company_id'], 'integer'],
            [['company_id'], 'required'],
            [['name', 'username'], 'string', 'max' => 64],
            [['phone'], 'string', 'max' => 30],
            [['image', 'website', 'api_url', 'token'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 120],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'status' => 'Status',
            'image' => 'Image',
            'website' => 'Website',
            'username' => 'Username',
            'password' => 'Password',
            'api_url' => 'Api Url',
            'token' => 'Token',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportShippings()
    {
        return $this->hasMany(ReportShipping::className(), ['transport_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
