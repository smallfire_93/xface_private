<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property integer $status
 * @property string $fb_id
 * @property string $fb_token
 * @property string $fb_token_expire
 * @property integer $user_id
 * @property string $instagram_id
 * @property string $instagram_token
 * @property string $instagram_token_expire
 *
 * @property Category[] $categories
 * @property User $user
 * @property Conversation[] $conversations
 * @property Customer[] $customers
 * @property Fanpage[] $fanpages
 * @property Promotion[] $promotions
 * @property ReportComment[] $reportComments
 * @property ReportCustomer[] $reportCustomers
 * @property ReportCustomerOrder[] $reportCustomerOrders
 * @property ReportInbox[] $reportInboxes
 * @property ReportOrderPost[] $reportOrderPosts
 * @property ReportOrderProduct[] $reportOrderProducts
 * @property ReportProduct[] $reportProducts
 * @property ReportRevenue[] $reportRevenues
 * @property ReportSale[] $reportSales
 * @property ReportShipping[] $reportShippings
 * @property Tag[] $tags
 * @property Transport[] $transports
 * @property User[] $users
 */
class Company extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'fb_token_expire', 'user_id'], 'required'],
            [['status', 'user_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['address'], 'string', 'max' => 120],
            [['fb_id', 'fb_token', 'fb_token_expire', 'instagram_id', 'instagram_token', 'instagram_token_expire'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'status' => 'Status',
            'fb_id' => 'Fd ID',
            'fb_token' => 'Fb Token',
            'fb_token_expire' => 'Fb Token Expire',
            'user_id' => 'User ID',
            'instagram_id' => 'Instagram ID',
            'instagram_token' => 'Instagram Token',
            'instagram_token_expire' => 'Instagram Token Expire',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversations()
    {
        return $this->hasMany(Conversation::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpages()
    {
        return $this->hasMany(Fanpage::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotions()
    {
        return $this->hasMany(Promotion::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportComments()
    {
        return $this->hasMany(ReportComment::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportCustomers()
    {
        return $this->hasMany(ReportCustomer::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportCustomerOrders()
    {
        return $this->hasMany(ReportCustomerOrder::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportInboxes()
    {
        return $this->hasMany(ReportInbox::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportOrderPosts()
    {
        return $this->hasMany(ReportOrderPost::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportOrderProducts()
    {
        return $this->hasMany(ReportOrderProduct::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportProducts()
    {
        return $this->hasMany(ReportProduct::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportRevenues()
    {
        return $this->hasMany(ReportRevenue::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportSales()
    {
        return $this->hasMany(ReportSale::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportShippings()
    {
        return $this->hasMany(ReportShipping::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransports()
    {
        return $this->hasMany(Transport::className(), ['company_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }
}
