<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PageConfig;

/**
 * PageConfigSearch represents the model behind the search form of `app\models\PageConfig`.
 */
class PageConfigSearch extends PageConfig
{
    /**
     * @inheritdoc
     */
    public $start_time, $end_time;
    public function rules()
    {
        return [
            [['id', 'fanpage_id', 'post_id', 'auto_reply_comment', 'auto_like_comment', 'auto_hidden_comment_all', 'auto_hidden_comment_phone', 'auto_hidden_comment_keyword', 'auto_reply_inbox'], 'integer'],
            [['comment_keyword', 'reply_inbox_content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageConfig::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fanpage_id' => $this->fanpage_id,
            'fb_post_id' => $this->fb_post_id,
            'auto_reply_comment' => $this->auto_reply_comment,
            'auto_like_comment' => $this->auto_like_comment,
            'auto_hidden_comment_all' => $this->auto_hidden_comment_all,
            'auto_hidden_comment_phone' => $this->auto_hidden_comment_phone,
            'auto_hidden_comment_keyword' => $this->auto_hidden_comment_keyword,
            'auto_reply_inbox' => $this->auto_reply_inbox,
        ]);

        $query->andFilterWhere(['>=','start_time', $this->start_time]);
        $query->andFilterWhere(['<=','start_time', $this->end_time]);

        $query->andFilterWhere(['like', 'comment_keyword', $this->comment_keyword])
            ->andFilterWhere(['like', 'reply_inbox_content', $this->reply_inbox_content]);

        return $dataProvider;
    }
}
