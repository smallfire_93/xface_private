<?php
namespace app\models;

use app\components\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "fanpage".
 *
 * @property integer               $id
 * @property integer               $company_id
 * @property integer               $fb_page_id
 * @property string                $access_token
 * @property string                $expire_date
 * @property integer               $is_join
 * @property string                $create
 * @property string                $name
 *
 * @property Category[]            $categories
 * @property Comment[]             $comments
 * @property Conversation[]        $conversations
 * @property Company               $company
 * @property Product               $products
 * @property Inbox[]               $inboxes
 * @property PageConfig[]          $pageConfigs
 * @property PageConfigReply[]     $pageConfigReplies
 * @property Post[]                $posts
 * @property User[]                $users
 * @property ProductFanpage[]      $productFanpages
 * @property PromotionPage[]       $promotionPages
 * @property ReportComment[]       $reportComments
 * @property ReportCustomer[]      $reportCustomers
 * @property ReportCustomerOrder[] $reportCustomerOrders
 * @property ReportInbox[]         $reportInboxes
 * @property ReportProduct[]       $reportProducts
 * @property ReportRevenue[]       $reportRevenues
 * @property ReportShipping[]      $reportShippings
 * @property UserFanpage[]         $userFanpages
 */
class Fanpage extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'fanpage';
	}

	const IS_JOIN  = [
		'Chưa join',
		'Đã join',
	];

	const JOINED   = 1;

	const NOT_JOIN = 0;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['company_id'],
				'required',
			],
			[
				[
					'company_id',
					'fb_page_id',
					'is_join',
				],
				'integer',
			],
			[
				[
					'expire_date',
					'create',
				],
				'safe',
			],
			[
				[
					'access_token',
					'name',
				],
				'string',
				'max' => 255,
			],
			[
				['company_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Company::className(),
				'targetAttribute' => ['company_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'company_id'   => 'Company ID',
			'page_id'      => 'Page ID',
			'access_token' => 'Access Token',
			'expire_date'  => 'Expire Date',
			'is_join'      => 'Is Join',
			'create'       => 'Create',
			'name'         => 'Name',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategories() {
		return $this->hasMany(Category::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getComments() {
		return $this->hasMany(Comment::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getConversations() {
		return $this->hasMany(Conversation::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCompany() {
		return $this->hasOne(Company::className(), ['id' => 'company_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInboxes() {
		return $this->hasMany(Inbox::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPageConfigs() {
		return $this->hasMany(PageConfig::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPageConfigReplies() {
		return $this->hasMany(PageConfigReply::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPosts() {
		return $this->hasMany(Post::className(), ['fanpage_id' => 'id']);
	}

	public function getUsers() {
		return $this->hasMany(User::className(), ['id' => 'user_id'])->via('userFanpages');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductFanpages() {
		return $this->hasMany(ProductFanpage::className(), ['fanpage_id' => 'id']);
	}

	public function getProducts() {
		return $this->hasMany(Product::className(), ['id' => 'product_id'])->via('productFanpages');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPromotionPages() {
		return $this->hasMany(PromotionPage::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportComments() {
		return $this->hasMany(ReportComment::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportCustomers() {
		return $this->hasMany(ReportCustomer::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportCustomerOrders() {
		return $this->hasMany(ReportCustomerOrder::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportInboxes() {
		return $this->hasMany(ReportInbox::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportProducts() {
		return $this->hasMany(ReportProduct::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportRevenues() {
		return $this->hasMany(ReportRevenue::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getReportShippings() {
		return $this->hasMany(ReportShipping::className(), ['fanpage_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserFanpages() {
		return $this->hasMany(UserFanpage::className(), ['fanpage_id' => 'id']);
	}

	public function setActive() {
		$fanpages      = $this->user->fanpages;
		$fanpage_array = ArrayHelper::map($fanpages, 'id', 'id');
		$class_active  = '';
		if($fanpages) {
			if(reset($fanpage_array) == $this->id) {
				$class_active = "active";
			}
		}
		return $class_active;
	}
}
