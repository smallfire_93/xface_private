<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_shipping".
 *
 * @property integer $id
 * @property string $created_date
 * @property integer $total_customer
 * @property integer $total_order_finish
 * @property integer $total_order_pending
 * @property integer $total_order_feedback
 * @property integer $total_money
 * @property integer $fanpage_id
 * @property integer $company_id
 * @property integer $transport_id
 * @property string $transport_name
 * @property integer $total_order_cod
 * @property integer $total_order_cod_finish
 * @property integer $total_order_cod_feedback
 * @property integer $total_money_debt
 * @property integer $total_money_real
 *
 * @property Company $company
 * @property Fanpage $fanpage
 * @property Transport $transport
 */
class ReportShipping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_shipping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date'], 'safe'],
            [['total_customer','total_pending_feedback', 'total_order_finish', 'total_order_pending', 'total_order_feedback', 'total_money', 'fanpage_id', 'company_id', 'transport_id', 'total_order_cod', 'total_order_cod_finish', 'total_order_cod_feedback', 'total_money_debt', 'total_money_real','total_not_meet'], 'integer'],
            [['fanpage_id', 'company_id'], 'required'],
            [['transport_name'], 'string', 'max' => 50],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['transport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transport::className(), 'targetAttribute' => ['transport_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'total_customer' => 'Total Customer',
            'total_order_finish' => 'Total Order Finish',
            'total_order_pending' => 'Total Order Pending',
            'total_order_feedback' => 'Total Order Feedback',
            'total_not_meet'    =>  'Total Not Meet',
            'total_money' => 'Total Money',
            'fanpage_id' => 'Fanpage ID',
            'company_id' => 'Company ID',
            'transport_id' => 'Transport ID',
            'transport_name' => 'Transport Name',
            'total_order_cod' => 'Total Order Cod',
            'total_order_cod_finish' => 'Total Order Cod Finish',
            'total_order_cod_feedback' => 'Total Order Cod Feedback',
            'total_money_debt' => 'Total Money Debt',
            'total_money_real' => 'Total Money Real',
            'total_pending_feedback'    =>  'Total_pending_feedback'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransport()
    {
        return $this->hasOne(Transport::className(), ['id' => 'transport_id']);
    }
}
