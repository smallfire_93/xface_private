<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer    $id
 * @property string     $name
 * @property integer    $parent_id
 * @property string     $description
 * @property integer    $status
 * @property integer    $quantity
 * @property integer    $total_quantity_sale
 * @property string     $create_date
 * @property integer    $fanpage_id
 * @property integer    $company_id
 *
 * @property Company    $company
 * @property Fanpage    $fanpage
 * @property Category   $parent
 * @property Category[] $categories
 * @property Product[]  $products
 */
class Category extends Model {

	const STATUS          = [
		'Inactive',
		'Active',
	];

	const STATUS_ACTIVE   = 1;

	const STATUS_INACTIVE = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'category';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'parent_id',
					'status',
					'quantity',
					'total_quantity_sale',
					'fanpage_id',
					'company_id',
				],
				'integer',
			],
			[
				['create_date'],
				'safe',
			],
//			[
//				[
//					'fanpage_id',
//					'company_id',
//				],
//				'required',
//			],
			[
				['name'],
				'string',
				'max' => 120,
			],
			[
				['description'],
				'string',
				'max' => 255,
			],
			[
				['company_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Company::className(),
				'targetAttribute' => ['company_id' => 'id'],
			],
			[
				['fanpage_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Fanpage::className(),
				'targetAttribute' => ['fanpage_id' => 'id'],
			],
			[
				['parent_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Category::className(),
				'targetAttribute' => ['parent_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'                  => 'ID',
			'name'                => 'Tên',
			'parent_id'           => 'Ngành hàng',
			'description'         => 'Mô tả',
			'status'              => 'Trạng thái',
			'quantity'            => 'Số lượng',
			'total_quantity_sale' => 'Tổng số lượng đã bán',
			'create_date'         => 'Create Date',
			'fanpage_id'          => 'Fanpage ID',
			'company_id'          => 'Company ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCompany() {
		return $this->hasOne(Company::className(), ['id' => 'company_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFanpage() {
		return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent() {
		return $this->hasOne(Category::className(), ['id' => 'parent_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategories() {
		return $this->hasMany(Category::className(), ['parent_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProducts() {
		return $this->hasMany(Product::className(), ['category_id' => 'id']);
	}

	public function getParentCategory() {
		$parent = $this->find()->where(['parent_id' => 0])->all();
		return $parent;
	}
}
