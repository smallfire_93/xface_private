<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $fb_comment_id
 * @property integer $fanpage_id
 * @property string $content
 * @property integer $status
 * @property string $created_date
 * @property integer $parent_id
 * @property integer $user_id_manager
 * @property integer $user_id_reply
 * @property integer $type
 * @property integer $is_hidden
 * @property integer $is_like
 * @property integer $fb_user_id
 *
 * @property Fanpage $fanpage
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'fanpage_id', 'status', 'parent_id', 'user_id_manager', 'user_id_reply', 'type', 'is_hidden', 'is_like', 'fb_user_id'], 'integer'],
            [['created_date'], 'safe'],
            [['content', 'fb_comment_id'], 'string', 'max' => 255],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'fb_comment_id' => 'Fb Comment ID',
            'fanpage_id' => 'Fanpage ID',
            'content' => 'Content',
            'status' => 'Status',
            'created_date' => 'Created Date',
            'parent_id' => 'Parent ID',
            'user_id_manager' => 'User Id Manager',
            'user_id_reply' => 'User Id Reply',
            'type' => 'Type',
            'is_hidden' => 'Is Hidden',
            'is_like' => 'Is Like',
            'fb_user_id' => 'Fb User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    public function getParent()
    {
        return $this->hasOne(Comment::className(), ['id' => 'parent_id']);
    }
}
