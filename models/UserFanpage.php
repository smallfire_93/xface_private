<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_fanpage".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $fanpage_id
 * @property integer $comment_quantity
 * @property integer $inbox_quantity
 *
 * @property Fanpage $fanpage
 * @property User $user
 */
class UserFanpage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_fanpage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'fanpage_id'], 'required'],
            [['user_id', 'fanpage_id', 'comment_quantity', 'inbox_quantity'], 'integer'],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'fanpage_id' => 'Fanpage ID',
            'comment_quantity' => 'Comment Quantity',
            'inbox_quantity' => 'Inbox Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
