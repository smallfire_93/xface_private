<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_config_reply".
 *
 * @property integer $id
 * @property integer $fanpage_id
 * @property integer $type
 * @property string $keyword
 * @property string $reply_content
 *
 * @property Fanpage $fanpage
 */
class PageConfigReply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_config_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fanpage_id'], 'required'],
            [['fanpage_id', 'type'], 'integer'],
            [['keyword', 'reply_content'], 'string', 'max' => 2000],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fanpage_id' => 'Fanpage ID',
            'type' => 'Type',
            'keyword' => 'Keyword',
            'reply_content' => 'Reply Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }
}
