<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fanpage;

/**
 * FanpageSearch represents the model behind the search form of `app\models\Fanpage`.
 */
class FanpageSearch extends Fanpage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'page_id', 'is_join'], 'integer'],
            [['access_token', 'expire_date', 'create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fanpage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
	            'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'page_id' => $this->page_id,
            'expire_date' => $this->expire_date,
            'is_join' => $this->is_join,
            'create' => $this->create,
        ]);

        $query->andFilterWhere(['like', 'access_token', $this->access_token]);

        return $dataProvider;
    }
}
