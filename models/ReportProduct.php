<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_product".
 *
 * @property integer $id
 * @property string $created_date
 * @property integer $product_id
 * @property string $product_name
 * @property integer $in_stock
 * @property integer $total_order
 * @property integer $total_order_finish
 * @property integer $total_money
 * @property integer $company_id
 * @property integer $fanpage_id
 *
 * @property Company $company
 * @property Fanpage $fanpage
 * @property Product $product
 */
class ReportProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date'], 'safe'],
            [['product_id', 'company_id', 'fanpage_id'], 'required'],
            [['product_id', 'in_stock', 'total_order', 'total_order_finish', 'total_money', 'company_id', 'fanpage_id'], 'integer'],
            [['product_name'], 'string', 'max' => 50],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['fanpage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fanpage::className(), 'targetAttribute' => ['fanpage_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'product_id' => 'Product ID',
            'product_name' => 'Product Name',
            'in_stock' => 'In Stock',
            'total_order' => 'Total Order',
            'total_order_finish' => 'Total Order Finish',
            'total_money' => 'Total Money',
            'company_id' => 'Company ID',
            'fanpage_id' => 'Fanpage ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFanpage()
    {
        return $this->hasOne(Fanpage::className(), ['id' => 'fanpage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
