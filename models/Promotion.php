<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promotion".
 *
 * @property int $id
 * @property string $name
 * @property string $start_time
 * @property string $end_time
 * @property int $promotion_type
 * @property int $discount_amount
 * @property int $product_id
 * @property int $product_id_promotion
 * @property int $promotion_quantity
 * @property int $product_quantity
 * @property string $created_date
 * @property int $created_id
 * @property int $company_id
 *
 * @property Company $company
 * @property User $created
 * @property PromotionPage[] $promotionPages
 */
class Promotion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time', 'promotion_type', 'product_id', 'created_id', 'company_id'], 'required'],
            [['start_time', 'end_time', 'created_date'], 'safe'],
            [['promotion_type', 'discount_amount', 'product_id', 'product_id_promotion', 'promotion_quantity', 'product_quantity', 'created_id', 'company_id'], 'integer'],
            [['name'], 'string', 'max' => 120],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['created_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'start_time' => 'Thời gian bắt đầu: ',
            'end_time' => 'Thời gian kết thúc: ',
            'promotion_type' => 'Promotion Type',
            'discount_amount' => 'Discount Amount',
            'product_id' => 'Product ID',
            'product_id_promotion' => 'Product Id Promotion',
            'promotion_quantity' => 'Promotion Quantity',
            'product_quantity' => 'Product Quantity',
            'created_date' => 'Created Date',
            'created_id' => 'Created ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotionPages()
    {
        return $this->hasMany(PromotionPage::className(), ['promotion_id' => 'id']);
    }
}
