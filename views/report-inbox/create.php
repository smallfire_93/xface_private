<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportInbox */

$this->title = 'Create Report Inbox';
$this->params['breadcrumbs'][] = ['label' => 'Report Inboxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-inbox-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
