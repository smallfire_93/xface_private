<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportInboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Inboxes';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$hostname    =   [];
$total  =   [];

foreach($getInboxDayByDAy as $item){
    array_push($hostname    ,   $item->inbox_date);
    array_push($total    ,   $item->total_inbox);
}

?>

<div class="report-inbox-index">
    <?php
    $gridColumns    =   [
        ['class' => 'yii\grid\SerialColumn'],
//        [
//            'header' => 'Ngày',
//            'value' => 'post_id.created_date',
//            'attribute' => 'post_id',
//        ],

        [
            'header' => 'Tổng số inbox',
            'value' => 'total_inbox',
            'attribute' => 'total_inbox',
        ],
    ]
    ?>
</div>
<div class="page-content">
    <div class="page-header">
        <h1>
            Báo cáo Inbox
            <small>
                <i class="icon-double-angle-right"></i>
            </small>
        </h1>
    </div><!-- /.page-header -->
    <div class="row" style="margin-top: 20px">
        <div class="">

        </div>
        <div class="col-xs-12">
            <div class="well">
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>
    </div>
    <?php
    echo Highcharts::widget([
        'options' => [
            'title'  => ['text' => 'Báo cáo inbox'],
            'xAxis'  => [
                'categories' => $hostname,
            ],
            'yAxis'  => [
                'title' => ['text' => 'Inbox'],
            ],
            'series' => [
                [
                    'name' => 'Inbox theo ngày',
                    'data' => $total,
                ],
            ],
        ],
    ]);
    ?>
    <div class="panel panel-info">

        <div class="panel panel-body">
            <div class="col-sm-12">
                <div class="col-sm-6 alert-info">
                    <div class="col-sm-6">Ngày inbox nhiều nhất:</div>
                    <div class="col-sm-6">30-02-2017 170 inbox</div>
                </div>
                <div class="col-sm-12"></div>
                <div class="col-sm-6 alert-success">
                    <div class="col-sm-6">Thời gian inbox nhiều nhất:</div>
                    <div class="col-sm-6">3h-4h 10 inbox</div>
                </div>
                <div class="col-sm-12"></div>
            </div>
        </div>
    </div>
    <?php echo ExportMenu::widget([
        'dataProvider'    => $dataProvider,
        'columns'         => $gridColumns,
        'fontAwesome'     => true,
        'dropdownOptions' => [
            'label' => 'Export',
            'class' => 'btn btn-warning',
        ],
        'exportConfig'    => [
            ExportMenu::FORMAT_TEXT  => false,
            ExportMenu::FORMAT_PDF   => false,
            ExportMenu::FORMAT_HTML  => false,
            ExportMenu::FORMAT_EXCEL => false,
        ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => $gridColumns,
    ]); ?>

    <div class="row">
        <div class="col-sm-3">
            <h3><b>Tổng</b></h3>
        </div>
    </div>
</div>
