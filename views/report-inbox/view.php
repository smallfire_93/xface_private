<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReportInbox */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Report Inboxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-inbox-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fanpage_id',
            'company_id',
            'total_comment',
            'hour_1',
            'hour_2',
            'hour_3',
            'hour_4',
            'hour_5',
            'hour_6',
            'hour_7',
            'hour_8',
            'hour_9',
            'hour_10',
            'hour_11',
            'hour_12',
        ],
    ]) ?>

</div>
