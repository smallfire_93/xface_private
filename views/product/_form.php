<?php
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form border-top">

	<?php $form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>
	<div class="padding-top">
		<div class="col-sm-6">
			<div class="panel panel-info">
				<div class="panel panel-heading">
					<b>Thông tin cơ bản</b>
				</div>
				<div class="panel panel-body">
					<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'category_id')->widget(Select2::className(), [
						'data'    => ArrayHelper::map($model->getCompanyCategory(), 'id', 'name'),
						'options' => ['prompt' => 'Chọn danh mục'],
					]) ?>

					<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'base_price')->textInput() ?>

					<?= $form->field($model, 'retail_price')->textInput() ?>

					<?= $form->field($model, 'whole_price')->textInput() ?>

					<?= $form->field($model, 'quantity')->textInput() ?>
					<?= $form->field($model, 'weight')->textInput() ?>

					<?= $form->field($model, 'size')->textInput() ?>

					<?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'post')->widget(Select2::className(), [
						'data'    => $model->getAvailablePost(),
						'options' => [
							'prompt'   => 'Chọn bài viết',
							'multiple' => true,
						],
					]) ?>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-success">
				<div class="panel panel-heading">
					<b>Thông tin bổ sung</b>
				</div>
				<div class="panel panel-body">
					<?= $form->field($model, 'product_image')->widget(FileInput::className(), [
						'options'       => ['accept' => 'image/*'],
						'pluginOptions' => [
							'showUpload'     => false,
							'showRemove'     => false,
							'initialPreview' => $model->getIsNewRecord() ? [
								Html::img(Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif', ['class' => 'file-preview-image']),
							] : [
								Html::img($model->getPictureUrl('image'), ['class' => 'file-preview-image']),
							],
							//							'initialPreviewAsData' => true,
							'browseClass'    => 'btn btn-md btn-info',
							'browseLabel'    => '',
						],
					]) ?>
					<?= $form->field($model, 'provider_id')->widget(Select2::className(), [
						'data'    => ArrayHelper::map($model->getCompanyProvider(), 'id', 'name'),
						'options' => ['prompt' => 'Chọn nhà cung cấp'],
					]) ?>
					<?= $form->field($model, 'content')->textarea([
						'maxlength' => true,
						'rows'      => 5,
					]) ?>

					<?= $form->field($model, 'status')->dropDownList($model::STATUS, ['prompt' => 'Status']) ?>
				</div>
			</div>
		</div>
		<div class="form-group col-sm-12">
			<div class="col-sm-6">
				<div class="col-sm-offset-3">
					<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
			</div>
		</div>
	</div>
	<?php ActiveForm::end(); ?>

</div>
