<?php
use app\models\Product;
use kartik\export\ExportMenu;
use kartik\file\FileInput;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var \app\models\UploadExcel $model */
$this->title                   = Yii::t('app', 'Danh sách sản phẩm');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$gridColumns = [
	['class' => 'kartik\grid\SerialColumn'],
	'name',
	[
		'attribute' => 'category_id',
		'value'     => function (Product $data) {
			return $data->category->name;
		},
	],
	'code',
	[
		'attribute' => 'image',
		'value'     => function (Product $data) {
			return Html::img($data->getPictureUrl('image'), ['class' => 'product-image']);
		},
		'format'    => 'raw',
	],
	[
		'attribute' => 'base_price',
		'value'     => function (Product $data) {
			return number_format($data->base_price);
		},
	],
	[
		'attribute' => 'whole_price',
		'value'     => function (Product $data) {
			return number_format($data->whole_price);
		},
	],
	[
		'attribute' => 'retail_price',
		'value'     => function (Product $data) {
			return number_format($data->retail_price);
		},
	],
	'quantity',
	'quantity_sale',
	//	[
	//		'attribute' => 'status',
	//		'value'     => function (Product $data) {
	//			return $data::STATUS[$data->status];
	//		},
	//	],
	// 'weight',
	// 'size',
	// 'color',
	// 'content',
	// 'provider_id',
	['class' => 'kartik\grid\ActionColumn'],
] ?>
<div class="product-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<div class="panel panel-info">
		<div class="panel panel-heading">
			<b>Tìm kiếm</b>
		</div>
		<div class="panel panel-body">
			<?php echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	</div>
	<div class="panel panel-danger">
		<div class="panel panel-heading">
			<b>Báo cáo</b>
		</div>
		<div class="panel panel-body">
			<div class="col-sm-7">
				<div class="row top-buffer">
					<div class="col-sm-6">Tổng số sản phẩm: <?= $total_product ?></div>
					<div class="col-sm-6">Tổng số tiền bán: <?= $total_money ?></div>
				</div>
				<div class="row top-buffer">
					<div class="col-sm-6">Tổng số đã bán: <?= $total_sale ?></div>
					<div class="col-sm-6">Tổng số bài viết: <?= $total_post ?></div>
				</div>
			</div>
			<div class="col-sm-5">
			</div>
		</div>
	</div>
	<div class="panel panel-success">
		<div class="panel panel-heading">
			<b>Import sản phẩm</b>
		</div>
		<div class="panel panel-body">
			<?php $form = ActiveForm::begin([
				'layout'  => 'horizontal',
				'options' => [
					'enctype' => 'multipart/form-data',
				],
			]); ?>
			<div class="col-sm-5">
				<?= $form->field($model, 'excel', [])->widget(FileInput::className(), [
					'pluginOptions' => [
						'allowedFileExtensions' => [
							'xlsx',
						],
						'showCaption'           => true,
						'showRemove'            => false,
						'showUpload'            => false,
						'showPreview'           => false,
						'browseClass'           => 'btn btn-primary',
						'browseIcon'            => '',
						'browseLabel'           => 'Chọn file',
					],
				])->label(false) ?>
				<!--	<button style="float: right;clear: right">--><?php //echo  ?><!--</button>-->
				<div class="col-sm-offset-3">
					<?= Html::submitButton('Import excel', ['class' => 'btn btn-warning']) ?>
				</div>
			</div>
			<?php ActiveForm::end(); ?>

		</div>
	</div>

	<?= Html::a(Yii::t('app', 'Thêm mới sản phẩm'), ['create'], ['class' => 'btn btn-success']) ?>

	<?php echo ExportMenu::widget([
		'dataProvider'    => $dataProvider,
		'columns'         => $gridColumns,
		'fontAwesome'     => true,
		'dropdownOptions' => [
			'label' => 'Export',
			'class' => 'btn btn-warning',
		],
		'exportConfig'    => [
			\kartik\export\ExportMenu::FORMAT_TEXT  => false,
			\kartik\export\ExportMenu::FORMAT_PDF   => false,
			\kartik\export\ExportMenu::FORMAT_HTML  => false,
			\kartik\export\ExportMenu::FORMAT_EXCEL => false,
		],
	]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => $gridColumns,
	]); ?>
</div>
