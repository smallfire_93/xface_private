<?php
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\search\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
	]); ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<?= $form->field($model, 'name')->textInput(['placeholder'=>'Tên sản phẩm']) ?>
			</div>
			<div class="col-sm-2">
				<?= $form->field($model, 'code')->textInput(['placeholder'=>'Mã sản phẩm']) ?>
			</div>
			<div class="col-sm-2">
				<?= $form->field($model, 'status')->dropDownList($model::STATUS, [
					'prompt' => 'Trạng thái',
				]); ?>
			</div>
		</div>
	</div>
	<?php // echo $form->field($model, 'base_price') ?>

	<?php // echo $form->field($model, 'retail_price') ?>

	<?php // echo $form->field($model, 'whole_price') ?>

	<?php // echo $form->field($model, 'quantity') ?>

	<?php // echo $form->field($model, 'quantity_sale') ?>

	<?php // echo $form->field($model, 'status') ?>

	<?php // echo $form->field($model, 'weight') ?>

	<?php // echo $form->field($model, 'size') ?>

	<?php // echo $form->field($model, 'color') ?>

	<?php // echo $form->field($model, 'content') ?>

	<?php // echo $form->field($model, 'provider_id') ?>

	<div class="form-group top-buffer col-sm-3">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
