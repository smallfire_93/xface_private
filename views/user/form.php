<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 *
 */
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Thêm mới/cập nhật";
$this->params['breadcrumbs'][] = "Nhân viên";
?>

<div class="page-content">
    <div class="page-header">
        <h1>
            Thêm mới/ cập nhật Nhân viên
            <small>
                <i class="icon-double-angle-right"></i>

            </small>
        </h1>
    </div><!-- /.page-header -->
    <div class="row">
    <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->

    <form class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Họ Tên </label>

        <div class="col-sm-9">
            <input type="text" id="form-field-1" placeholder="Họ tên" class="col-xs-10 col-sm-5" />
        </div>
    </div>

    <div class="space-4"></div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Số điện thoại </label>

        <div class="col-sm-9">
            <input type="password" id="form-field-2" placeholder="1214135235" class="col-xs-10 col-sm-5" />

        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Email </label>

        <div class="col-sm-9">
            <input type="password" id="form-field-2" placeholder="email" class="col-xs-10 col-sm-5" />

        </div>
    </div>

    <div class="space-4"></div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Mật khẩu </label>

        <div class="col-sm-9">
            <input type="password" id="form-field-2" placeholder="**********" class="col-xs-10 col-sm-5" />

        </div>
    </div>

    <div class="space-4"></div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Phân ca</label>

        <div class="col-sm-3">
            <div class="input-group input-group-sm">
                <input type="text" id="datepicker" class="form-control hasDatepicker">
                                                <span class="input-group-addon">
                                                    <i class="icon-calendar"></i>
                                                </span>
            </div>
            <div class="input-group input-group-sm">
                <input type="text" id="datepicker" class="form-control hasDatepicker">
                                                <span class="input-group-addon">
                                                    <i class="icon-calendar"></i>
                                                </span>
            </div>
        </div>
    </div>

    <div class="space-4"></div>
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Trạng thái</label>
        <div class="col-sm-3">
            <select id="user-status" class="form-control" name="User[status]" aria-invalid="false">
                <option value="">---Trạng thái---</option>
                <option value="1" selected="">Active</option>
                <option value="0">Block</option>
            </select>
            <div class="help-block help-block-error "></div>
        </div>
    </div>


    <div class="clearfix">
        <div class="col-md-offset-3 col-md-9">
            <button class="btn btn-info" type="button">
                <i class="icon-ok bigger-110"></i>
                Thêm mới
            </button>
        </div>
    </div>

    <div class="hr hr-24"></div>
<h2>Quyền quản trị</h2>
    <div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Nhóm nhân viên</h4>

<!--                <div class="widget-toolbar">-->
<!--                    <a href="#" data-action="collapse">-->
<!--                        <i class="icon-chevron-up"></i>-->
<!--                    </a>-->
<!---->
<!--                    <a href="#" data-action="close">-->
<!--                        <i class="icon-remove"></i>-->
<!--                    </a>-->
<!--                </div>-->
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Nhân viên tương tác</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Nhân viên chốt sale</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Nhân viên kho</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Nhân viên sử lí sau bán hàng</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Nhân viên CSKH</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /span -->

    <div class="col-xs-12 col-sm-6">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Quyền quản trị (Chọn các tính năng tài khoản được truy cập)</h4>

<!--													<span class="widget-toolbar">-->
<!--														<a href="#" data-action="settings">-->
<!--                                                            <i class="icon-cog"></i>-->
<!--                                                        </a>-->
<!---->
<!--														<a href="#" data-action="reload">-->
<!--                                                            <i class="icon-refresh"></i>-->
<!--                                                        </a>-->
<!---->
<!--														<a href="#" data-action="collapse">-->
<!--                                                            <i class="icon-chevron-up"></i>-->
<!--                                                        </a>-->
<!---->
<!--														<a href="#" data-action="close">-->
<!--                                                            <i class="icon-remove"></i>-->
<!--                                                        </a>-->
<!--													</span>-->
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Chát & comment</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Tạo đơn hàng</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Khách hàng</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Xử lí đơn hàng</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Khuyến mãi</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Vận chuyển</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Xử lí đơn hàng vận chuyển</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Xử lí khiếu nại</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Danh sách KH</span>
                        </label>
                    </div>
                    </hr>
                    <div class="checkbox">
                        <label>
                            <input name="form-field-checkbox" type="checkbox" class="ace">
                            <span class="lbl"> Cập nhật thông tin</span>
                        </label>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /span -->

    <div class="col-xs-12 col-sm-4">


    </div>
    </div><!-- /span -->
    </div><!-- /row -->

    <div class="space-24"></div>


    </div>
    </form>

    </div><!-- /.col -->
    </div><!-- /.row -->
</div>