<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content">

    <div class="page-header">
        <h1>
            Nhân Viên
            <small>
                <i class="icon-double-angle-right"></i>
                Danh sách nhân viên
            </small>
        </h1>
    </div><!-- /.page-header -->
    <p style="margin-top: 20px">
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <div id="sample-table-2_wrapper" class="dataTables_wrapper" role="grid">
                    <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'header' => 'Mã nhân viên',
                                    'value' => 'code',
                                    'attribute' => 'code',
                                ],
                                [
                                    'header' => 'Tên nhân viên',
                                    'value' => 'full_name',
                                    'attribute' => 'full_name',
                                ],
                                [
                                    'header' => 'Email',
                                    'value' => 'email',
                                    'attribute' => 'email',
                                ],
                                [
                                    'header' => 'Lần đăng nhập gần nhất',
                                    'value' => 'last_login',
                                    'attribute' => 'last_login',
                                ],
                                [
                                    'header' => 'Địa chỉ',
                                    'value' => 'address',
                                    'attribute' => 'address',
                                ],
                                [
                                    'header' => 'Số điện thoại',
                                    'value' => 'phone',
                                    'attribute' => 'phone',
                                ],
                                [
                                    'header' => 'Nhóm nhân viên',
//                                    'value' => 'userGroup.group.name',
                                    'attribute' => 'parent_id',
                                ],
                                [
                                    'header' => 'Trạng thái',
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        if ($model->status == 1) {
                                            return '<span class="label label-sm label-success">Active</span>';
                                        } elseif ($model->status == 0) {
                                            return '<span class="label label-sm label-inverse arrowed-in">Block</span>';
                                        }
                                    },
                                ],
                                ['class' => 'yii\grid\ActionColumn','template'=>'{update},{delete}'],
                            ],
                        ]); ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
