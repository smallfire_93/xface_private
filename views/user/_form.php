<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use phamxuanloc\jui\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-content">

    <div class="page-header">
        <h1>
            <h1><?= Html::encode($this->title) ?></h1>
        </h1>
    </div><!-- /.page-header -->
<div class="row" style="margin-top: 20px">
    <div class="col-xs-12">
        <?php $form = ActiveForm::begin(
            ['layout' => 'horizontal']
        ); ?>
        <div class="row">
            <?= $form->field($model, 'code')->textInput(['placeholder'=>"code"])->label('Mã nhân viên') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'username')->textInput(['placeholder'=>"Username"])->label('Tài khoản đăng nhập') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>"Mật khẩu"])->label('Mật khẩu') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'full_name')->textInput(['placeholder'=>"Họ Tên"])->label('Tên nhân viên') ?>
        </div>

        <div class="row">
            <?= $form->field($model, 'phone')->textInput(['placeholder'=>"Số điện thoại"])->label('Số điện thoại') ?>
        </div>

        <div class="row">
            <?= $form->field($model, 'email')->textInput(['placeholder'=>"email"])->label('Email') ?>
        </div>


        <div class="row">
                <?= $form->field($model, 'start_time')->widget(DateTimePicker::className(), [
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                    ],
                    'options' => ['style' => 'width:200px',],
                ]) ?>

                <?= $form->field($model, 'end_time')->widget(DateTimePicker::className(), [
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                    ],
                    'options' => ['style' => 'width:200px',],
                ]) ?>

        </div>


        <div class="row">
            <?php $arrStatus = ['' => '---Trạng thái---', '1' => 'Active', '0' => 'Block']; ?>
            <?= $form->field($model, 'status')->dropDownList($arrStatus)->label('Trạng thái') ?>
        </div>

        <div class="clearfix">
            <div class="col-md-offset-4 col-md-3">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            </div>
        </div>

        <?php ActiveForm::end(); ?>


        <div class="hr hr-24"></div>



        <h2>Quyền quản trị</h2>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4>Nhóm nhân viên</h4>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4>Quyền quản trị (Chọn các tính năng tài khoản được truy cập)</h4>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
