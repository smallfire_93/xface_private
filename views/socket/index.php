<html>
<head>
    <title>WebSocket Client</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://localhost:5555/socket.io/socket.io.js"></script>
    <script>
        $(document).ready(function () {
            // Connect to our node/websockets server
            var socket = io.connect('http://localhost:5555');

            // Initial set of notes, loop through and add to list
            socket.on('initial notes', function (data) {
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    // We store html as a var then add to DOM after for efficiency
                    html += '<li class="li">' + data[i].username + '</li>'
                }
                $('#notes').empty();
                $('#notes').append(html);
            });

            // New socket connected, display new count on page
            socket.on('users connected', function (data) {
                $('#usersConnected').html('Users connected: ' + data)
            });

            setInterval(function(){
                socket.emit('news', 'I want news :D ');
            }, 5000);
        });
    </script>
</head>
<body>
<ul id="notes"></ul>
<div id="usersConnected"></div>
</body>
</html>