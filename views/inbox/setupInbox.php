<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Cài đặt chung: cấu hình inbox';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row page-header">
    <h1>
        Cài đặt chung:
        <small>
            <i class="icon-double-angle-right"></i>
            Cấu hình inbox
        </small>
    </h1>
</div>

<h3 class="header smaller green">Tự động trả lời inbox</h3>
<div class="row">
    <label class="pull-left inline" style="margin-top: -20px">
        <input id="id-button-borders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-5">
        <span class="lbl"></span>
    </label>
</div>

<div class="row container" style="margin-top: 20px">
    <form>
        <div class="form-group">
            <label for="exampleInputEmail1">Nội dung trả lời tự động</label>
            <textarea type="email" class="form-control" id="exampleInputEmail1"></textarea>
        </div>
    </form>
</div>

<h3 class="header smaller green">Kịch bản trả lời tự động</h3>
<form class="form-horizontal">
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung inbox</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung trả lời</label>
        <div class="col-sm-10">
            <textarea type="email" class="form-control" id="inputEmail3"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung inbox</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung trả lời</label>
        <div class="col-sm-10">
            <textarea type="email" class="form-control" id="inputEmail3"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung inbox</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung trả lời</label>
        <div class="col-sm-10">
            <textarea type="email" class="form-control" id="inputEmail3"></textarea>
        </div>
    </div>
    <div class="row">
        <button class="col-md-offset-11 col-md-1">Lưu</button>
    </div>
</form>