<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FanpageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fanpages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fanpage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fanpage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company_id',
            'page_id',
            'access_token',
            'expire_date',
            // 'is_join',
            // 'create',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
