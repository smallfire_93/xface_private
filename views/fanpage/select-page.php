<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Breadcrumbs;

$this->title                   = "Chọn fanpage";
$this->params['breadcrumbs'][] = "Fanpage";
?>

<div class="page-content">
	<div class="page-header">
		<h1>
			Chọn fanpage
			<small>
				<i class="icon-double-angle-right"></i>
				Danh sách fanpage
			</small>
		</h1>
	</div>
	<?php $form = ActiveForm::begin() ?>

	<div class="row">
		<div class="col-xs-12">

			<?php
			$i = 1;
			foreach($fbPages['data'] as $fb) { ?>
				<div class="col-md-4">
					<div class="checkbox">
						<?= Html::checkbox('Fanpage[' . $i . '][join]', false, [
							'class' => 'page-checkbox',
							'value' => $fb['id'],
						]) ?>
						<span class="lbl">
							<img src="//graph.facebook.com/<?= $fb['id'] ?>/picture"> <?= $fb['name'] ?></span>
					</div>
				</div>
				<?php $i ++;
			} ?>

		</div>
	</div>
	<div class="clearfix" style="margin-top: 30px">
		<div class="col-md-offset-5 col-md-3">
			<?= Html::submitButton('<i class="icon-ok bigger-110"></i> Chọn', ['class' => 'btn btn-info']) ?>
		</div>
	</div>
	<?php ActiveForm::end() ?>

</div>