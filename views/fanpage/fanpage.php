<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title                   = "Fanpage đã chọn";
$this->params['breadcrumbs'][] = "Fanpage";
?>

<div class="page-content">
	<div class="page-header">
		<h1>
			Fanpage đã chọn
			<small>
				<i class="icon-double-angle-right"></i>
				Danh sách fanpage
			</small>
		</h1>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php foreach($fb_pages as $fb) { ?>
				<div class="col-md-3">
					<div class="checkbox">
						<label>
							<span class="lbl"><i class="icon-facebook-sign"></i> <?= $fb->name ?></span>
						</label>
						<a href="#" data-href="<?= Url::to(['/fanpage/delete-join']) ?>" data-id=<?= $fb->id ?> class="tooltip-error delete-page" data-rel="tooltip" title=""
						data-original-title="Delete">
						<span class="red">
          <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
						</span>
						</a>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="clearfix" style="margin-top: 30px">
		<div class="col-md-offset-5 col-md-3">
			<a class="btn btn-info" href="<?php echo yii::$app->urlManager->createUrl(['fanpage/select-page']) ?>" style="color: #000000">
				<i class="icon-ok bigger-110"></i> Thêm fanpage mới
			</a>
		</div>
	</div>
</div>
<script>

</script>