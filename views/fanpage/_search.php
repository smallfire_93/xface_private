<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FanpageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fanpage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'company_id') ?>

    <?= $form->field($model, 'page_id') ?>

    <?= $form->field($model, 'access_token') ?>

    <?= $form->field($model, 'expire_date') ?>

    <?php // echo $form->field($model, 'is_join') ?>

    <?php // echo $form->field($model, 'create') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
