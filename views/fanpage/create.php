<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Fanpage */

$this->title = 'Create Fanpage';
$this->params['breadcrumbs'][] = ['label' => 'Fanpages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fanpage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
