<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\components\widgets\SidebarWidget;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="navbar-fixed">
<?php $this->beginBody() ?>

<div class="navbar navbar-default navbar-fixed-top" id="navbar">
	<script type="text/javascript">
		try {
			ace.settings.check('navbar', 'fixed')
		} catch(e) {
		}
	</script>

	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="#" class="navbar-brand">
				<small>
					<i class="icon-leaf"></i>
					X-FACE
				</small>
			</a><!-- /.brand -->
		</div><!-- /.navbar-header -->

	</div><!-- /.container -->
</div>

<div class="main-container" id="main-container">

	<div class="main-container-inner">
		<div class="main-content">
			<div class="breadcrumbs" id="breadcrumbs">
				<?= Breadcrumbs::widget([
					'homeLink'     => [
						'label' => '<i class="icon-home home-icon"></i>',
						'url'   => Yii::$app->homeUrl,
					],
					'encodeLabels' => false,
					'links'        => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]) ?>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-sm-12">
						<?= $content ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
