<?php
use app\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Danh mục sản phẩm';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<div class="panel panel-info">
		<div class="panel panel-heading">
			<b>Tìm kiếm</b>
		</div>
		<div class="panel panel-body">
			<?php echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	</div>
	<div class="panel panel-danger">
		<div class="panel panel-heading">
			<b>Báo cáo</b>
		</div>
		<div class="panel panel-body">
			<div class="col-sm-7">
				<div class="row top-buffer">
					<div class="col-sm-6">Tổng số sản phẩm: <?= $product ?></div>
					<div class="col-sm-6">Hàng trong kho: <?= $quantity ?></div>
				</div>
				<div class="row top-buffer">
					<div class="col-sm-6">Tổng số tiền đã bán: <?= $money_sale ?> VNĐ</div>
					<div class="col-sm-6">Tổng số sản phẩm đã bán: <?= $quantity_sale ?></div>
				</div>
			</div>
			<div class="col-sm-5">
			</div>
		</div>
	</div>
	<p>
		<?= Html::a('Tạo danh mục', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		//		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'name',
			[
				'attribute' => 'parent_id',
				'value'     => function (Category $data) {
					return $data->parent_id != 0 && $data->parent_id != null ? $data->parent->name : 'Không có';
				},
			],
			//					'description',
			[
				'attribute' => 'status',
				'value'     => function (Category $data) {
					return $data::STATUS[$data->status];
				},
			],
			'quantity',
			'total_quantity_sale',
			//					'create_date',
			//					 'fanpage_id',
			//					 'company_id',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
