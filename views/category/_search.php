<?php
use app\models\Category;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\search\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
	]); ?>
	<div class="col-sm-12">
		<?= $form->field($model, 'id')->widget(Select2::className(), [
			'data'    => ArrayHelper::map(Category::find()->where(['company_id' => $model->user->company_id])->all(), 'id', 'name'),
			'options' => ['prompt' => 'Chọn danh mục',],
		]) ?>
	</div>
	<?php // echo $form->field($model, 'quantity') ?>

	<?php // echo $form->field($model, 'total_quantity_sale') ?>

	<?php // echo $form->field($model, 'create_date') ?>

	<?php // echo $form->field($model, 'fanpage_id') ?>

	<?php // echo $form->field($model, 'company_id') ?>

	<div class="form-group top-buffer col-sm-3">
		<?= Html::submitButton('Tìm kiếm', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
