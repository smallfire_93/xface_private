<?php
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="border-top">
	<div class="category-form padding-top">
		<?php $form = ActiveForm::begin([
			'layout' => 'horizontal',
		]); ?>

		<?= $form->field($model, 'parent_id')->widget(Select2::className(), ['data' => ArrayHelper::map($model->getParentCategory(), 'id', 'name')]) ?>
		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'status')->dropDownList($model::STATUS, ['class' => 'col-sm-3']) ?>

		<div class="form-group col-sm-4">
			<?= Html::submitButton($model->isNewRecord ? '<i class=\'icon-ok bigger-110\'></i>  Thêm mới' : '<i class=\'icon-ok bigger-110\'></i>  Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-info pull-right' : 'btn btn-primary pull-right']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
