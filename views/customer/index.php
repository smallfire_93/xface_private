<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <div id="sample-table-2_wrapper" class="dataTables_wrapper" role="grid">
                <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                    <?php
                    $gridColumns = [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'header' => 'Tên khách hàng',
                            'value' => 'full_name',
                            'attribute' => 'full_name',
                        ],

                        [
                            'header' => 'Số điện thoại',
                            'value' => 'phone',
                            'attribute' => 'phone',
                        ],

                        [
                            'header' => 'Địa chỉ',
                            'value' => 'address',
                            'attribute' => 'address',
                        ],

                        [
                            'header' => 'Email',
                            'value' => 'email',
                            'attribute' => 'email',
                        ],

                        [
                            'header' => 'Facebook',
                            'value' => 'email',
                            'attribute' => 'fb_user_id',
                        ],

                        [
                            'header' => 'Số lượng đã mua',
                            'value' => 'total_order',
                            'attribute' => 'total_order',
                        ],

//                        [
//                            'header'  => 'xem đơn hàng',
//                            'value' =>  ''
//                        ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ]?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="page-header">
        <h1>
            Khách hàng
            <small>
                <i class="icon-double-angle-right"></i>
                Danh sách khách hàng
            </small>
        </h1>
    </div><!-- /.page-header -->
    <div class="row" style="margin-top: 20px">
        <div class="col-xs-12">
            <div class="well">
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>
    </div>
    <?php echo ExportMenu::widget([
        'dataProvider'    => $dataProvider,
        'columns'         => $gridColumns,
        'fontAwesome'     => true,
        'dropdownOptions' => [
            'label' => 'Export',
            'class' => 'btn btn-warning',
        ],
        'exportConfig'    => [
            ExportMenu::FORMAT_TEXT  => false,
            ExportMenu::FORMAT_PDF   => false,
            ExportMenu::FORMAT_HTML  => false,
            ExportMenu::FORMAT_EXCEL => false,
        ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => $gridColumns,
    ]); ?>

</div>
