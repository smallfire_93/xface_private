<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


        <div class="form">
            <div class="col-sm-3">
                <?= $form->field($model, 'full_name')->textInput(['placeholder'=>"Họ tên khách hàng"])->label('') ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'phone')->textInput(['placeholder'=>"Số điện thoại"])->label('') ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'address')->textInput(['placeholder'=>"Địa chỉ"])->label('') ?>
            </div>
            <div class="col-sm-3">
                <?php  echo $form->field($model, 'fb_user_id')->textInput(['placeholder'=>"Facebook"])->label('') ?>
            </div>
        </div>


    <div class="form-group" style="padding-left: 12px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
