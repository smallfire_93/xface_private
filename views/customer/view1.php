<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-content">

    <div class="page-header">
        <h1>
            Khách hàng
            <small>
                <i class="icon-double-angle-right"></i>
                Danh sách khách hàng
            </small>
        </h1>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <div class="well">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <input type="text" id="form-field-1" placeholder="Họ tên khách hàng"/>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="form-field-1" placeholder="Số điện thoại"/>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="form-field-1" placeholder="Địa chỉ"/>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" id="form-field-1" placeholder="Facebook"/>
                        </div>
                    </div>
                </form>
                <div class="clearfix ">
                    <button class="btn btn-info" type="button">
                        <i class="icon-search bigger-110"></i>
                        <a href="" style="color: #000000">Tìm kiếm</a>
                    </button>
                    <button class="btn btn-warning">Export</button>
                </div>
            </div>
        </div>
    </div>

    <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
        <thead>

        <tr role="row">
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2"
                rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending"
                style="width: 170px;">ID
            </th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2"
                rowspan="1" colspan="1" aria-label="Price: activate to sort column ascending"
                style="width: 114px;">Tên khách hàng
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Clicks: activate to sort column ascending" style="width: 123px;">số điện thoại
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Địa chỉ
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Email
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Facebook
            </th>

            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Số lượng hàng đã mua
            </th>

            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Xem đơn hàng
            </th>

            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">
            </th>

        </tr>
        </thead>


        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="odd">


            <td class=" ">
                <a href="#">1</a>
            </td>
            <td class=" ">cuongluong</td>
            <td class="hidden-480 ">321312312</td>
            <td class=" ">Hà Nội</td>
            <td class=" ">ada@gmail.com</td>
            <td class=" ">facebook</td>
            <td class=" ">34</td>
            <td class=" "><a href="">đơn hàng a</a></td>

            <td class="">
                <button class="btn btn-sm btn btn-danger">Report</button>
            </td>
        </tr>
        <tr class="even">

            <td class=" ">
                <a href="#">1</a>
            </td>
            <td class=" ">cuong</td>
            <td class="hidden-480 ">43545345</td>
            <td class=" ">Hà Nội</td>
            <td class=" ">Hà nội@gmail.com</td>
            <td class=" ">Facebook\name</td>
            <td class=" ">56</td>
            <td class=" "><a href="">đơn hàng b</a></td>

            <td class="">
                <button class="btn btn-sm btn btn-danger">Report</button>
            </td>

        </tr>
        <!---->
        </tbody>
    </table>

</div>
<div class="row">
    <div class="col-sm-6">
        <div class="dataTables_info" id="sample-table-2_info">Showing 1 to 10 of 23 entries</div>
    </div>
    <div class="col-sm-6">
        <div class="dataTables_paginate paging_bootstrap">
            <ul class="pagination">
                <li class="prev disabled"><a href="#"><i class="icon-double-angle-left"></i></a>
                </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li class="next"><a href="#"><i class="icon-double-angle-right"></i></a></li>
            </ul>
        </div>
    </div>
</div>