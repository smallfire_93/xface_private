<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use miloschuman\highcharts\Highcharts;



/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Products';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$hostname    =   [];
$total  =   [];

foreach($getProductDayByDAy as $item){
    array_push($hostname    ,   $item->created_date);
    array_push($total    ,   $item->total_order);
}

?>

<div class="report-product-index">
    <?php
    $gridColumns    =   [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'header' => 'Sản phẩm',
            'value' => 'product_id.name',
            'attribute' => 'product_id',
        ],
        [
            'header' => 'Tồn kho',
            'value' => 'in_stock',
            'attribute' => 'in_stock',
        ],

        [
            'header' => 'Tổng đơn hàng',
            'value' => 'total_order',
            'attribute' => 'total_order',
        ],
        [
            'header' => 'Tổng đơn hoàn thành',
            'value' => 'total_order_finish',
            'attribute' => 'total_order_finish',
        ],
        [
            'header' => 'Doanh thu',
            'value' => 'total_money',
            'attribute' => 'total_money',
        ],

        // 'company_id',
        // 'fanpage_id',
    ]
    ?>
    <div class="page-content">
        <div class="page-header">
            <h1>
                Báo cáo Sản Phẩm
                <small>
                    <i class="icon-double-angle-right"></i>
                </small>
            </h1>
        </div><!-- /.page-header -->
        <div class="row" style="margin-top: 20px">
            <div class="">

            </div>
            <div class="col-xs-12">
                <div class="well">
                    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <?php
        echo Highcharts::widget([
            'options' => [
                'title'  => ['text' => 'Báo cáo sản phẩm'],
                'xAxis'  => [
                    'categories' => $hostname,
                ],
                'yAxis'  => [
                    'title' => ['text' => 'Đơn hàng'],
                ],
                'series' => [
                    [
                        'name' => 'Doanh số sản phẩm theo ngày',
                        'data' => $total,
                    ],
                ],
            ],
        ]);
        ?>
        <div class="row border-top">
            <div class="col-sm-12">
                <div class="row">
                    <div class="space-6"></div>

                    <div class="col-sm-12 infobox-container">
                        <div class="col-sm-3">

                            <div class="infobox infobox-green ">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?php echo '<b>'. $TotalProduct .'</b>' ?></span>
                                    <div class="infobox-content">Tổng số sản phẩm</div>
                                </div>
                                <!--					<div class="stat stat-success">8%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-blue">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">11</span>
                                    <div class="infobox-content">Sàn phẩm hết hàng</div>
                                </div>

                                <!--					<div class="badge badge-success">-->
                                <!--						+32%-->
                                <!--						<i class="icon-arrow-up"></i>-->
                                <!--					</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">10.000.000</span>
                                    <div class="infobox-content">Sản phẩm đang bán</div>
                                </div>
                                <!--					<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">1.000.000</span>
                                    <div class="infobox-content">Sản phẩm đã bán</div>
                                </div>
                                <!--						<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <?php echo ExportMenu::widget([
            'dataProvider'    => $dataProvider,
            'columns'         => $gridColumns,
            'fontAwesome'     => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-warning',
            ],
            'exportConfig'    => [
                ExportMenu::FORMAT_TEXT  => false,
                ExportMenu::FORMAT_PDF   => false,
                ExportMenu::FORMAT_HTML  => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => $gridColumns,
        ]); ?>

    </div>
</div>
