<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportProduct */

$this->title = 'Create Report Product';
$this->params['breadcrumbs'][] = ['label' => 'Report Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
