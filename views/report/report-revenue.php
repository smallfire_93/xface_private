<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;

$this->title                   = Yii::t('app', 'Báo cáo doanh thu');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-info">
	<div class="panel panel-heading">
		<b>Tìm kiếm</b>
	</div>
	<div class="panel panel-body">
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Trạng thái đơn hàng']) ?>
		<?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'From date'],
		]) ?>        <?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'To date'],
		]) ?>
		<div class="row">
			<div class="col-sm-12 padding-top">
				<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
				<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
			</div>
		</div>
	</div>
</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo doanh thu'],
		'xAxis'  => [
			'categories' => [
				'01',
				'02',
				'03',
				'04',
				'05',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Doanh thu'],
		],
		'series' => [
			[
				'name' => 'Doanh thu',
				'data' => [
					1,
					0,
					4,
					10,
					5,
				],
			],
		],
	],
]);
?>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>
				<i class="icon-time bigger-110 hidden-480"></i>

				Ngày
			</th>
			<th>Tổng số khách hàng</th>

			<th>
				Đơn hàng hoàn thành
			</th>
			<th>
				Đơn hàng đang giao
			</th>
			<th>
				Đơn đang hồi hoàn
			</th>
			<th>
				Doanh thu
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Feb 12</td>
			<td>100</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>100</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>
		<tr style="border-top: 1px solid green">

			<td>Tổng doanh thu:</td>
			<td>10000</td>
			<td>3,33000</td>
			<td>3,33000</td>
			<td>3,33000</td>
			<td>10,000,000</td>

		</tr>

		</tbody>
	</table>
</div>

