<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;

$this->title                   = Yii::t('app', 'Báo cáo comment');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-info">
	<div class="panel panel-heading">
		<b>Tìm kiếm</b>
	</div>
	<div class="panel panel-body">
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
		<?= Html::dropDownList('init', [], [], ['prompt' => 'Đơn vị vận chuyển']) ?>
		<?= Html::dropDownList('status', [], [], ['prompt' => 'Trạng thái đơn hàng']) ?>
		<?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'From date'],
		]) ?>        <?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'To date'],
		]) ?>
		<div class="row">
			<div class="col-sm-12 padding-top">
				<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
				<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
			</div>
		</div>
	</div>

</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo vận chuyển'],
		'xAxis'  => [
			'categories' => [
				'01',
				'02',
				'03',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Vận chuyển theo ngày'],
		],
		'series' => [
			[
				'name' => 'Đơn hàng',
				'data' => [
					1,
					0,
					4,
				],
			],
		],
	],
]);
?>
<div class="row">
	<div class="col-sm-12 top-buffer">
		<h3>Vận chuyển</h3>
	</div>
</div>
<div class="row border-top">
	<div class="col-sm-12">
		<div class="row">
			<div class="space-6"></div>

			<div class="col-sm-12 infobox-container">
				<div class="col-sm-3">

					<div class="infobox infobox-green ">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">32</span>
							<div class="infobox-content">Đơn chưa vận chuyển</div>
						</div>
						<!--					<div class="stat stat-success">8%</div>-->
					</div>
				</div>
				<div class="col-sm-3">

					<div class="infobox infobox-blue">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">11</span>
							<div class="infobox-content">Giao không gặp khách</div>
						</div>

						<!--					<div class="badge badge-success">-->
						<!--						+32%-->
						<!--						<i class="icon-arrow-up"></i>-->
						<!--					</div>-->
					</div>
				</div>
				<div class="col-sm-3">

					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">8</span>
							<div class="infobox-content">Chờ hồi hoàn</div>
						</div>
						<!--					<div class="stat stat-important">4%</div>-->
					</div>
				</div>
				<div class="col-sm-3">
					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">8</span>
							<div class="infobox-content">Đã hoàn thành</div>
						</div>
						<!--						<div class="stat stat-important">4%</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>
				<i class="icon-time bigger-110 hidden-480"></i>

				Ngày
			</th>
			<th>Đơn vị vận chuyển</th>

			<th>
				Tổng số đơn hàng
			</th>
			<th>
				Đơn hàng hoàn thành
			</th>
			<th>
				Đơn đang hồi hoàn
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Feb 12</td>
			<td>ĐV 1</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>ĐV 1</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Tổng</td>
			<td></td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>

		</tr>

		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-sm-12 top-buffer">
		<h3>Thu hộ</h3>
	</div>
</div>
<div class="row border-top">
	<div class="col-sm-12">
		<div class="row">
			<div class="space-6"></div>

			<div class="col-sm-12 infobox-container">
				<div class="col-sm-3">

					<div class="infobox infobox-green ">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">32</span>
							<div class="infobox-content">Số đơn thu hộ</div>
						</div>
						<!--					<div class="stat stat-success">8%</div>-->
					</div>
				</div>
				<div class="col-sm-3">

					<div class="infobox infobox-blue">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">11</span>
							<div class="infobox-content">Số tiền thu hộ</div>
						</div>

						<!--					<div class="badge badge-success">-->
						<!--						+32%-->
						<!--						<i class="icon-arrow-up"></i>-->
						<!--					</div>-->
					</div>
				</div>
				<div class="col-sm-3">

					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">10.000.000</span>
							<div class="infobox-content">Số tiền đã trả</div>
						</div>
						<!--					<div class="stat stat-important">4%</div>-->
					</div>
				</div>
				<div class="col-sm-3">
					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">1.000.000</span>
							<div class="infobox-content">Số tiền còn lại</div>
						</div>
						<!--						<div class="stat stat-important">4%</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>
				<i class="icon-time bigger-110 hidden-480"></i>

				Ngày
			</th>
			<th>Đơn vị vận chuyển</th>

			<th>
				Số đơn thu hộ
			</th>
			<th>
				Số đơn đã thu
			</th>
			<th>
				Số đơn hồi hoàn
			</th>
			<th>
				Cần thanh toán
			</th>
			<th>
				Đã thanh toán
			</th>
			<th>
				Còn lại
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Feb 12</td>
			<td>Giao hàng nhanh</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>
			<td>10,000</td>
			<td>10,000</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>Giao hàng nhanh</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>
			<td>10,000</td>
			<td>10,000</td>

		</tr>
		<tr>

			<td>Tổng</td>
			<td></td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>
			<td>10,000</td>
			<td>10,000</td>

		</tr>

		</tbody>
	</table>
</div>