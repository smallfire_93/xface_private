<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 12:01 PM
 */
?>
<h3 style="text-align: center">Báo cáo tổng quan</h3>
<div class="row top-buffer">
	<div class="col-sm-12">
		<div class="row">
			<div class="space-6"></div>

			<div class="col-sm-12 infobox-container">
				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-comment']) ?>">
						<div class="infobox infobox-green ">
							<div class="infobox-icon">
								<i class="icon-comment-alt"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">32</span>
								<div class="infobox-content">Comment</div>
							</div>
							<!--					<div class="stat stat-success">8%</div>-->
						</div>
					</a>
				</div>

				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-product']) ?>">

						<div class="infobox infobox-blue">
							<div class="infobox-icon">
								<i class="icon-tags"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">11</span>
								<div class="infobox-content">Sản phẩm</div>
							</div>

							<!--					<div class="badge badge-success">-->
							<!--						+32%-->
							<!--						<i class="icon-arrow-up"></i>-->
							<!--					</div>-->
						</div>
					</a>

				</div>
				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-orderpost']) ?>">

						<div class="infobox infobox-pink">
							<div class="infobox-icon">
								<i class="icon-shopping-cart"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">8</span>
								<div class="infobox-content">Đơn hàng</div>
							</div>
							<!--					<div class="stat stat-important">4%</div>-->
						</div>
					</a>

				</div>
				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-customer']) ?>">

						<div class="infobox infobox-pink">
							<div class="infobox-icon">
								<i class="icon-group"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">8</span>
								<div class="infobox-content">Khách hàng</div>
							</div>
							<!--						<div class="stat stat-important">4%</div>-->
						</div>
					</a>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="row">
			<div class="space-6"></div>

			<div class="col-sm-12 infobox-container">
				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-inbox']) ?>">

						<div class="infobox infobox-green ">
							<div class="infobox-icon">
								<i class="icon-inbox"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">32</span>
								<div class="infobox-content">Inbox</div>
							</div>
							<!--					<div class="stat stat-success">8%</div>-->
						</div>
					</a>

				</div>
				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-sale']) ?>">

						<div class="infobox infobox-blue">
							<div class="infobox-icon">
								<i class="icon-money"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">11</span>
								<div class="infobox-content">Doanh số</div>
							</div>

							<!--					<div class="badge badge-success">-->
							<!--						+32%-->
							<!--						<i class="icon-arrow-up"></i>-->
							<!--					</div>-->
						</div>
					</a>

				</div>
				<div class="col-sm-3">
					<a href="<?= \yii\helpers\Url::to(['/report/report-logistic']) ?>">

						<div class="infobox infobox-pink">
							<div class="infobox-icon">
								<i class="icon-truck"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number">8</span>
								<div class="infobox-content">Vận chuyển</div>
							</div>
							<!--					<div class="stat stat-important">4%</div>-->
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>