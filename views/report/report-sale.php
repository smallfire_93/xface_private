<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;

$this->title                   = Yii::t('app', 'Báo cáo chốt sale');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-info">
	<div class="panel panel-heading">
		<b>Tìm kiếm</b>
	</div>
	<div class="panel panel-body">
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
		<?= Html::dropDownList('order-status', [], [], ['prompt' => 'Trạng thái đơn hàng']) ?>
		<?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'From date'],
		]) ?>        <?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'To date'],
		]) ?>
		<div class="row">
			<div class="col-sm-12 padding-top">
				<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
				<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
			</div>
		</div>
	</div>
</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo chốt sale'],
		'xAxis'  => [
			'categories' => [
				'01',
				'02',
				'03',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Số đơn'],
		],
		'series' => [
			[
				'name' => 'Số đơn',
				'data' => [
					1,
					0,
					4,
				],
			],
		],
	],
]);
?>
<div class="row border-top">
	<div class="col-sm-12">
		<div class="row">
			<div class="space-6"></div>

			<div class="col-sm-12 infobox-container">
				<div class="col-sm-3">

					<div class="infobox infobox-green ">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">32</span>
							<div class="infobox-content">Tổng số Đơn hàng</div>
						</div>
						<!--					<div class="stat stat-success">8%</div>-->
					</div>
				</div>
				<div class="col-sm-3">

					<div class="infobox infobox-blue">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">11</span>
							<div class="infobox-content">Đã chốt</div>
						</div>

						<!--					<div class="badge badge-success">-->
						<!--						+32%-->
						<!--						<i class="icon-arrow-up"></i>-->
						<!--					</div>-->
					</div>
				</div>
				<div class="col-sm-3">

					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">10</span>
							<div class="infobox-content">Chưa chốt</div>
						</div>
						<!--					<div class="stat stat-important">4%</div>-->
					</div>
				</div>
				<div class="col-sm-3">
					<div class="infobox infobox-pink">
						<div class="infobox-icon">
							<i class="icon-shopping-cart"></i>
						</div>

						<div class="infobox-data">
							<span class="infobox-data-number">1</span>
							<div class="infobox-content">Thành công</div>
						</div>
						<!--						<div class="stat stat-important">4%</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 top-buffer">
		<h3>Thống kê từ 01/02/2017 đến 01/03/2017</h3>
	</div>
</div>

<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>Nhân viên</th>

			<th>
				Đã chốt
			</th>
			<th>
				Tổng đơn chưa chốt
			</th>
			<th>
				Tổng đơn thành công
			</th>
			<th>
				Tổng đơn
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Nhân viên 1</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>
		<tr>

			<td>Nhân viên 2</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>

		<tr>

			<td>Tổng</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>

		</tbody>
	</table>
</div>
