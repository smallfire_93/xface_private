<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;

$this->title                   = Yii::t('app', 'Báo cáo inbox');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-info">
	<div class="panel panel-heading">
		<b>Tìm kiếm</b>
	</div>
	<div class="panel panel-body">
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
		<?= \phamxuanloc\jui\DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'From date'],
		]) ?>        <?= \phamxuanloc\jui\DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'To date'],
		]) ?>
		<div class="row">
			<div class="col-sm-12 padding-top">
				<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
				<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
			</div>
		</div>
	</div>
</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo inbox'],
		'xAxis'  => [
			'categories' => [
				'01',
				'02',
				'03',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Inbox'],
		],
		'series' => [
			[
				'name' => 'Inbox theo ngày',
				'data' => [
					1,
					0,
					4,
				],
			],
		],
	],
]);
?>
<div class="panel panel-info">

	<div class="panel panel-body">
		<div class="col-sm-12">
			<div class="col-sm-6 alert-info">
				<div class="col-sm-6">Ngày inbox nhiều nhất:</div>
				<div class="col-sm-6">30-02-2017 170 comments</div>
			</div>
			<div class="col-sm-12"></div>
			<div class="col-sm-6 alert-success">
				<div class="col-sm-6">Thời gian inbox nhiều nhất:</div>
				<div class="col-sm-6">3h-4h 10 comments</div>
			</div>
			<div class="col-sm-12"></div>
		</div>
	</div>
</div>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th class="col-sm-10">
				<i class="icon-time bigger-110 hidden-480"></i>

				Ngày
			</th>

			<th>
				Tổng số inbox
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Feb 12</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>3,330</td>

		</tr>

		</tbody>
	</table>
</div>

<div class="col-sm-10">Tổng số:</div>
<div class="col-sm-2">
	<div style="font-weight: bold">100</div>
</div>

