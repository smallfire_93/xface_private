<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;

$this->title                   = Yii::t('app', 'Báo cáo comment');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-info">
	<div class="panel panel-heading">
		<b>Tìm kiếm</b>
	</div>
	<div class="panel panel-body">
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
		<?= Html::dropDownList('comment', [], [], ['prompt' => 'Tất cả comment']) ?>
		<?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'From date'],
		]) ?>        <?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'To date'],
		]) ?>
		<div class="row">
			<div class="col-sm-12 padding-top">
				<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
				<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
			</div>
		</div>
	</div>
</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo comment'],
		'xAxis'  => [
			'categories' => [
				'01',
				'02',
				'03',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Comments'],
		],
		'series' => [
			[
				'name' => 'Comment theo ngày',
				'data' => [
					1,
					0,
					4,
				],
			],
		],
	],
]);
?>
<div class="panel panel-info">

	<div class="panel panel-body">
		<div class="col-sm-12">
			<div class="col-sm-6 alert-info">
				<div class="col-sm-6">Ngày comment nhiều nhất:</div>
				<div class="col-sm-6">30-02-2017 170 comments</div>
			</div>
			<div class="col-sm-12"></div>
			<div class="col-sm-6 alert-success">
				<div class="col-sm-6">Thời gian comment nhiều nhất:</div>
				<div class="col-sm-6">3h-4h 10 comments</div>
			</div>
			<div class="col-sm-12"></div>
		</div>
	</div>
</div>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>
				<i class="icon-time bigger-110 hidden-480"></i>

				Ngày
			</th>
			<th>Bài viết</th>

			<th>
				Tổng số
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Feb 12</td>
			<td>Appota has built a professional working environment that is dynamic, modern, youth driven, Vietnamese based, and internationally inspired. In our open environment, employees have the opportunity to maximize their expertise, creativity, and gaming passion. At Appota, employees do not have to navigate relationships between superiors and subordinates, but instead, a culture where there are “No Bosses, only Leaders”.</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>Appota has built a professional working environment that is dynamic, modern, youth driven, Vietnamese based, and internationally inspired. In our open environment, employees have the opportunity to maximize their expertise, creativity, and gaming passion. At Appota, employees do not have to navigate relationships between superiors and subordinates, but instead, a culture where there are “No Bosses, only Leaders”.</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>Appota has built a professional working environment that is dynamic, modern, youth driven, Vietnamese based, and internationally inspired. In our open environment, employees have the opportunity to maximize their expertise, creativity, and gaming passion. At Appota, employees do not have to navigate relationships between superiors and subordinates, but instead, a culture where there are “No Bosses, only Leaders”.</td>
			<td>3,330</td>

		</tr>

		</tbody>
	</table>
</div>
<div class="col-sm-6">Tổng số:</div>
<div class="col-sm-6">
	<div style="float: right; font-weight: bold">100</div>
</div>