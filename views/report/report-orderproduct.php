<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;
use yii\web\JsExpression;

$this->title                   = Yii::t('app', 'Báo cáo đơn hàng theo sản phẩm');
$this->params['breadcrumbs'][] = $this->title;
?>

	<div class="panel panel-info">
		<div class="panel panel-heading">
			<b>Tìm kiếm</b>
		</div>
		<div class="panel panel-body">
			<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
			<?= DateTimePicker::widget([
				'name'       => 'date',
				'dateFormat' => 'yyyy-MM-dd',
				'options'    => ['placeholder' => 'From date'],
			]) ?>        <?= DateTimePicker::widget([
				'name'       => 'date',
				'dateFormat' => 'yyyy-MM-dd',
				'options'    => ['placeholder' => 'To date'],
			]) ?>
			<div class="row">
				<div class="col-sm-12 padding-top">
					<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
					<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
				</div>
			</div>
		</div>
	</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo đơn hàng theo sản phẩm'],
		'xAxis'  => [
			'categories' => [
				'01',
				'02',
				'03',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Đơn hàng'],
		],
		'series' => [
			[
				'name' => 'Đơn hàng',
				'data' => [
					1,
					0,
					4,
				],
			],
		],
	],
]);
?>
<?php
echo Highcharts::widget([
	'options' => [
		'title'       => ['text' => 'Thành phần khách hàng'],
		'plotOptions' => [
			'pie' => [
				'cursor' => 'pointer',
			],
		],
		'series'      => [
			[ // new opening bracket
				'type' => 'pie',
				'name' => 'Báo cáo đơn hàng',
				'data' => [
					[
						'name'  => 'Đơn hàng hoàn thành',
						'y'     => 13,
						'color' => new JsExpression('Highcharts.getOptions().colors[5]'),
					],
					[
						'name'  => 'Đơn hàng đang giao',
						'y'     => 70,
						'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
					],
					[
						'name'  => 'Đơn hàng hồi hoàn',
						'y'     => 70,
						'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
					],
				],
				'size' => 150,
			]
			// new closing bracket
		],
	],
]);
?>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>Sản phẩm</th>
			<th>Khách hàng</th>
			<th>Đơn hàng</th>
			<th>Đơn hàng hoàn thành</th>

			<th>
				Tổng số
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Appota has built a professional working environment that is dynamic”.</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Appota has built a professional working environment that is dynamic”.</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>

		</tr>
		<tr>

			<td>Appota has built a professional working environment that is dynamic”.</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>3,330</td>

		</tr>

		</tbody>
	</table>
</div>
<div class="col-sm-6">Tổng số:</div>
<div class="col-sm-6">
	<div style="float: right; font-weight: bold">100</div>
</div>
