<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 20-Mar-17
 * Time: 1:46 PM
 */
use miloschuman\highcharts\Highcharts;
use phamxuanloc\jui\DateTimePicker;
use yii\bootstrap\Html;
use yii\web\JsExpression;

$this->title                   = Yii::t('app', 'Báo cáo comment');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-info">
	<div class="panel panel-heading">
		<b>Tìm kiếm</b>
	</div>
	<div class="panel panel-body">
		<?= Html::dropDownList('fanpage', [], [], ['prompt' => 'Chọn fanpage']) ?>
		<?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'From date'],
		]) ?>        <?= DateTimePicker::widget([
			'name'       => 'date',
			'dateFormat' => 'yyyy-MM-dd',
			'options'    => ['placeholder' => 'To date'],
		]) ?>
	</div>
	<div class="row">
		<div class="col-sm-12 padding-top">
			<?= Html::button('Báo cáo', ['class' => 'btn btn-success']) ?>
			<?= Html::button('Export', ['class' => 'btn btn-warning']) ?>
		</div>
	</div>
</div>
<?php
echo Highcharts::widget([
	'options' => [
		'title'  => ['text' => 'Báo cáo khách hàng'],
		'xAxis'  => [
			'categories' => [
				'Apples',
				'Bananas',
				'Oranges',
			],
		],
		'yAxis'  => [
			'title' => ['text' => 'Khách hàng'],
		],
		'series' => [
			[
				'name' => 'Khách hàng theo ngày',
				'data' => [
					1,
					0,
					4,
				],
			],
		],
	],
]);
?>
<?php
echo Highcharts::widget([
	'options' => [
		'title'       => ['text' => 'Thành phần khách hàng'],
		'plotOptions' => [
			'pie' => [
				'cursor' => 'pointer',
			],
		],
		'series'      => [
			[ // new opening bracket
				'type' => 'pie',
				'name' => 'Báo cáo khách hàng',
				'data' => [
					[
						'name'  => 'Khách hàng cũ',
						'y'     => 13,
						'color' => new JsExpression('Highcharts.getOptions().colors[5]'),
					],
					[
						'name'  => 'Khách hàng mới',
						'y'     => 70,
						'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
					],
				],
				'size' => 150,
			]
			// new closing bracket
		],
	],
]);
?>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>
				<i class="icon-time bigger-110 hidden-480"></i>

				Ngày
			</th>
			<th>Tổng số khách hàng</th>

			<th>
				Khách hàng mới
			</th>
			<th>
				Khách hàng cũ
			</th>
			<th>
				Doanh thu
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Feb 12</td>
			<td>100</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>
		<tr>

			<td>Feb 12</td>
			<td>100</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>
		<tr style="border-top: 1px solid green">

			<td>Tổng doanh thu:</td>
			<td>10000</td>
			<td>3,33000</td>
			<td>3,33000</td>
			<td>10,000,000</td>

		</tr>

		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table id="sample-table-2" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>

			<th>

				Khách hàng
			</th>
			<th>Tổng số đơn hàng</th>

			<th>
				Đơn hàng đang thực hiện
			</th>
			<th>
				Đơn hàng hoàn thành
			</th>
			<th>
				Đơn hàng hồi hoàn
			</th>

		</tr>
		</thead>

		<tbody>
		<tr>

			<td>Nguyễn văn A</td>
			<td>100</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>
		<tr>

			<td>Nguyễn văn A</td>
			<td>100</td>
			<td>3,330</td>
			<td>3,330</td>
			<td>10,000</td>

		</tr>

		<tr style="border-top: 1px solid green">

			<td>Tổng doanh thu:</td>
			<td>10000</td>
			<td>3,33000</td>
			<td>3,33000</td>
			<td>10,000,000</td>

		</tr>

		</tbody>
	</table>
</div>
<!--<div class="col-sm-6">Tổng doanh thu:</div>-->
<!--<div class="col-sm-6">-->
<!--	<div style="float: right; font-weight: bold">100</div>-->
<!--</div>-->
