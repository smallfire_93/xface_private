<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use miloschuman\highcharts\Highcharts;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Báo cáo comment';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
    $hostname    =   [];
    $total  =   [];
    $day    =   [];
    $comment    =   [];
    foreach($getCommnetDayByDAy as $item){
        array_push($hostname    ,   $item->comment_date);
        array_push($total    ,   $item->total_comment);
    }
    foreach($getTotalComment as $item){
        array_push($day    ,   $item->comment_date);
        array_push($comment    ,   $item->total_comment);
    }
?>

<div class="report-comment-index">
    <?php
        $gridColumns    =   [
            ['class' => 'yii\grid\SerialColumn'],
//            [
//                'header' => 'Ngày',
//                'value' => 'post_id.created_date',
//                'attribute' => 'post_id',
//            ],
//            [
//                'header' => 'Bài viết',
//                'value' => 'post_id.content',
//                'attribute' => 'post_id',
//            ],
            [
                'header' => 'Tổng số comment',
                'value' => 'total_comment',
                'attribute' => 'total_comment',
            ],
        ]
    ?>
</div>

<div class="page-content">
    <div class="page-header">
        <h1>
            Báo cáo Comment
            <small>
                <i class="icon-double-angle-right"></i>
            </small>
        </h1>
    </div><!-- /.page-header -->
    <div class="row" style="margin-top: 20px">
        <div class="">

        </div>
        <div class="col-xs-12">
            <div class="well">
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>
    </div>
    <?php
    echo Highcharts::widget([
        'options' => [
            'title'  => ['text' => 'Báo cáo comment'],
            'xAxis'  => [
                'categories' => $hostname,
            ],
            'yAxis'  => [
                'title' => ['text' => 'Comments'],
            ],
            'series' => [
                [
                    'name' => 'Comment theo ngày',
                    'data' =>  $total,
                ],
            ],
        ],
    ]);
    ?>
    <div class="panel panel-info">

        <div class="panel panel-body">
            <div class="col-sm-12">
                <div class="col-sm-6 alert-info">
                    <div class="col-sm-6">Ngày comment nhiều nhất:</div>
                    <div class="col-sm-6"><?php echo $item->comment_date ?>---<?php echo $item->total_comment ?> comments</div>
                </div>
                <div class="col-sm-12"></div>
                <div class="col-sm-6 alert-success">
                    <div class="col-sm-6">Thời gian comment nhiều nhất:</div>
                    <div class="col-sm-6">3h-4h 10 comments</div>
                </div>
                <div class="col-sm-12"></div>
            </div>
        </div>
    </div>
    <?php echo ExportMenu::widget([
        'dataProvider'    => $dataProvider,
        'columns'         => $gridColumns,
        'fontAwesome'     => true,
        'dropdownOptions' => [
            'label' => 'Export',
            'class' => 'btn btn-warning',
        ],
        'exportConfig'    => [
            ExportMenu::FORMAT_TEXT  => false,
            ExportMenu::FORMAT_PDF   => false,
            ExportMenu::FORMAT_HTML  => false,
            ExportMenu::FORMAT_EXCEL => false,
        ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => $gridColumns,
    ]); ?>

    <div class="row">
        <div class="col-sm-3">
            <h3><b>Tổng</b></h3>
        </div>
    </div>
</div>