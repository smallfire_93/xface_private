<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fanpage_id')->textInput() ?>

    <?= $form->field($model, 'company_id')->textInput() ?>

    <?= $form->field($model, 'total_comment')->textInput() ?>

    <?= $form->field($model, 'hour_1')->textInput() ?>

    <?= $form->field($model, 'hour_2')->textInput() ?>

    <?= $form->field($model, 'hour_3')->textInput() ?>

    <?= $form->field($model, 'hour_4')->textInput() ?>

    <?= $form->field($model, 'hour_5')->textInput() ?>

    <?= $form->field($model, 'hour_6')->textInput() ?>

    <?= $form->field($model, 'hour_7')->textInput() ?>

    <?= $form->field($model, 'hour_8')->textInput() ?>

    <?= $form->field($model, 'hour_9')->textInput() ?>

    <?= $form->field($model, 'hour_10')->textInput() ?>

    <?= $form->field($model, 'hour_11')->textInput() ?>

    <?= $form->field($model, 'hour_12')->textInput() ?>

    <?= $form->field($model, 'post_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
