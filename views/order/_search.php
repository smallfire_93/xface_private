<?php
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

	<?php $form = ActiveForm::begin([
		'action' => ['list'],
		'method' => 'get',
		'layout' => 'inline',
	]); ?>
	<div class="row show-grid">
		<div class="col-sm-12">

			<div class="col-sm-3">
				<?php echo $form->field($model, 'user_id_create', ['options' => ['class' => 'row col-sm-9 col-xs-12']])->widget(Select2::className(), [
					'data'    => [],
					'options' => ['prompt' => 'Người tạo đơn'],
				]) ?>
			</div>
			<div class="col-sm-3">
				<?php echo $form->field($model, 'user_id_process', ['options' => ['class' => 'row col-sm-9 col-xs-12']])->widget(Select2::className(), [
					'data'    => [],
					'options' => ['prompt' => 'Người đang xử lý'],
				]) ?>
			</div>
			<div class="col-sm-3">

				<?php echo $form->field($model, 'customer_email')->textInput(['placeholder' => 'email']) ?>
			</div>
			<div class="col-sm-3">

				<?php echo $form->field($model, 'phone')->textInput(['placeholder' => 'Số điện thoại khách hàng']) ?>
			</div>
		</div>
	</div>
	<div class="row show-grid">
		<div class="col-sm-12">

			<div class="col-sm-3">

				<?php echo $form->field($model, 'code')->textInput(['placeholder' => 'Mã đơn hàng']) ?>
			</div>
			<div class="col-sm-3">

				<?php echo $form->field($model, 'status')->dropDownList($model::STATUS, ['prompt' => 'Trạng thái đơn']) ?>
			</div>
			<div class="col-sm-4">
				<div class="input-group input-group-sm">
					<input type="text" id="ordersearch-from_date" class="form-control" placeholder="Từ ngày" name="OrderSearch[from_date]">
					<span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
					<input type="text" id="ordersearch-to_date" class="form-control" placeholder="Đến ngày" name="OrderSearch[to_date]">
					<span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
				</div>
			</div>
		</div>
	</div>
	<?php // echo $form->field($model, 'payment_status') ?>

	<?php // echo $form->field($model, 'shipping_status') ?>

	<?php // echo $form->field($model, 'payment_id') ?>

	<?php // echo $form->field($model, 'customer_note') ?>

	<?php // echo $form->field($model, 'fb_user_id') ?>

	<?php // echo $form->field($model, 'tracking') ?>

	<?php // echo $form->field($model, 'export_date') ?>

	<?php // echo $form->field($model, 'receiver_date') ?>

	<?php // echo $form->field($model, 'shipping_fee') ?>

	<?php // echo $form->field($model, 'code_fee') ?>

	<?php // echo $form->field($model, 'address') ?>


	<?php // echo $form->field($model, 'transport_id') ?>

	<?php // echo $form->field($model, 'discount') ?>

	<div class="" style="padding-left: 15px">
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
