<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use app\models\Order;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = "Đơn hàng";
$this->params['breadcrumbs'][] = "Danh sách đơn hàng";
?>
<div class="page-header">
	<h1>
		Quản lí đơn hàng
		<small>
			<i class="icon-double-angle-right"></i>
			Danh sách đơn hàng
		</small>
	</h1>
</div><!-- /.page-header -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-info">
			<div class="panel-heading">Báo cáo tổng quan</div>
			<div class="panel-body">
				<div class="">
					<div class="form-group">
						<div class="col-sm-3">
							<p>
								Tổng số đơn hàng : 20
							</p>
						</div>
						<div class="col-sm-3">
							<p>
								Số đơn hàng đang xử lí : 10
							</p>
						</div>
						<div class="col-sm-3">
							<p>
								Số đơn hàng đã hoàn thành : 10
							</p>
						</div>
						<div class="col-sm-3">
							<p>
								Tổng số khách hàng : 100
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-info">
			<div class="panel-heading">Tìm kiếm</div>
			<div class="">
				<?php echo $this->render('_search', ['model' => $searchModel]) ?>
			</div>
		</div>
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			//	'filterModel' => $searchModel,
			'columns'      => [
				//					['class' => 'kartik\grid\SerialColumn'],
				'code',
				[
					'attribute' => 'user_id_create',
					'value'     => function (Order $data) {
						return $data->userCreate->username;
					},
				],
				[
					'attribute' => 'user_id_process',
					'value'     => function (Order $data) {
						return $data->userProcess->username;
					},
				],
				'quantity',
				[
					'attribute' => 'total_money',
					'value'     => function (Order $data) {
						return number_format($data->total_money);
					},
				],
				'address',
				'phone',
				[
					'attribute' => 'customer_email',
					'value'     => function (Order $data) {
						$data->customer->email;
					},
				],
				'customer_id',
				[
					'attribute' => 'payment_status',
					'value'     => function (Order $data) {
						return "<span class='{$data->getColorPayment()}'>" . $data::PAYMENT_STATUS[$data->payment_status] . "</span>";
					},
				],
				[
					'attribute' => 'shipping_status',
					'value'     => function (Order $data) {
						return "<span class='{$data->getColorShipping()}'>" . $data::SHIPPING_STATUS[$data->shipping_status] . "</span>";
					},
				],
				// 'payment_id',
				// 'customer_note',
				// 'fb_user_id',
				// 'tracking',
				// 'export_date',
				// 'receiver_date',
				// 'shipping_fee',
				// 'code_fee',
				// 'transport_id',
				// 'discount',
				//		 'status',
				[
					'class'  => 'kartik\grid\ActionColumn',
					'header' => 'Chức năng',
				],
			],
		]); ?>
	</div>
</div>
