<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'user_id_create',
            'user_id_process',
            'quantity',
            // 'total_money',
            // 'customer_id',
            // 'payment_status',
            // 'shipping_status',
            // 'payment_id',
            // 'customer_note',
            // 'fb_user_id',
            // 'tracking',
            // 'export_date',
            // 'receiver_date',
            // 'shipping_fee',
            // 'code_fee',
            // 'address',
            // 'phone',
            // 'transport_id',
            // 'discount',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
