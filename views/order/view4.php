<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use yii\widgets\Breadcrumbs;

$this->title = "Đơn hang kho vận";
$this->params['breadcrumbs'][] = "Đơn hàng kho vận";
?>

<div class="page-content" xmlns="http://www.w3.org/1999/html">
<div class="page-header">
    <h1>
        Chi tiết đơn hàng kho vận
        <small>
            <i class="icon-double-angle-right"></i>
            Đơn hàng 1
        </small>
    </h1>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="col-sm-3">
            <p>
                Tổng giá trị: 200.000 VNĐ
            </p>
        </div>
        <div class="col-sm-3">
            <p>
                Ngày bán : 08:15  31/3/2017
            </p>
        </div>
        <div class="col-sm-3">
            <button class="btn btn-success btn-sm">Đã thanh toán</button>
        </div>
        <div class="col-sm-3">
            <div class="clearfix ">
                <button class="btn-pink btn-sm" type="button">
                    <i class="icon-print"></i>
                    <a href="" style="color: #000000">In đơn hàng</a>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="page-header">
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="form-horizontal" role="form">
            <form class="form-group">
                <label class="col-sm-1 control-label no-padding-right">Trạng thái</label>
                <div class="col-sm-2">
                    <select id="user-status" class="form-control" name="User[status]" aria-invalid="false">
                        <option value="">---Trạng thái---</option>
                        <option value="1" selected="">Active</option>
                        <option value="0">Block</option>
                    </select>
                    <div class="help-block help-block-error "></div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group field-news-description has-success" >
                        <textarea id="news-description" class="form-control" name="News[description]" rows="6" placeholder="Ghi chú"></textarea>

                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="clearfix ">
                        <button class="btn btn-info btn-sm" type="button">
                            <i class="icon-edit"></i>
                            <a href="" style="color: #000000">Cập nhật</a>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="page-header">
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Thông tin vận chuyển</h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Đơn vị vận chuyển</label>
                            <div class="col-sm-4">
                                <select id="user-status" class="form-control" name="User[status]" aria-invalid="false">
                                    <option value="">---Đơn vị---</option>
                                    <option value="1" selected="">Giao hàng nhanh</option>
                                    <option value="0">Viettel Post</option>
                                </select>
                                <div class="help-block help-block-error "></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Phí vận chuyển </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-2" placeholder="20.000 VNĐ" class="col-xs-10 col-sm-5" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Mã vận chuyển </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-2" placeholder="IP2017" class="col-xs-10 col-sm-5" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Ngày lấy hàng </label>
                            <div class="col-sm-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" id="datepicker" class="form-control hasDatepicker">
                                                <span class="input-group-addon">
                                                    <i class="icon-calendar"></i>
                                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Ngày nhận hàng </label>
                            <div class="col-sm-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" id="datepicker" class="form-control hasDatepicker">
                                                <span class="input-group-addon">
                                                    <i class="icon-calendar"></i>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Địa chỉ nhận hàng</h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Tên người nhận </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-2" placeholder="Nguyễn văn A" class="col-xs-10 col-sm-5" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Địa chỉ </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-2" placeholder="75 Nguyễn Trãi Hà Nội" class="col-xs-10 col-sm-5" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Điện thoại </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-2" placeholder="098765475" class="col-xs-10 col-sm-5" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Facebook </label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-2" placeholder="https://www.facebook.com/luongcuong22" class="col-xs-10 col-sm-5" />

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="page-header">
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="col-sm-3">
            <img style="width: 100px;height: 100px" src="http://vhshop.vn/uploads/images/Balo-cac-loai/Toppu/TP-515-Blue/Balo-The-Toppu-TP-515-blue-1.png" alt="mô tả ảnh">
            <p>
                Tổng giá trị: 200.000 VNĐ
            </p>
            <p>Tên sản phẩm : Balo</p>
            <p>Mã sản phẩm : Ms01</p>
        </div>
        <div class="col-sm-3">
            <p>
                Số lượng * Giá tiền : 1*200.000 VNĐ
            </p>
        </div>
        <div class="col-sm-3">
            <p>
                Tổng tiền : 200.000 VNĐ
            </p>
        </div>
        <div class="col-sm-3">
            <div class="clearfix ">
                <button class="btn btn-app btn-danger btn-xs">
                    <i class="icon-trash bigger-200"></i>
                    Delete
                </button>
            </div>
        </div>
    </div>
</div>
<div class="page-header">
</div>
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Ghi chú đơn hàng</h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <p>hàng dễ vỡ, xin cẩn thận</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Ghi chú của khách hàng</h4>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <p>
                        yêu cầu giao hàng đúng giờ
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Tổng chi phí</h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <p class="muted">Tiền sản phẩm : 200.000 VNĐ</p>
                    <p class="muted">Phí vận chuyển và thu tiền : 20.000 VNĐ</p>
                    <p class="muted">Khuyến mãi : 0</p>
                    <div class="page-header">
                    </div>
                    <p class="muted">Tổng tiền : 220.000</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>