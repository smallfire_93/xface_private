<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'user_id_create',
            'user_id_process',
            'quantity',
            'total_money',
            'customer_id',
            'payment_status',
            'shipping_status',
            'payment_id',
            'customer_note',
            'fb_user_id',
            'tracking',
            'export_date',
            'receiver_date',
            'shipping_fee',
            'code_fee',
            'address',
            'phone',
            'transport_id',
            'discount',
            'status',
        ],
    ]) ?>

</div>
