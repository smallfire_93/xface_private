<div class="wrapper-content">
    <table class="table-normal table-responsive">
        <?php foreach ($product_order as $p) : ?>
            <tr>
                <td class="width-45-px min-width-45-px">
                    <div class="wrap-img vertical-align-m-i">
                        <img class="thumb-image" src="<?= $p['image'] ?>"
                             title="<?= $p['name'] ?>">
                    </div>
                </td>
                <td>
                    <ul>
                        <li style="margin-bottom:2px;">
                            <a class="text-underline hover-underline pre-line" href="" target="_blank" data-original-title="" title=""><?= $p['name'] ?></a>
                        </li>
                        <li>
                            <div class="dropdown-priceOrderNew dropdown-ctkm-harapage inline_block vertical-align-m-i">
                                <div class="inline_block">
                                    <a class="wordwrap hide-print show-dropdown"><?= number_format($p['price'], '0', '.', ',') ?> ₫</a>
                                </div>
                            </div>
                            <!--/ko-->
                            <div class="inline_block vertical-align-m-i">
                                <!--ko if : !ProductId()--><!--/ko-->
                                <span class="pl5 p-r5">x</span>
                                <input class="form-control inline_block p-none-r width-50-px min-width-50-px" type="number" min="1" value="<?= $p['quantity']?>">
                                <span class="pl5 p-r5">=</span>
                                <span><?= number_format($p['price'] * $p['quantity'], '0', '.', ',') ?> ₫</span>
                            </div>
                        </li>
                    </ul>
                </td>
                <td class="text-right width-10-px min-width-10-px">
                    <a class="remove" onclick="remove_card(<?= $p['id']?>)">
                        <i class="fa fa-times color-stateGray"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>