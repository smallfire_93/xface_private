<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportShipping */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-shipping-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'total_customer')->textInput() ?>

    <?= $form->field($model, 'total_order_finish')->textInput() ?>

    <?= $form->field($model, 'total_order_pending')->textInput() ?>

    <?= $form->field($model, 'total_order_feedback')->textInput() ?>

    <?= $form->field($model, 'total_money')->textInput() ?>

    <?= $form->field($model, 'fanpage_id')->textInput() ?>

    <?= $form->field($model, 'company_id')->textInput() ?>

    <?= $form->field($model, 'transport_id')->textInput() ?>

    <?= $form->field($model, 'transport_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_order_cod')->textInput() ?>

    <?= $form->field($model, 'total_order_cod_finish')->textInput() ?>

    <?= $form->field($model, 'total_order_cod_feedback')->textInput() ?>

    <?= $form->field($model, 'total_money_debt')->textInput() ?>

    <?= $form->field($model, 'total_money_real')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
