<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReportShipping */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Report Shippings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-shipping-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_date',
            'total_customer',
            'total_order_finish',
            'total_order_pending',
            'total_order_feedback',
            'total_money',
            'fanpage_id',
            'company_id',
            'transport_id',
            'transport_name',
            'total_order_cod',
            'total_order_cod_finish',
            'total_order_cod_feedback',
            'total_money_debt',
            'total_money_real',
        ],
    ]) ?>

</div>
