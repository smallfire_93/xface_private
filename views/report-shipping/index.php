<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use miloschuman\highcharts\Highcharts;
use app\models\ReportShipping;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportShippingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Shippings';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$hostname    =   [];
$total  =   [];

foreach($getShippingDayByDAy as $item){
    array_push($hostname    ,   $item->created_date);
    array_push($total    ,   $item->total_customer);
}

?>

<div class="report-shipping-index">

    <?php
    $gridColumns    =   [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'header' => 'Ngày',
            'value' => 'created_date',
            'attribute' => 'created_date',
        ],
        [
            'header' => 'Đơn vị vận chuyển',
            'value' => 'transport_id.name',
            'attribute' => 'transport_id',
        ],
        [
            'header' => 'Số đơn thu hộ',
            'value' => 'total_order_cod',
            'attribute' => 'total_order_cod',
        ],
        [
            'header' => 'Số đơn đã thu',
            'value' => 'total_order_cod_finish',
            'attribute' => 'total_order_cod_finish',
        ],
        [
            'header' => 'Số đơn hồi hoàn',
            'value' => 'total_order_cod_feedback',
            'attribute' => 'total_order_cod_feedback',
        ],
        [
            'header' => 'Cần thanh toán',
            'value' => 'total_money_debt',
            'attribute' => 'total_money_debt',
        ],
        [
            'header' => 'Đã thanh toán',
            'value' => 'total_money_real',
            'attribute' => 'total_money_real',
        ],
        [
            'header' => 'Còn lại',
        ],
    ]
    ?>
    <div class="page-content">
        <div class="page-header">
            <h1>
                Báo cáo vận chuyển
                <small>
                    <i class="icon-double-angle-right"></i>
                </small>
            </h1>
        </div><!-- /.page-header -->
        <div class="row" style="margin-top: 20px">
            <div class="">

            </div>
            <div class="col-xs-12">
                <div class="well">
                    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>

        <?php
        echo Highcharts::widget([
            'options' => [
                'title'  => ['text' => 'Báo cáo vận chuyển'],
                'xAxis'  => [
                    'categories' => $hostname,
                ],
                'yAxis'  => [
                    'title' => ['text' => 'Khách hàng'],
                ],
                'series' => [
                    [
                        'name' => 'Báo cáo vận chuyển theo ngày',
                        'data' => $total,
                    ],
                ],
            ],
        ]);
        ?>
<!--            --><?php
//            $gridColumns    =   [
//                ['class' => 'yii\grid\SerialColumn'],
//                [
//                    'header' => 'Ngày',
//                    'value' => 'created_date',
//                    'attribute' => 'created_date',
//                ],
//                [
//                    'header' => 'Đơn vị vận chuyển',
//                    'value' => 'transport_id.name',
//                    'attribute' => 'transport_id',
//                ],
//                [
//                    'header' => 'Tổng số đơn hàng',
//                    'value' => 'total_order_cod',
//                    'attribute' => 'total_order_cod',
//                ],
//                [
//                    'header' => 'Đơn hàng hoàn thành',
//                    'value' => 'total_order_finish',
//                    'attribute' => 'total_order_finish',
//                ],
//                [
//                    'header' => 'Đơn hàng hồi hoàn',
//                    'value' => 'total_pending_feedback',
//                    'attribute' => 'total_pending_feedback',
//                ],
//            ]
//            ?>

        <div class="row">
            <div class="col-sm-12 top-buffer">
                <h3>Vận chuyển</h3>
            </div>
        </div>
        <div class="row border-top">
            <div class="col-sm-12">
                <div class="row">
                    <div class="space-6"></div>

                    <div class="col-sm-12 infobox-container">
                        <div class="col-sm-3">

                            <div class="infobox infobox-green ">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order_pending  ?></span>
                                    <div class="infobox-content">Đơn chưa vận chuyển</div>
                                </div>
                                <!--					<div class="stat stat-success">8%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-blue">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_not_meet; ?></span>
                                    <div class="infobox-content">Giao không gặp khách</div>
                                </div>

                                <!--					<div class="badge badge-success">-->
                                <!--						+32%-->
                                <!--						<i class="icon-arrow-up"></i>-->
                                <!--					</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order_feedback; ?></span>
                                    <div class="infobox-content">Chờ hồi hoàn</div>
                                </div>
                                <!--					<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order_finish; ?></span>
                                    <div class="infobox-content">Đã hoàn thành</div>
                                </div>
                                <!--						<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <?php echo ExportMenu::widget([
            'dataProvider'    => $dataProvider,
            'columns'         => $gridColumns,
            'fontAwesome'     => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-warning',
            ],
            'exportConfig'    => [
                ExportMenu::FORMAT_TEXT  => false,
                ExportMenu::FORMAT_PDF   => false,
                ExportMenu::FORMAT_HTML  => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => $gridColumns,
        ]); ?>

        <div class="row">
            <div class="col-sm-12 top-buffer">
                <h3>Thu hộ</h3>
            </div>
        </div>
        <div class="row border-top">
            <div class="col-sm-12">
                <div class="row">
                    <div class="space-6"></div>

                    <div class="col-sm-12 infobox-container">
                        <div class="col-sm-3">

                            <div class="infobox infobox-green ">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order_cod; ?></span>
                                    <div class="infobox-content">Số đơn thu hộ</div>
                                </div>
                                <!--					<div class="stat stat-success">8%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-blue">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_money_debt; ?></span>
                                    <div class="infobox-content">Số tiền thu hộ</div>
                                </div>

                                <!--					<div class="badge badge-success">-->
                                <!--						+32%-->
                                <!--						<i class="icon-arrow-up"></i>-->
                                <!--					</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_money_real; ?></span>
                                    <div class="infobox-content">Số tiền đã trả</div>
                                </div>
                                <!--					<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">1.000.000</span>
                                    <div class="infobox-content">Số tiền còn lại</div>
                                </div>
                                <!--						<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <?php echo ExportMenu::widget([
            'dataProvider'    => $dataProvider,
            'columns'         => $gridColumns,
            'fontAwesome'     => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-warning',
            ],
            'exportConfig'    => [
                ExportMenu::FORMAT_TEXT  => false,
                ExportMenu::FORMAT_PDF   => false,
                ExportMenu::FORMAT_HTML  => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => $gridColumns,
        ]); ?>

    </div>
</div>