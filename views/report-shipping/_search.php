<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use phamxuanloc\jui\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ReportShippingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-shipping-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3">
        <?php echo $form->field($model, 'fanpage_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(),'id','name'),['prompt'=>'Chọn Fanpage'])->label('Fanpage')?>
    </div>
    <div class="col-sm-3">
        <?php echo $form->field($model, 'transport_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(),'id','name'),['prompt'=>'Đơn vị vận chuyển'])->label('Đơn vị vận chuyển')?>
    </div>
    <div class="col-sm-3">
        <?php echo $form->field($model, 'transport_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(),'id','status'),['prompt'=>'Trạng thái đơn hàng'])->label('Trạng thái đơn hàng')?>
    </div>
    <div class="row">
        <div class="date-time">
            <div class="col-sm-2">
                <?php echo $form->field($model, 'start_time', [
                    'template' => '<div class="col-sm-6 col-sm-offset-3"><i style="vertical-align: sub;" class="fa fa-calendar fa-2x" aria-hidden="true"></i><div style="display: inline-block">{input}</div></div>',
                ])->widget(DateTimePicker::className(), [
                    'options' => [
                        'placeholder' => 'From date',
                        'class'       => 'form-control',
                        'style' =>  'width: 200px'
                    ],
                ]) ?>
            </div>
            <div class="col-sm-2">
                <?php echo $form->field($model, 'end_time', ['template' => '<div class="col-sm-6 col-sm-offset-3"><i style="vertical-align: sub;" class="fa fa-calendar fa-2x" aria-hidden="true"></i><div style="display: inline-block">{input}</div></div>'])->widget(DateTimePicker::className(), [
                    'options' => [
                        'placeholder' => 'To date',
                        'class'       => 'form-control',
                        'style' =>  'width: 200px',
                    ],
                ]) ?>
            </div>
        </div>
    </div>




    <div class="form-group" style="margin-left: 12px">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
