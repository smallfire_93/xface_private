<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportShipping */

$this->title = 'Create Report Shipping';
$this->params['breadcrumbs'][] = ['label' => 'Report Shippings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-shipping-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
