<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use yii\widgets\Breadcrumbs;

$this->title = "Đơn vị vận chuyển";
$this->params['breadcrumbs'][] = "Đơn vị vận chuyển";
?>

<div class="page-content">
    <div class="page-header">
        <h1>
            Đơn vị vận chuyển
            <small>
                <i class="icon-double-angle-right"></i>
                Danh sách đơn vị vận chuyển
            </small>
        </h1>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="widget-box">

                <div class="widget-body">
                    <div class="widget-main">
                        <li>
                            <a href="assets/images/gallery/image-6.jpg" data-rel="colorbox" class="cboxElement">
                                <img alt="150x150" src="assets/images/gallery/thumb-6.jpg">
                            </a>
                        </li>
                            <h3>
                                <a href="">Giao hàng nhanh</a>
                            </h3>
                            <p>
                                Đơn hàng sẽ tự động đẩy sang hệ thống giao nhận của Giaohangnhanh
                            </p>
                        <button class="btn btn-pink">Kết nối nhà vận chuyển</button>
                    </div>
                </div>
            </div>
        </div><!-- /span -->

        <div class="col-xs-12 col-sm-12">
            <div class="widget-box">

                <div class="widget-body">
                    <div class="widget-main">
                        <li>
                            <a href="assets/images/gallery/image-6.jpg" data-rel="colorbox" class="cboxElement">
                                <img alt="150x150" src="assets/images/gallery/thumb-6.jpg">
                            </a>
                        </li>
                        <h3>
                            <a href="">Viettel Post</a>
                        </h3>
                        <p>
                            Đơn hàng sẽ tự động đẩy sang hệ thống giao nhận của Viettel Post
                        </p>
                        <button class="btn btn-pink">Kết nối nhà vận chuyển</button>
                    </div>
                </div>
            </div>
        </div><!-- /span -->

        <div class="col-xs-12 col-sm-4">


        </div>
    </div><!-- /span -->


    <div class="row">
    <div class="col-xs-12">
    <div class="table-responsive">
    <div id="sample-table-2_wrapper" class="dataTables_wrapper" role="grid">

    <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
        <thead>
        <div class="row">
            <button class="btn btn-primary"><a href="<?= Yii::$app->urlManager->createUrl(['transport/form'])?>" style="color: black">Thêm mới đơn vị vận chuyển</a></button>
        </div>

        <tr role="row">
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2"
                rowspan="1" colspan="1" aria-label="Domain: activate to sort column ascending"
                style="width: 170px;">ID
            </th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2"
                rowspan="1" colspan="1" aria-label="Price: activate to sort column ascending"
                style="width: 114px;">Họ và Tên
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Clicks: activate to sort column ascending" style="width: 123px;">Số điện thoại
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Trạng thái
            </th>
            <th class="hidden-480 sorting" role="columnheader" tabindex="0"
                aria-controls="sample-table-2" rowspan="1" colspan="1"
                aria-label="Status: activate to sort column ascending" style="width: 163px;">Chức năng
            </th>

        </tr>
        </thead>


        <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr class="odd">


            <td class=" ">
                <a href="#">1</a>
            </td>
            <td class=" ">cuongluong</td>
            <td class="hidden-480 ">098963443</td>

            <td class="hidden-480 ">
                <span class="label label-sm label-success">Active</span>
            </td>


            <td class=" ">
                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                    <!--            <a class="blue" href="#">-->
                    <!--                <i class="icon-zoom-in bigger-130"></i>-->
                    <!--            </a>-->

                    <a class="green" href="#">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="#">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="visible-xs visible-sm hidden-md hidden-lg">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle"
                                data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-info" data-rel="tooltip" title=""
                                   data-original-title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title=""
                                   data-original-title="Edit">
																				<span class="green">
																					<i class="icon-edit bigger-120"></i>
																				</span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title=""
                                   data-original-title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="even">

            <td class=" ">
                <a href="#">2</a>
            </td>
            <td class=" ">cuong</td>
            <td class="hidden-480 ">74633452</td>

            <td class="hidden-480 ">
                <span class="label label-sm label-inverse arrowed-in">Block</span>
            </td>

            <td class=" ">
                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                    <!--            <a class="blue" href="#">-->
                    <!--                <i class="icon-zoom-in bigger-130"></i>-->
                    <!--            </a>-->

                    <a class="green" href="#">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="#">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="visible-xs visible-sm hidden-md hidden-lg">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle"
                                data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-info" data-rel="tooltip" title=""
                                   data-original-title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title=""
                                   data-original-title="Edit">
																				<span class="green">
																					<i class="icon-edit bigger-120"></i>
																				</span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title=""
                                   data-original-title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        <!---->
        </tbody>
    </table>

    </div>
    </div>
    </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="dataTables_info" id="sample-table-2_info">Showing 1 to 10 of 23 entries</div>
    </div>
    <div class="col-sm-6">
        <div class="dataTables_paginate paging_bootstrap">
            <ul class="pagination">
                <li class="prev disabled"><a href="#"><i class="icon-double-angle-left"></i></a>
                </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li class="next"><a href="#"><i class="icon-double-angle-right"></i></a></li>
            </ul>
        </div>
    </div>
</div>