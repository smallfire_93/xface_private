<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-content">
    <div class="page-header">
        <h1>
            Thêm mới/ cập nhật đơn vị vận chuyển
            <small>
                <i class="icon-double-angle-right"></i>

            </small>
        </h1>
    </div><!-- /.page-header -->

    <div class="row" style="margin-top: 20px">
        <div class="col-xs-12">
            <?php $form = ActiveForm::begin(
                ['layout' => 'horizontal']
            ); ?>
            <div class="row">
                <?= $form->field($model, 'name')->textInput(['placeholder'=>"Họ tên"])->label('Họ tên') ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="row">
                <?php $arrStatus = ['' => '---Trạng thái---', '1' => 'Active', '0' => 'Block']; ?>
                <?= $form->field($model, 'status')->dropDownList($arrStatus)->label('Trạng thái') ?>
            </div>
<!--            <div class="row">-->
<!--                --><?php //echo $form->field($model, 'company_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Company::find()->all(),'id','name'),['prompt'=>'Công ty'])->label('Chọn công ty')?>
<!--            </div>-->

         <div class="col-sm-offset-3">
             <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

         </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
