<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 *
 */
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Thêm mới/cập nhật";
$this->params['breadcrumbs'][] = "Đơn vị vận chuyển";
?>

<div class="page-content">
    <div class="page-header">
        <h1>
            Thêm mới/ cập nhật đơn vị vận chuyển
            <small>
                <i class="icon-double-angle-right"></i>

            </small>
        </h1>
    </div><!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Họ Tên </label>

                    <div class="col-sm-9">
                        <input type="text" id="form-field-1" placeholder="Họ tên" class="col-xs-10 col-sm-5" />
                    </div>
                </div>
                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Số điện thoại </label>

                    <div class="col-sm-9">
                        <input type="password" id="form-field-2" placeholder="1214135235" class="col-xs-10 col-sm-5" />

                    </div>
                </div>
                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Trạng thái</label>
                    <div class="col-sm-3">
                        <select id="user-status" class="form-control" name="User[status]" aria-invalid="false">
                            <option value="">---Trạng thái---</option>
                            <option value="1" selected="">Active</option>
                            <option value="0">Block</option>
                        </select>
                        <div class="help-block help-block-error "></div>
                    </div>
                </div>
            </form>

        </div>
        <div class="clearfix">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="button">
                    <i class="icon-ok bigger-110"></i>
                    <a href="<?= yii::$app->urlManager->createUrl(['transport/view1'])?>" style="color: #000000">Thêm mới</a>
                </button>
            </div>
        </div>
    </div>
</div>