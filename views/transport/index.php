<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content">
     <div class="page-header">
         <h1>
             Vận chuyển
             <small>
                 <i class="icon-double-angle-right"></i>
                 Danh sách đơn vị vận chuyển
             </small>
         </h1>
     </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel-body">
                <div class="panel panel-info">
                    <div class="panel panel-body">
                        <div class="col-sm-3">
                            <li>
                                <a href=">" data-rel="colorbox" class="cboxElement">
                                    <img alt="150x150" src="/xface_private/uploads/logo_giaohangnhanh.png">
                                </a>
                            </li>
                        </div>
                        <div class="col-sm-6">
                            <p >
                                Cửa hàng của bạn chưa sử dụng dịch vụ vận chuyển VNPost. Thông tin của bạn sẽ được chúng tôi mã hóa trước khi lưu trữ. Để tìm hiểu thêm về VNPost và các thông tin khác vui lòng
                            </p>
                        </div>

                        <div class="col-sm-3">
                            <button class="btn btn-pink">Kết nối nhà vận chuyển</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="panel panel-info">
                    <div class="panel panel-body">
                        <div class="col-sm-3">
                            <li>
                                <a href="assets/images/gallery/image-6.jpg" data-rel="colorbox" class="cboxElement">
                                    <img alt="150x150" src="assets/images/gallery/thumb-6.jpg">
                                </a>
                            </li>
                        </div>
                        <div class="col-sm-6">
                            <p >
                                Cửa hàng của bạn chưa sử dụng dịch vụ vận chuyển Saigon Post. Thông tin của bạn sẽ được chúng tôi mã hóa trước khi lưu trữ. Để tìm hiểu thêm về Saigon Post và các thông tin khác vui lòng                            </p>
                        </div>

                        <div class="col-sm-3">
                            <button class="btn btn-pink">Kết nối nhà vận chuyển</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p>
        <?= Html::a('Thêm mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <div id="sample-table-2_wrapper" class="dataTables_wrapper" role="grid">
                    <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                        <thead>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'header' => 'Tên người vận chuyển',
                                    'value' => 'name',
                                    'attribute' => 'name',
                                ],
                                [
                                    'header' => 'Số điện thoại',
                                    'value' => 'phone',
                                    'attribute' => 'phone',
                                ],
                                [
                                    'header' => 'Trạng thái',
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        if ($model->status == 1) {
                                            return '<span class="label label-sm label-success">Active</span>';
                                        } elseif ($model->status == 0) {
                                            return '<span class="label label-sm label-inverse arrowed-in">Block</span>';
                                        }
                                    },
                                ],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
