<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 17-Mar-17
 * Time: 3:44 PM
 */
use yii\helpers\Html;

$this->title = 'Cập nhật mật khẩu';
?>
<form class="form-horizontal">
	<div class="form-group">
		<?= Html::label('Mật khẩu mới', null, ['class' => 'control-label col-sm-3']) ?>
		<div class="col-sm-5">
			<?= Html::passwordInput('password', 'tested', ['class' => 'form-control']) ?>
		</div>
	</div>
	<div class="form-group">

		<?= Html::label('Nhập lại mật khẩu mới', null, ['class' => 'control-label col-sm-3']) ?>
		<div class="col-sm-5">
			<?= Html::passwordInput('password-repeat', 'tested', ['class' => 'form-control']) ?>
		</div>
	</div>
	<div class="form-group">

		<div class="col-sm-9 col-sm-offset-3 padding-top">
			<?php echo Html::submitButton('Reset mật khẩu', ['class' => 'btn btn-success']) ?>
		</div>
	</div>

</form>