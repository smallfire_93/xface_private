<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 17-Mar-17
 * Time: 3:44 PM
 */
use yii\helpers\Html;

$this->title = 'Lấy lại mật khẩu';
?>
<p style="font-weight: bold; color: red; text-align:  center"> * Vui lòng điền email khi đăng ký</p>
<form class="form-horizontal">
	<div class="form-group">
		<?= Html::label('Email', null, ['class' => 'control-label col-sm-3']) ?>
		<div class="col-sm-6">
			<?= Html::textInput('email', 'admin@gmail.com', ['class' => 'form-control col-sm-6']) ?>
		</div>
		<div class="col-sm-9 col-sm-offset-3 padding-top">
			<?php echo Html::submitButton('Lấy lại mật khẩu', ['class' => 'btn btn-danger']) ?>
		</div>
	</div>

</form>