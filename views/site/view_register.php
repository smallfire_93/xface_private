<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Hệ thống đăng ký';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .login-layout label {
        margin-bottom: -5px;
    }
</style>
<script>
    /*function checkPass(){
        var pass1 = document.getElementById("pass").value;
        var pass2 = document.getElementById("rePass").value;
        if(pass1 != pass2) {
            alert("Mật khẩu không trùng khớp");
            return false;
        }
    }*/
</script>
                <div class="widget-main" style="background: #f7f7f7; padding: 16px 36px 36px">
                <h4 class="header green lighter bigger">
                    <i class="icon-group blue"></i>
                    New User Registration
                </h4>
                <div class="space-6"></div>
<?php $form = ActiveForm::begin(["action" =>["site/register"],"method" => "post",]);?>
    <fieldset>
        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?= $form->field($model_reg, 'email')->textInput(['placeholder'=>'Email','class'=>'form-control',])->label(false) ?>
                                <i class="icon-envelope"></i>
                            </span>
        </label>

        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?= $form->field($model_reg, 'username')->textInput(['placeholder'=>'Username','class'=>'form-control'])->label(false) ?>
                                <i class="icon-user"></i>
                            </span>
        </label>

        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?= $form->field($model_reg, 'password')->passwordInput(['placeholder'=>'Password','id'=>'pass','class'=>'form-control'])->label(false) ?>
                                <i class="icon-lock"></i>
                            </span>
        </label>

        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?= $form->field($model_reg, 'password_repeat')->passwordInput(['placeholder'=>'Repeat password','id'=>'rePass','class'=>'form-control'])->label(false) ?>
                                <i class="icon-retweet"></i>
                            </span>
        </label>

        <label class="block">
            <!--<input type="checkbox" class="ace" />
            <span class="lbl">
                <a href="#">User Agreement</a>
            </span>-->
            <?= $form->field($model_reg,'invalid')->checkbox([
                'uncheck'      => null,
                'class' =>'lbl',
                'label' =>'I accept the User Agreement',
                'value'        => 0,
                'checked'      => true,
                ])->label('');
            ?>
        </label>

        <div class="space-24"></div>

        <div class="clearfix">
            <button type="reset" class="width-30 pull-left btn btn-sm">
                <i class="icon-refresh"></i>
                Reset
            </button>
            <?= Html::submitButton(' Register', ['class' => 'icon-arrow-right icon-on-right width-65 pull-right btn btn-sm btn-success','onclick'=>'checkPass()']) ?>
        </div>
    </fieldset>
<?php ActiveForm::end() ?>
            </div>
<div class="toolbar center" style=" background: #76b774; height: 50px; line-height: 50px">
    <a href="index.php?r=site/login" style="color:#FE9;">
        <i class="icon-arrow-left"></i>
        Back to login
    </a>
</div>

