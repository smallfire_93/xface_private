<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Hệ thống đăng nhập';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="position-relative">
    <div id="login-box" class="login-box visible widget-box no-border">
        <div class="widget-body">
            <div class="widget-main">
                <h4 class="header blue lighter bigger">
                    <i class="icon-coffee green"></i>
                    Please Enter Your Information
                </h4>
                <div class="space-6"></div>
                <!-- Form -->

                <!-- Form -->
                <?php $form = ActiveForm::begin() ?>
                <fieldset>
                    <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username: admin', 'class' => 'form-control'])->label(false) ?>
                                <i class="icon-user"></i>
                            </span>
                    </label>

                    <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password: 123456'])->label(false) ?>

                                <i class="icon-lock"></i>
                            </span>
                    </label>

                    <div class="space"></div>

                    <div class="clearfix">
                        <label class="inline">
                            <?= $form->field($model, 'rememberMe')->checkbox(['label' => 'Remember Me', 'checked' => true]) ?>
                        </label>
                        <?= Html::submitButton(' Login', ['class' => 'width-35 pull-right btn btn-sm btn-primary icon-key']) ?>
                    </div>
                    <div class="space-4"></div>
                </fieldset>
                <?php ActiveForm::end() ?>

                <div class="social-or-login center">
                    <span class="bigger-110">Or Login Using</span>
                </div>

                <div class="social-login center">
                    <a data-popup-width="860" data-popup-height="480" class="btn btn-primary facebook auth-link" href="<?= Yii::$app->urlManager->createUrl(['site/auth', 'authclient'=>'facebook'])?>">
                        <i class="icon-facebook"></i>
                    </a>

                    <a class="btn btn-info">
                        <i class="icon-twitter"></i>
                    </a>

                    <a class="btn btn-danger">
                        <i class="icon-google-plus"></i>
                    </a>
                </div>
            </div><!-- /widget-main -->

            <div class="toolbar clearfix">
                <div>
                    <a href="#" onclick="show_box('forgot-box'); return false;" class="forgot-password-link">
                        <i class="icon-arrow-left"></i>
                        I forgot my password
                    </a>
                </div>

                <div>
                    <a href="index.php?r=site/register" class="user-signup-link">
                        I want to register
                        <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div><!-- /widget-body -->
    </div><!-- /login-box -->

    <div id="forgot-box" class="forgot-box widget-box no-border">
        <div class="widget-body">
            <div class="widget-main">
                <h4 class="header red lighter bigger">
                    <i class="icon-key"></i>
                    Retrieve Password
                </h4>

                <div class="space-6"></div>
                <p>
                    Enter your email and to receive instructions
                </p>

                <form>
                    <fieldset>
                        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <input type="email" class="form-control"
                                       placeholder="Email"/>
                                <i class="icon-envelope"></i>
                            </span>
                        </label>

                        <div class="clearfix">
                            <button type="button" class="width-35 pull-right btn btn-sm btn-danger">
                                <i class="icon-lightbulb"></i>
                                Send Me!
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div><!-- /widget-main -->

            <div class="toolbar center">
                <a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
                    Back to login
                    <i class="icon-arrow-right"></i>
                </a>
            </div>
        </div><!-- /widget-body -->
    </div><!-- /forgot-box -->

    <div id="signup-box" class="signup-box widget-box no-border">
        <div class="widget-body">
            <div class="widget-main">
                <h4 class="header green lighter bigger">
                    <i class="icon-group blue"></i>
                    New User Registration
                </h4>

                <!-- Code đăng ký tài khoản-->
            </div>

            <div class="toolbar center">
                <a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
                    <i class="icon-arrow-left"></i>
                    Back to login
                </a>
            </div>
        </div><!-- /widget-body -->
    </div><!-- /signup-box -->
</div><!-- /position-relative -->