<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
$this->params['breadcrumbs'][] = "Quản lý chung";
?>
<div class="page-header">
</div><!-- /.page-header -->

<div class="row">
    <div class="col-sm-7 infobox-container">
        <div class="infobox infobox-green  ">
            <div class="infobox-icon">
                <i class="icon-comments"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number">1.000</span>
                <div class="infobox-content">comment & tin nhắn</div>
            </div>
        </div>

        <div class="infobox infobox-blue  ">
            <div class="infobox-icon">
                <i class="icon-instagram"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number">11</span>
                <div class="infobox-content">người theo dõi</div>
            </div>
        </div>

        <div class="infobox infobox-pink  ">
            <div class="infobox-icon">
                <i class="icon-shopping-cart"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number">8</span>
                <div class="infobox-content">đơn hàng</div>
            </div>
        </div>

        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="lighter">
                    <i class="icon-signal"></i>
                    Doanh số
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main padding-4">
                    <div id="sales-charts" style="width: 100%; height: 220px; padding: 0px; position: relative;"><canvas class="flot-base" width="635" height="220" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 635px; height: 220px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 30px; text-align: center;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 125px; text-align: center;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 220px; text-align: center;">2.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 315px; text-align: center;">3.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 410px; text-align: center;">4.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 505px; text-align: center;">5.0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 79px; top: 203px; left: 600px; text-align: center;">6.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 190px; left: 1px; text-align: right;">-2.000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 166px; left: 1px; text-align: right;">-1.500</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 143px; left: 1px; text-align: right;">-1.000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 119px; left: 1px; text-align: right;">-0.500</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 95px; left: 4px; text-align: right;">0.000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 71px; left: 4px; text-align: right;">0.500</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 48px; left: 4px; text-align: right;">1.000</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 24px; left: 4px; text-align: right;">1.500</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 0px; left: 4px; text-align: right;">2.000</div></div></div><canvas class="flot-overlay" width="635" height="220" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 635px; height: 220px;"></canvas><div class="legend"><div style="position: absolute; width: 64px; height: 66px; top: 13px; right: 13px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:13px;right:13px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(237,194,64);overflow:hidden"></div></div></td><td class="legendLabel">Domains</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(175,216,248);overflow:hidden"></div></div></td><td class="legendLabel">Hosting</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(203,75,75);overflow:hidden"></div></div></td><td class="legendLabel">Services</td></tr></tbody></table></div></div>
                </div><!-- /widget-main -->
            </div><!-- /widget-body -->
        </div>
    </div>
    <div class="col-sm-4">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="lighter">
                    <i class="icon-star orange"></i>
                    Đơn hàng mới
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <table class="table table-bordered table-striped">
                        <thead class="thin-border-bottom">
                        <tr>
                            <th>
                                <i class="icon-caret-right blue"></i>
                                Người đặt hàng
                            </th>

                            <th>
                                <i class="icon-caret-right blue"></i>
                                Giá
                            </th>

                            <th class="hidden-480">
                                <i class="icon-caret-right blue"></i>
                                Trạng thái
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>Nguyễn Văn A</td>

                            <td>
                                <b class="green">299.000</b>
                            </td>

                            <td class="hidden-480">
                                <span class="label label-info arrowed-right arrowed-in">đang giao hàng</span>
                            </td>
                        </tr>

                        <tr>
                            <td>Phạm Văn B</td>
                            <td>
                                <b class="green">180.000</b>
                            </td>

                            <td class="hidden-480">
                                <span class="label label-success arrowed-in arrowed-in-right">đã đặt hàng</span>
                            </td>
                        </tr>

                        <tr>
                            <td>Phạm Đình C</td>
                            <td>
                                <b class="green">399.000</b>
                            </td>

                            <td class="hidden-480">
                                <span class="label label-danger arrowed">đã xác nhận</span>
                            </td>
                        </tr>

                        <tr>
                            <td>Trịnh Văn D</td>
                            <td>
                                <b class="green">399.000</b>
                            </td>

                            <td class="hidden-480">
                                <span class="label arrowed">
                                    <s>hồi hoàn</s>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>Nguyễn Văn E</td>
                            <td>
                                <b class="green">150.000</b>
                            </td>

                            <td class="hidden-480">
                                <span class="label label-warning arrowed arrowed-right">hoàn thành</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- /widget-main -->
            </div><!-- /widget-body -->
        </div><!-- /widget-box -->
    </div>
</div>

<script type="text/javascript">
    window.jQuery || document.write("<script src='web/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='web/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>

