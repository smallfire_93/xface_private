<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PageConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cấu hình';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-config-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Page Config', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fanpage_id',
            'fb_post_id',
            'auto_reply_comment',
            'auto_like_comment',
            // 'auto_hidden_comment_all',
            // 'auto_hidden_comment_phone',
            // 'auto_hidden_comment_keyword',
            // 'auto_reply_inbox',
            // 'comment_keyword',
            // 'reply_inbox_content',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
