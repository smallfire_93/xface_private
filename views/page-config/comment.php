<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use phamxuanloc\jui\DateTimePicker;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PageConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cấu hình';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content">
    <?php $form = ActiveForm::begin(); ?>
    <div class="page-header">
        <h1>
            Cấu hình :
            <small>
                <i class="icon-double-angle-right"></i>
                Cấu hình comment
            </small>
        </h1>
    </div>
    <div class="col-xs-12">

        <div class="row">

            <h3 class="header smaller green">Trả lời comment tự động</h3>
            <div class="row">
                <label class="pull-left inline" style="margin-left: 20px">

                    <?php
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'auto_reply_comment',
                        'pluginOptions' => [
                            'threeState' => true,
                            'size' => 'lg'
                        ]
                    ]);
                    ?>

                <span class="lbl"></span>
                </label>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <form>
                <div class="form-group">

                        <?= $form->field($model, 'comment_keyword')->textarea(['maxlength' => true])->label('Nội dung trả lời tự động') ?>

                </div>
            </form>
        </div>

        <div class="row">
            <h3 class="header smaller green">Ẩn comment tự động</h3>
            <div class="col-sm-4">
                <label class="pull-left inline">


                    <?php
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'auto_hidden_comment_all',
                        'pluginOptions' => [
                            'threeState' => true,
                            'size' => 'lg'
                        ],
                    ]);
                    ?>
                    <span>Ẩn tất cả comment</span>

                    <span class="lbl"></span>
                </label>
            </div>
            <div class="col-sm-4">
                <label class="pull-left inline">
                    <?php
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'auto_hidden_comment_phone',
                        'pluginOptions' => [
                            'threeState' => true,
                            'size' => 'lg'
                        ],
                    ]);
                    ?>
                    <span>Ẩn comment có số điện thoại</span>

                    <span class="lbl"></span>
                </label>
            </div>
            <div class="col-sm-4">
                <label class="pull-left inline">
                    <?php
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'auto_hidden_comment_keyword',
                        'pluginOptions' => [
                            'threeState' => true,
                            'size' => 'lg'
                        ],
                    ]);
                    ?>
                    <span>Ẩn comment theo từ khóa</span>

                    <span class="lbl"></span>
                </label>
            </div>
        </div>

<!--        <div class="row">-->
<!---->
<!--                <h3 class="header smaller green">Tự động comment theo lịch bài viết</h3>-->
<!--                <div class="col-sm-3">-->
<!---->
<!--                        --><?//= $form->field($model, 'start_time')->widget(DateTimePicker::className(), [
//                            'dateFormat' => 'yyyy-MM-dd',
//                            'clientOptions' => [
//                            ],
//                            'options' => ['style' => 'width:200px',],
//                        ]) ?>
<!--                </div>-->
<!--                <div class="col-sm-3">-->
<!--                    --><?//= $form->field($model, 'end_time')->widget(DateTimePicker::className(), [
//                        'dateFormat' => 'yyyy-MM-dd',
//                        'clientOptions' => [
//                        ],
//                        'options' => ['style' => 'width:200px',],
//                    ]) ?>
<!--                </div>-->
<!---->
<!--        </div>-->
        <div class="row">
            <h3 class="header smaller green">Trả lời comment tự động</h3>
            <form>
                <div class="form-group">

                </div>
                <div class="form-group">
                </div>
                <div class="form-group">
                </div>
            </form>
        </div>
    </div>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>