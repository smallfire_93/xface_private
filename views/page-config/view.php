<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PageConfig */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Page Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-config-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fanpage_id',
            'fb_post_id',
            'auto_reply_comment',
            'auto_like_comment',
            'auto_hidden_comment_all',
            'auto_hidden_comment_phone',
            'auto_hidden_comment_keyword',
            'auto_reply_inbox',
            'comment_keyword',
            'reply_inbox_content',
        ],
    ]) ?>

</div>
