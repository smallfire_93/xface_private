<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PageConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cấu hình';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content">
    <div class="page-header">
        <h1>
            Cấu hình :
            <small>
                <i class="icon-double-angle-right"></i>
                Cấu hình inbox
            </small>
        </h1>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <h3 class="header smaller green">Tự động trả lời inbox</h3>
            <div class="row">
                <label class="pull-left inline" style="margin-left: 20px">
                    <?php $form = ActiveForm::begin(); ?>
                    <?php
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'auto_reply_inbox',
                        'pluginOptions' => [
                            'threeState' => true,
                            'size' => 'lg'
                        ]
                    ]);
                    ?>
                    <?php ActiveForm::end(); ?>
                    <span class="lbl"></span>
                </label>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <form>
                <div class="form-group">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'reply_inbox_content')->textarea(['maxlength' => true])->label('Nội dung trả lời tự động') ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </form>
        </div>

        <div class="row">
            <h3 class="header smaller green">Kịch bản trả lời tự động</h3>
            <form>
                <div class="form-group">
                    <?= $form->field($model, 'fanpage_id')->textarea(\yii\helpers\ArrayHelper::map(\app\models\PageConfigReply::find()->where('type = 2')->all(), 'id','keyword'),['prompt'=>'Nội dung inbox'])->label('Nội dung inbox') ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'fanpage_id')->textarea(\yii\helpers\ArrayHelper::map(\app\models\PageConfigReply::find()->where('type = 2')->all(), 'id','reply_content'),['prompt'=>'Nội dung trả lời inbox'])->label('Nội dung trả lời inbox') ?>
                </div>
            </form>
        </div>
    </div>
    <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>