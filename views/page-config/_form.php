<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PageConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-config-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fanpage_id')->textInput() ?>

    <?= $form->field($model, 'fb_post_id')->textInput() ?>

    <?= $form->field($model, 'auto_reply_comment')->textInput() ?>

    <?= $form->field($model, 'auto_like_comment')->textInput() ?>

    <?= $form->field($model, 'auto_hidden_comment_all')->textInput() ?>

    <?= $form->field($model, 'auto_hidden_comment_phone')->textInput() ?>

    <?= $form->field($model, 'auto_hidden_comment_keyword')->textInput() ?>

    <?= $form->field($model, 'auto_reply_inbox')->textInput() ?>

    <?= $form->field($model, 'comment_keyword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reply_inbox_content')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
