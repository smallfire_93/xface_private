<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PageConfigSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-config-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fanpage_id') ?>

    <?= $form->field($model, 'fb_post_id') ?>

    <?= $form->field($model, 'auto_reply_comment') ?>

    <?= $form->field($model, 'auto_like_comment') ?>

    <?php // echo $form->field($model, 'auto_hidden_comment_all') ?>

    <?php // echo $form->field($model, 'auto_hidden_comment_phone') ?>

    <?php // echo $form->field($model, 'auto_hidden_comment_keyword') ?>

    <?php // echo $form->field($model, 'auto_reply_inbox') ?>

    <?php // echo $form->field($model, 'comment_keyword') ?>

    <?php // echo $form->field($model, 'reply_inbox_content') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
