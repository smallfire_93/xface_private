<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Sales';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$hostname    =   [];
$total  =   [];

foreach($getSaleDayByDAy as $item){
    array_push($hostname    ,   $item->created_date);
    array_push($total    ,   $item->total_order);
}

?>

<div class="report-sale-index">

    <?php
    $gridColumns    =   [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'header' => 'Nhân viên',
            'value' => 'user_id.username',
            'attribute' => 'user_id',
        ],
        [
            'header' => 'Đã chốt',
            'value' => 'total_order_confirm',
            'attribute' => 'total_order_confirm',
        ],
        [
            'header' => 'Tổng chưa chốt',

        ],
        [
            'header' => 'Tổng đơn thành công',
            'value' => 'total_order_finish',
            'attribute' => 'total_order_finish',
        ],
        [
            'header' => 'Tổng đơn',
            'value' => 'total_order',
            'attribute' => 'total_order',
        ],

    ]
    ?>
    <div class="page-content">
        <div class="page-header">
            <h1>
                Báo cáo chôt Sale
                <small>
                    <i class="icon-double-angle-right"></i>
                </small>
            </h1>
        </div><!-- /.page-header -->
        <div class="row" style="margin-top: 20px">
            <div class="">

            </div>
            <div class="col-xs-12">
                <div class="well">
                    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <?php
        echo Highcharts::widget([
            'options' => [
                'title'  => ['text' => 'Báo cáo comment'],
                'xAxis'  => [
                    'categories' => $hostname,
                ],
                'yAxis'  => [
                    'title' => ['text' => 'Số Đơn'],
                ],
                'series' => [
                    [
                        'name' => 'Số đơn',
                        'data' => $total,
                    ],
                ],
            ],
        ]);
        ?>
        <div class="row border-top">
            <div class="col-sm-12">
                <div class="row">
                    <div class="space-6"></div>

                    <div class="col-sm-12 infobox-container">
                        <div class="col-sm-3">

                            <div class="infobox infobox-green ">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order ;?></span>
                                    <div class="infobox-content">Tổng số Đơn hàng</div>
                                </div>
                                <!--					<div class="stat stat-success">8%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-blue">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order_confirm ;?></span>
                                    <div class="infobox-content">Đã chốt</div>
                                </div>

                                <!--					<div class="badge badge-success">-->
                                <!--						+32%-->
                                <!--						<i class="icon-arrow-up"></i>-->
                                <!--					</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">

                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">432432</span>
                                    <div class="infobox-content">Chưa chốt</div>
                                </div>
                                <!--					<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="icon-shopping-cart"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $model->total_order_finish ;?></span>
                                    <div class="infobox-content">Thành công</div>
                                </div>
                                <!--						<div class="stat stat-important">4%</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <?php echo ExportMenu::widget([
            'dataProvider'    => $dataProvider,
            'columns'         => $gridColumns,
            'fontAwesome'     => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-warning',
            ],
            'exportConfig'    => [
                ExportMenu::FORMAT_TEXT  => false,
                ExportMenu::FORMAT_PDF   => false,
                ExportMenu::FORMAT_HTML  => false,
                ExportMenu::FORMAT_EXCEL => false,
            ],
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => $gridColumns,
        ]); ?>

    </div>
</div>
