<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportSale */

$this->title = 'Create Report Sale';
$this->params['breadcrumbs'][] = ['label' => 'Report Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-sale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
