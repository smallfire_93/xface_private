<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\bootstrap\Html;

$this->title = "Tương tác";
$this->params['breadcrumbs'][] = "Quản lí tương tác";
?>
<form class="form-search">
    <div class="input-group chat-search">
        <input class="form-control" type="text" placeholder="Search ..."
               class="nav-search-input"
               id="nav-search-input" autocomplete="off">
        <i class="icon-search nav-search-icon"></i>
    </div>
</form>
<div class="inbox-list fb-chat-list">
    <ul class="chats-list chat-list-in-box">
        <?php foreach ($getComment['data'] as $pages) {
            if (isset($pages['comments'])) {
                foreach ($pages['comments'] as $name) {
                    foreach ($name as $nameComment) {
                        if (isset($nameComment['from'])) { ?>
                            <li class="item inbox-detail-conversations"
                                onclick="show_detail_conversations('<?= $nameComment['id'] ?>', '0', '<?= $pages['id'] ?>')">
                                <div class="chat-img">
                                    <img class="direct-chat-img"
                                         src="http://graph.facebook.com/<?= $nameComment['from']['id'] ?>/picture"
                                         alt="<?= $nameComment['from']['name'] ?>">
                                </div>
                                <div class="chat-info">
                                    <p class="chat-title">
                                        <span class="direct-chat-name"><?= $nameComment['from']['name'] ?></span>
                                    </p>
                                    <span class="chat-description"><?= $nameComment['message'] ?></span>
                                </div>
                                <div class="conversation-info">
                                    <span class="direct-chat-timestamp mes-ac-1"><?= date('d-m-Y H:i:s', strtotime($nameComment['created_time'])); ?></span>
                                    <span class="mes-ac-2"
                                          data-original-title="">sale1</span>
                                </div>
                            </li>
                        <?php }
                    }
                }
            }
        } ?>
    </ul>
</div>