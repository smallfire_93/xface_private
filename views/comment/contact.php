<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Product;
use yii\helpers\Html;

$this->title = "Tương tác";
$this->params['breadcrumbs'][] = "Quản lí tương tác";
?>
<style type="text/css">
    body {
        overflow-y: hidden;
    }
</style>
<?php
Modal::begin([
    'header' => '<h4 class="modal-title">Vui lòng chọn Fanpage</h4>',
    'toggleButton' => ['class' => 'btn btn-warning btn-modal select-fanpage', 'label' => 'Đang kết nối '.$fanpage->name],
]); ?>
<table class="table show-list-fanpage" id="show-list-fanpage">
    <tbody>
    <?php foreach ($getFanpages as $fb_page) { ?>
        <tr>
            <td class="avatar-image-td">
                <a class="avatar-image-store" href="#" data-original-title="" title="">
                    <img class="img-circle"
                         src="//graph.facebook.com/<?= $fb_page->fb_page_id ?>/picture?width=40&amp;height=40">
                </a>
            </td>
            <td>
                <a class="avatar-text-store" href="https://www.facebook.com/<?= $fb_page->fb_page_id ?>"
                   target="_blank" data-original-title="" title=""><?= $fb_page->name ?></a>
            </td>
            <td>
                <button aria-disabled="false" class="btn btn-info facebook-page-add pull-right" role="button"
                        onclick="return connect_fanpage('<?= $fb_page->fb_page_id ?>');" data-dismiss="modal"
                        type="button">Chọn
                </button>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php Modal::end(); ?>
<div id="fanpage" class="page-content">
    <div class="tabbable">
        <div class="col-sm-3 border-right" style="padding: 0">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#total">
                        Tất cả
                    </a>
                </li>

                <li class="dropdown">
                    <a data-toggle="tab" href="#comment">
                        comment
                    </a>
                </li>

                <li class="dropdown">
                    <a data-toggle="tab" href="#inbox">
                        inbox
                    </a>
                </li>
                <li class="dropdown">
                    <a data-toggle="tab" href="#read">
                        chưa đọc(5)
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="total" class="tab-pane active">
                    <form class="form-search">
                        <div class="input-group chat-search">
                            <input class="form-control" type="text" placeholder="Search ..."
                                   class="nav-search-input"
                                   id="nav-search-input" autocomplete="off">
                            <i class="icon-search nav-search-icon"></i>
                        </div>
                    </form>
                    <div class="inbox-list fb-chat-list">
                        <ul class="chats-list chat-list-in-box">
                            <?php foreach ($result as $key => $item) { ?>
                                <li class="item inbox-detail-conversations"
                                    onclick="show_detail_conversations('<?= $item['fb_comment_id'] ?>', '0', '<?= $pageId ?>')">
                                    <div class="row" style="margin-left:0;">
                                        <div class="chat-img">
                                            <img class="direct-chat-img"
                                                 src="http://graph.facebook.com/<?= $item['fb_user_id'] ?>/picture"
                                                 alt="">
                                        </div>
                                        <div class="chat-info">
                                            <p class="chat-title">
                                                <span class="direct-chat-name">asasssasa</span>
                                            </p>
                                            <span class="chat-description"><?= $item['content'] ?></span>
                                        </div>
                                        <div class="conversation-info">
                                                        <span class="mes-ac-2"
                                                              data-original-title="">sale1</span>
                                            <span class="direct-chat-timestamp mes-ac-1"><?= date('d-m-Y', strtotime($item['created_date'])); ?></span>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div id="comment" class="tab-pane">
                    <form class="form-search">
                        <div class="input-group chat-search">
                            <input class="form-control" type="text" placeholder="Search ..."
                                   class="nav-search-input"
                                   id="nav-search-input" autocomplete="off">
                            <i class="icon-search nav-search-icon"></i>
                        </div>
                    </form>
                    <div class="inbox-list fb-chat-list">
                        <ul class="chats-list chat-list-in-box">
                            <?php foreach ($comments as $key => $item) { ?>
                                <li class="item inbox-detail-conversations"
                                    onclick="show_detail_conversations('<?= $item['fb_comment_id'] ?>', '0', '<?= $pageId ?>')">
                                    <div class="chat-img">
                                        <img class="direct-chat-img"
                                             src="http://graph.facebook.com/<?= $item['fb_user_id'] ?>/picture"
                                             alt="">
                                    </div>
                                    <div class="chat-info">
                                        <p class="chat-title">
                                            <span class="direct-chat-name">asasssasa</span>
                                        </p>
                                        <span class="chat-description"><?= $item['content'] ?></span>
                                    </div>
                                    <div class="conversation-info">
                                                        <span class="mes-ac-2"
                                                              data-original-title="">sale1</span>
                                        <span class="direct-chat-timestamp mes-ac-1"><?= date('d-m-Y', strtotime($item['created_date'])); ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div id="inbox" class="tab-pane">
                    <form class="form-search">
                        <div class="input-group chat-search">
                            <input class="form-control" type="text" placeholder="Search ..."
                                   class="nav-search-input"
                                   id="nav-search-input" autocomplete="off">
                            <i class="icon-search nav-search-icon"></i>
                        </div>
                    </form>
                    <div class="inbox-list fb-chat-list">
                        <ul class="chats-list chat-list-in-box">
                            <?php foreach ($inboxs as $key => $item) { ?>
                                <li class="item inbox-detail-conversations"
                                    onclick="show_detail_conversations('<?= $item['fb_comment_id'] ?>', '0', '<?= $pageId ?>')">
                                    <div class="chat-img">
                                        <img class="direct-chat-img"
                                             src="http://graph.facebook.com/<?= $item['fb_user_id'] ?>/picture"
                                             alt="">
                                    </div>
                                    <div class="chat-info">
                                        <p class="chat-title">
                                            <span class="direct-chat-name">asasssasa</span>
                                        </p>
                                        <span class="chat-description"><?= $item['content'] ?></span>
                                    </div>
                                    <div class="conversation-info">
                                                        <span class="mes-ac-2"
                                                              data-original-title="">sale1</span>
                                        <span class="direct-chat-timestamp mes-ac-1"><?= date('d-m-Y', strtotime($item['created_date'])); ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div id="read" class="tab-pane">
                    <form class="form-search">
                        <div class="input-group chat-search">
                            <input class="form-control" type="text" placeholder="Search ..."
                                   class="nav-search-input"
                                   id="nav-search-input" autocomplete="off">
                            <i class="icon-search nav-search-icon"></i>
                        </div>
                    </form>
                    <div class="inbox-list fb-chat-list">
                        <ul class="chats-list chat-list-in-box">
                            <?php foreach ($notReads as $key => $item) { ?>
                                <li class="item inbox-detail-conversations"
                                    onclick="show_detail_conversations('<?= $item['fb_comment_id'] ?>', '0', '<?= $pageId ?>')">
                                    <div class="chat-img">
                                        <img class="direct-chat-img"
                                             src="http://graph.facebook.com/<?= $item['fb_user_id'] ?>/picture"
                                             alt="">
                                    </div>
                                    <div class="chat-info">
                                        <p class="chat-title">
                                            <span class="direct-chat-name">asasssasa</span>
                                        </p>
                                        <span class="chat-description"><?= $item['content'] ?></span>
                                    </div>
                                    <div class="conversation-info">
                                                        <span class="mes-ac-2"
                                                              data-original-title="">sale1</span>
                                        <span class="direct-chat-timestamp mes-ac-1"><?= date('d-m-Y', strtotime($item['created_date'])); ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8" id="please_select_inbox">
            <div class="displayed">
                <p>Chọn đoạn hội thoại</p>
            </div>
        </div>
        <div id="max_loader" class="loader" style="display: none">
            <div class="loader1">
            </div>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div id="detail_inbox" style="display: none">

                </div>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#sidebar").addClass("menu-min");
        });
    </script>