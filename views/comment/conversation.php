<?php
use app\components\Common;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

?>

<script type="text/javascript" src="<?= Yii::$app->request->baseUrl ?>/js/inputTags.jquery.js"></script>
<script type="text/javascript" src="<?= Yii::$app->request->baseUrl ?>/js/app.js"></script>
<script type="text/javascript" src="<?= Yii::$app->request->baseUrl ?>/js/jquery.slimscroll.min.js"></script>
<?php $day = Common::getDay(date('l', strtotime($post->created_date))); ?>
<div class="col-sm-8 box box-primary direct-chat direct-chat-primary">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-md-4">
                <a href="">
                    <div class="chat-img">
                        <img class="direct-chat-img"
                             src="https://graph.facebook.com/<?= $customer['from']['id'] ?>/picture?type=large"
                             alt="<?= $customer['from']['name'] ?>">
                    </div>
                    <h3 id="fb_name" class="box-title user-name-w"><?= $customer['from']['name'] ?></h3>
                </a>
                <h6 style="color: red;margin-top: -1px;padding-left: 50px;"> Quản lý : sale 1 </h6>
            </div>
            <div class="col-md-8 conversation-tags">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                ]); ?>
                <?php
                Modal::begin([
                    'header' => '<h4 class="modal-title">Gắn thẻ</h4>',
                    'toggleButton' => ['class' => 'btn btn-sm btn-modal btn-default', 'label' => '<i class="fa fa-tags"></i> Gắn thẻ'],
                ]); ?>
                <div font-size="medium" class="_4-i2 _pig  _50f4">
                    <div class="_5hrx">
                        <em class="_4qba"
                            data-intl-translation="Nhãn giúp bạn theo dõi và tìm cuộc trò chuyện. Hãy tạo nhãn của riêng bạn và áp dụng chúng cho mọi người dựa trên nội dung họ đã nhắn cho bạn."
                            data-intl-trid="">Nhãn giúp bạn theo dõi và tìm cuộc trò chuyện. Hãy tạo nhãn của riêng
                            bạn và áp dụng chúng cho mọi người dựa trên nội dung họ đã nhắn cho bạn.</em>
                    </div>
                    <input type="text" id="tags" value=""/>
                    <div id="getTags" style="display: none"></div>
                </div>
                <?php Modal::end(); ?>
                <div id="fbTags">
                    <ul>
                        <?php foreach ($getTags as $tag) { ?>
                            <li>
                                <span class="item tag-color-1"><?= $tag->name ?></span>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">

        </div>
    </div>
    <div class="box-body">
        <?php if ($post->content) { ?>
            <div class="text">
                <?= $post->content; ?>
            </div>
            <div class="page-header"></div>
        <?php } ?>
        <div class="conversation-timestamp">
        <span class="direct-chat-timestamp "><?= $day ?>
            , <?= date('d/m/Y', strtotime($post->created_date)); ?></span>
        </div>
        <div class="direct-chat-messages">
            <div class="direct-chat-msg row">
                <img class="direct-chat-img"
                     src="https://graph.facebook.com/<?= $customer['from']['id'] ?>/picture?type=large"
                     alt="<?= $customer['from']['name'] ?>">
                <div class="direct-chat-text-w">
                    <div class="direct-chat-text left">
                        <p><?= $customer['message'] ?></p>
                        <div class="option-comment">
                            <?php if ($commentParent->is_hidden == 0 && $checkHidden == true) { ?>
                                <span onclick="hidden_comment('<?= $conversation_id ?>')" class="hidden-comment">Ẩn bình luận · </span>
                            <?php } elseif ($commentParent->is_hidden == 1 && $checkHidden == true) { ?>
                                <span onclick="show_comment('<?= $conversation_id ?>')"
                                      class="show-comment">Bỏ ẩn · </span>
                            <?php } ?>
                            <span onclick="delete_comment('<?= $conversation_id ?>')"
                                  class="delete-comment">Xóa · </span>
                            <?php
                            Modal::begin([
                                'id' => 'modal-inbox',
                                'header' => '<h4 class="modal-title">Tin nhắn mới</h4>',
                                'toggleButton' => ['class' => 'btn btn-inbox btn-modal', 'label' => 'Nhắn tin'],
                                'size' => 'modal-sm',
                            ]); ?>
                            <div class="p-xs border-bottom">
                                Tới: <span class="send-private-message-user"
                                           data-bind="text: CurrentMessageSendPrivateReply().DisplayName"><?= $customer['from']['name'] ?></span>
                            </div>
                            <div class="p-xs">
                            <textarea class="form-control textarea-auto-height border-none" rows="3"
                                      style="resize: none"
                                      placeholder="Viết tin nhắn với tư cách <?= $getFanpage->name ?>"></textarea>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-primary disable">Gửi</a>
                            </div>
                            <?php Modal::end(); ?>
                        </div>
                    </div>
                </div>
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-timestamp"><?= date('H:i:s d/m/Y', strtotime($customer['created_time'])); ?></span>
                </div>
            </div>
            <div id="loadConversation">
                <?php foreach ($getAllComments as $item) { ?>
                    <?php if ($item->fb_user_id != $pageId) { ?>
                        <div class="direct-chat-msg row left">
                            <img class="direct-chat-img"
                                 src="https://graph.facebook.com/<?= $item->fb_user_id ?>/picture?type=large"
                                 alt="">
                            <div class="direct-chat-text-w">
                                <div class="direct-chat-text">
                                    <p data-value="<?= $item->fb_comment_id ?>"><?= $item->content ?></p>
                                    <div class="option-comment">
                                        <?php if ($commentParent->is_hidden == 0 && $checkHidden == true) { ?>
                                            <span onclick="hidden_comment('<?= $item->fb_comment_id ?>')"
                                                  class="hidden-comment">Ẩn bình luận · </span>
                                        <?php } elseif ($commentParent->is_hidden == 1 && $checkHidden == true) { ?>
                                            <span onclick="show_comment('<?= $item->fb_comment_id ?>')"
                                                  class="show-comment">Bỏ ẩn · </span>
                                        <?php } ?>
                                        <span onclick="delete_comment('<?= $item->fb_comment_id ?>')"
                                              class="delete-comment">Xóa · </span>
                                        <?php
                                        Modal::begin([
                                            'id' => 'modal-inbox',
                                            'header' => '<h4 class="modal-title">Tin nhắn mới</h4>',
                                            'toggleButton' => ['class' => 'btn btn-inbox btn-modal', 'label' => 'Nhắn tin'],
                                            'size' => 'modal-sm',
                                        ]); ?>
                                        <div class="p-xs border-bottom">
                                            Tới: <span class="send-private-message-user"
                                                       data-bind="text: CurrentMessageSendPrivateReply().DisplayName"><?= $customer['from']['name'] ?></span>
                                        </div>
                                        <div class="p-xs">
                                        <textarea class="form-control textarea-auto-height border-none" rows="3"
                                                  style="resize: none"
                                                  placeholder="Viết tin nhắn với tư cách <?= $getFanpage->name ?>"></textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="btn btn-primary disable">Gửi</a>
                                        </div>
                                        <?php Modal::end(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-timestamp"><?= date('H:i:s d/m/Y', strtotime($item->created_date)); ?></span>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="direct-chat-msg row right">
                            <img class="direct-chat-img"
                                 src="https://graph.facebook.com/<?= $item->fb_user_id ?>/picture?type=large"
                                 alt="">
                            <div class="direct-chat-text-w">
                                <div class="direct-chat-text">
                                    <p data-value="<?= $item->fb_comment_id ?>"><?= $item->content ?></p>
                                </div>
                            </div>
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-timestamp"><?= date('H:i:s d/m/Y', strtotime($item->created_date)); ?></span>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="wait_load_messages" style="display: none">
        <center><img src="<?= Yii::$app->request->baseUrl ?>/css/images/loader3.gif" height="16" width="16"> Đang gửi...
        </center>
    </div>
    <div class="box-footer" id="chatBottomBar">
        <form id="send_message" method="post" enctype="multipart/form-data">
            <div class="type-mes">
                <img class="img-chat" src="https://graph.facebook.com/<?= $pageId ?>/picture?type=large"/>
                <textarea title="Nhập nội dung..." class="type-mes-textarea"
                          placeholder="Nhập nội dung..." name="chatText"
                          id="chatText"></textarea>
                <a id="upload_photo_local">
                    <i class="fa fa-file-image-o"></i>
                </a>
            </div>
            <span class="_error" id="write_content"></span>
            <div class="type-mes-tool">
                <div class="pull-right">
                    <input type="hidden" value="<?= $conversation_id ?>"
                           id="hidden_conversation_id" name="hidden_conversation_id">
                    <input type="hidden" value="<?= $object_id ?>"
                           id="hidden_object_id_id" name="hidden_object_id_id">
                    <input type="hidden" value="<?= $post->id ?>"
                           id="hidden_post_id" name="hidden_post_id">
                    <input type="hidden" value="<?= $parent_id ?>"
                           id="hidden_parent_id" name="hidden_parent_id">
                    <input type="hidden" value="<?= $pageId ?>"
                           id="hidden_page_id" name="hidden_page_id">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-sm-4 border-left">
    <div id="card" class="box box-widget widget-user" style="display: none">
        <?php $form = ActiveForm::begin([
            'id' => 'create-order',
            'method' => 'get',
        ]); ?>
        <?php if ($promotion) { ?>
            <p>Sản phẩm khuyến mãi xxxx</p>
        <?php } ?>
        <?= $form->field($order, 'product_array')->textInput(['placeholder' => 'Chọn sản phẩm', 'onclick' => 'select_product()'])->label(false) ?>
        <div id="show_shopping" class="panel panel-default">
            <div class="list-search-data">
                <ul class="clearfix" data-bind="foreach:Data">
                    <?php foreach ($productAll as $item) { ?>
                        <li class="row" onclick="add_card(<?= $item->id ?>)">
                            <div class="col-md-2">
                                <div class="wrap-img inline_block vertical-align-m">
                                    <img class="thumb-image"
                                         src="<?= $item->image ?>"
                                         title="<?= $item->product_name ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="inline_block ml10"
                                       data-bind="text:Value"><?= $item->product_name ?></label>
                            </div>
                            <div class="col-md-4">
                                                <span data-bind="textMoneyWithSymbol: Price"><?= number_format($item->price, '0', '.', ',') ?>
                                                    ₫</span>
                            </div>
                        </li>
                        <div class="border-top"></div>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?= $form->field($order, 'customer_note')->textarea(['placeholder' => 'Ghi chú'])->label(false) ?>
        <h4>Thông tin người nhận hàng</h4>
        <br/>
        <div class="row">
            <div class="col-sm-7">
                <?= $form->field($order, 'customer_name')->textInput(['placeholder' => 'Họ tên'])->label(false) ?>
            </div>
            <div class="col-sm-5" style="padding-left:0;">
                <?= $form->field($order, 'phone')->textInput(['placeholder' => 'Số điện thoại', 'id' => 'phone'])->label(false) ?>
            </div>
        </div>
        <?= $form->field($order, 'address')->textarea(['placeholder' => 'Địa chỉ'])->label(false) ?>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($order, 'discount')->textInput(['placeholder' => 'Giảm giá'])->label(false) ?>
            </div>
            <div class="col-sm-8">
                <?= $form->field($order, 'payment_id')->dropDownList($dropPayment, ['prompt' => 'Phương thức thanh toán'])->label(false) ?>
            </div>
        </div>
        <?= $form->field($order, 'fb_user_id')->hiddenInput(['value' => $customer['from']['id']])->label(false) ?>
        <?= $form->field($order, 'post_id')->hiddenInput(['value' => $post->id])->label(false) ?>
        <div id="show_card"></div>
        <div class="summary-order">
            <div class="invoice-price-footer">
                <p>Tổng tiền : <span id="total_money"></span></p>
            </div>
            <div class="text-center">
                <button id="add-card" type="submit" class="btn btn-info"
                        data-toggle="tooltip" data-placement="bottom">Tạo đơn hàng
                </button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".direct-chat-img").click(function () {
            var parent_id = $("#hidden_parent_id").val();
            $.ajax({
                type: "POST",
                url: "index.php?r=comment/set-status",
                data: {parent_id: parent_id},

                success: function (data) {
                    alert(data);
                }
            })
        });
    });
</script>