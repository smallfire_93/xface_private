<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Cài đặt chung: cấu hình comment';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row page-header">
    <h1>
        Cài đặt chung:
        <small>
            <i class="icon-double-angle-right"></i>
            Cấu hình comment
        </small>
    </h1>
</div>

<h3 class="header smaller green">Trả lời comment tự động</h3>
<div class="row">
    <label class="pull-left inline" style="margin-top: -20px">
        <input id="id-button-borders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-5">
        <span class="lbl"></span>
    </label>
</div>

<div class="row container" style="margin-top: 20px">
<form>
    <div class="form-group">
        <label for="exampleInputEmail1">Nội dung trả lời tự động</label>
        <textarea type="email" class="form-control" id="exampleInputEmail1"></textarea>
    </div>
</form>
</div>

<h3 class="header green">Ẩn comment tự động</h3>
<div class="row">
    <div class="col-md-4">
        <div class="col-md-5">Ẩn tất cả comment</div>
        <div class="col-md-7">
            <label class="pull-left inline" style="margin-top: -20px">
                <input id="id-button-borders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-5" style="margin-left: -20px; margin-top: 10px">
                <span class="lbl"></span>
            </label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-7">Ẩn comment có số điện thoại</div>
        <div class="col-md-5">
            <label class="pull-left inline" style="margin-top: -20px">
                <input id="id-button-borders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-5" style="margin-left: -20px; margin-top: 10px">
                <span class="lbl"></span>
            </label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-7">Ẩn comment theo từ khóa</div>
        <div class="col-md-5">
            <label class="pull-left inline" style="margin-top: -20px">
                <input id="id-button-borders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-5" style="margin-left: -20px; margin-top: 10px">
                <span class="lbl"></span>
            </label>
        </div>
    </div>
</div>
<h3 class="header smaller green">Tự động comment theo lịch bài viết</h3>
<form class="form-horizontal">
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Chọn thời gian</label>
        <div class="col-sm-5">
            <input type="text" id="datepicker" class="form-control hasDatepicker">
        </div>
        <div class="col-sm-5">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
</form>
<h3 class="header smaller green">Trả lời comment tự động</h3>
<form class="form-horizontal">
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung comment</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung trả lời</label>
        <div class="col-sm-10">
            <textarea type="email" class="form-control" id="inputEmail3"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung comment</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung trả lời</label>
        <div class="col-sm-10">
            <textarea type="email" class="form-control" id="inputEmail3"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung comment</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nội dung trả lời</label>
        <div class="col-sm-10">
            <textarea type="email" class="form-control" id="inputEmail3"></textarea>
        </div>
    </div>
    <div class="row">
        <button class="col-md-offset-11 col-md-1">Lưu</button>
    </div>
</form>