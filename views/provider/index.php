<?php
use app\models\Provider;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProviderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Yii::t('app', 'Nhà cung cấp');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-index ">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="padding-top border-top">
		<p>
			<?= Html::a(Yii::t('app', 'Thêm mới nhà cung cấp'), ['create'], ['class' => 'btn btn-success']) ?>
		</p>
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'name',
				'address',
				'phone',
				'website',
				// 'email:email',
				[
					'attribute' => 'status',
					'value'     => function (Provider $data) {
						return $data::STATUS[$data->status];
					},
				],
				// 'created_date',
				// 'company_id',
				['class'    => 'yii\grid\ActionColumn',
				 'template' => '{update} {delete}',
				],
			],
		]); ?>
	</div>
</div>