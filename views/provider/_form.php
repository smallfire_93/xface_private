<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Provider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-form border-top">

	<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
	<div class="padding-top">
		<?= $form->field($model, 'name')->textInput([
			'maxlength'   => true,
			'placeholder' => 'Tên',
		]) ?>

		<?= $form->field($model, 'address')->textInput([
			'maxlength'   => true,
			'placeholder' => 'Địa chỉ',
		]) ?>
		<?= $form->field($model, 'website')->textInput([
			'maxlength'   => true,
			'placeholder' => 'website',
		]) ?>
		<?= $form->field($model, 'email')->textInput([
			'maxlength'   => true,
			'placeholder' => 'email',
		]) ?>
		<?= $form->field($model, 'phone', ['inputTemplate' => '<div class="row"><div class="col-sm-6">{input}</div></div>',])->textInput([
			'maxlength'   => true,
			'placeholder' => 'SĐT',
		]) ?>
		<?= $form->field($model, 'status', ['inputTemplate' => '<div class="row"><div class="col-sm-3">{input}</div></div>',])->dropDownList($model::STATUS, ['prompt' => 'Trạng thái']) ?>
		<div class="col-sm-offset-3">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
	</div>
	<?php ActiveForm::end(); ?>

</div>
