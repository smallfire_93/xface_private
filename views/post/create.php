<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
$this->title                   = Yii::t('app', 'Thêm mới bài viết');
$this->params['breadcrumbs'][] = [
	'label' => Yii::t('app', 'Bài viết'),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
\app\assets\MainAsset::register($this)
?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>

