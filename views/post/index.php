<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 */
use app\models\Post;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

$this->title                   = "Bài viết";
$this->params['breadcrumbs'][] = "Bài viết";
/* @var $searchModel app\models\PostSearch */
/* @var $fanpage app\models\Fanpage */
?>

<div class="page-content">
	<div class="page-header">
		<h1>
			Bài viết
			<small>
				<i class="icon-double-angle-right"></i>
				Danh sách bài viết
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="col-xs-12">

		<?= Html::a('Thêm mới bài viết', ['/post/create'], ['class' => 'btn btn-primary']) ?>
		<div class="tabbable" style="margin-top: 20px">
			<ul class="nav nav-tabs" id="myTab">
				<?php foreach($fanpages as $fanpage) { ?>

					<li class="<?= $fanpage->setActive() ?>">
						<a data-toggle="tab" href="#<?= $fanpage->fb_page_id ?>">
							<?= $fanpage->name ?>
						</a>
					</li>
				<?php } ?>
				<!--				<li class="">-->
				<!--					<a data-toggle="tab" href="#fanpage2">-->
				<!--						Fanpage-2-->
				<!--					</a>-->
				<!--				</li>-->
				<!---->
				<!--				<li class="dropdown">-->
				<!--					<a data-toggle="tab" href="#fanpage3">-->
				<!--						Fanpage-3-->
				<!--					</a>-->
				<!--				</li>-->
				<!--				<li class="dropdown">-->
				<!--					<a data-toggle="tab" href="#fanpage4">-->
				<!--						...-->
				<!--					</a>-->
				<!--				</li>-->
			</ul>

			<div class="tab-content">
				<?php foreach($fanpages as $fanpage) { ?>

					<div id="<?= $fanpage->fb_page_id ?>" class="tab-pane <?= $fanpage->setActive() ?>">
						<?php
						Pjax::begin();
						echo GridView::widget([
							'dataProvider' => $searchModel->data($params, $fanpage->id),
							'columns'      => [
								'id',
								[
									'attribute' => 'content',
									'value'     => function (Post $data) {
										return $data->subtext($data->content, 50);
									},
								],
								'total_comment',
								'total_like',
								'total_share',
								'total_order',
								[
									'attribute' => 'product',
									'value'     => function (Post $data) {
										return implode(",", ArrayHelper::map($data->productData, 'id', 'name'));
									},
								],
								[
									'class'    => 'yii\grid\ActionColumn',
									'header'   => 'Hành động',
									'template' => '{update} {delete}',
								],
							],
						]);
						Pjax::end();
						?>
					</div>
				<?php } ?>
			</div>

		</div>
	</div>

</div>
<!--<div class="row">-->
<!--	<div class="col-sm-6">-->
<!--		<div class="dataTables_info" id="sample-table-2_info">Showing 1 to 10 of 23 entries</div>-->
<!--	</div>-->
<!--	<div class="col-sm-6">-->
<!--		<div class="dataTables_paginate paging_bootstrap">-->
<!--			<ul class="pagination">-->
<!--				<li class="prev disabled"><a href="#"><i class="icon-double-angle-left"></i></a>-->
<!--				</li>-->
<!--				<li class="active"><a href="#">1</a></li>-->
<!--				<li><a href="#">2</a></li>-->
<!--				<li><a href="#">3</a></li>-->
<!--				<li class="next"><a href="#"><i class="icon-double-angle-right"></i></a></li>-->
<!--			</ul>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->