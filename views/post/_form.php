<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 3:00 PM
 *
 */
use app\models\form\PostForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use phamxuanloc\jui\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\form\PostForm */
?>

<div class="page-header">

	<h1><?= Html::encode($this->title) ?></h1>
</div><!-- /.page-header -->

<div class="row">
	<?php $form = ActiveForm::begin([
		'layout'               => 'horizontal',
		'options'              => [
			'enctype' => 'multipart/form-data',
		],
		'enableAjaxValidation' => true,
	]); ?>

	<div class="col-sm-6">
		<div class="panel panel-success">
			<div class="panel-heading font-heading">Bài viết</div>
			<div class="panel-body">
				<h3 class="header blue lighter smaller">
				</h3>
				<?php echo $form->field($model, 'fanpage')->widget(Select2::className(), ['data' => ArrayHelper::map($model->getCompanyFanpage(), 'id', 'name')]) ?>
				<?php echo $form->field($model, 'type')->dropDownList($model::TYPE, ['class' => 'post-type']) ?>
				<div class="post-images post-file" style="display: none">
					<?php echo $form->field($model, 'images[]')->widget(FileInput::className(), [
						'options'       => [
							'multiple' => true,
							'accept'   => 'image/*',
						],
						'pluginOptions' => [
							'showUpload'  => false,
							'showRemove'  => false,
							//									'initialPreview' => true,-->//			//							'initialPreviewAsData' => true,-->
							'browseClass' => 'btn btn-md btn-info',
							'browseLabel' => '',
						],
					])->label('Nhiều ảnh'); ?>
				</div>
				<div class="post-image post-file" style="display: none">
					<?php echo $form->field($model, 'image')->widget(FileInput::className(), [
						'options'       => [
							'accept' => 'image/*',
						],
						'pluginOptions' => [
							'showUpload'  => false,
							'showRemove'  => false,
							//									'initialPreview' => true,-->//			//							'initialPreviewAsData' => true,-->
							'browseClass' => 'btn btn-md btn-info',
							'browseLabel' => '',
						],
					])->label('Một ảnh'); ?>
				</div>
				<div class="post-video post-file" style="display: none">
					<?php echo $form->field($model, 'video')->widget(FileInput::className(), [
						'options'       => [
							'multiple' => true,
							//							'accept'   => 'image/*',
						],
						'pluginOptions' => [
							'showUpload'  => false,
							'showRemove'  => false,
							//									'initialPreview' => true,-->//			//							'initialPreviewAsData' => true,-->
							'browseClass' => 'btn btn-md btn-info',
							'browseLabel' => '',
						],
					])->label('Video'); ?>
				</div>
				<div class="post-url post-file" style="display: none">
					<?php echo $form->field($model, 'url')->textInput()->label('Link chia sẻ') ?>
				</div>
				<br>
				<br>
				<p>
					<?php echo $form->field($model, 'content', ['template' => '<div>{input}</div>'])->textarea([
						'placeholder' => 'Nội dung bài viết',
						'rows'        => 10,
					]) ?>
				</p>
				<div class="row">
					<div class="col-xs-6">

					</div>
				</div>
			</div><!-- ./span -->
		</div>
	</div>

	<div class="col-sm-6">
		<div class="panel panel-info">
			<div class="panel-heading font-heading">Đăng bài tự động</div>
			<div class="panel-body">
				<h3 class="header blue lighter smaller">

				</h3>
				<?php ?>
				<div class="form-group">
					<div class="items col-sm-9">
						<?php for($i = 0; $i < 1; $i ++) { ?>
							<div class="content-items">
								<div class="item-detail col-sm-9">
									<?= $form->field($model, 'schedule', ['template' => '<div class="col-sm-7 col-sm-offset-5" style="padding-left: 15px">{input}</div>'])->widget(DateTimePicker::className(), [
										'options'    => [
											'placeholder' => 'Chọn thời gian đăng',
											'class'       => 'border-form form-control',
											'name'        => 'PostForm[' . $i . '][schedule]',
											'id'          => 'postform-schedule-' . $i,
										],
										'dateFormat' => 'yyyy-MM-d',
									])->label(false) ?>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="col-sm-3">
						<?php echo Html::a('Thêm thời gian', '#', [
							'class' => 'btn btn-success margin-button',
							'id'    => 'add-form',
						]) ?>
					</div>
				</div>
				<div class="space-4"></div>
				<?php echo $form->field($model, 'product')->widget(Select2::className(), [
					'data'    => $model->getAvailableProduct(),
					'options' => [
						'prompt'   => 'Liên kết sản phẩm',
						'multiple' => true,
					],
				])->label('Liên kết sản phẩm') ?>
				<div class="space-4"></div>
				<?= $form->field($model, 'auto_comment')->checkbox()->label('Tự động trả lời comment') ?>

				<div class="space-4"></div>
				<?= $form->field($model, 'comment_content')->textarea([
					'placeholder' => 'Nội dung trả lời',
					'rows'        => 5,
				])->label('Nội dung trả lời') ?>

				<div class="space-4"></div>
				<?= $form->field($model, 'hidden_phone')->checkbox()->label('Ẩn comment có số điện thoại') ?>
				<div class="space-4"></div>

				<?= $form->field($model, 'hidden_all')->checkbox()->label('Ẩn tất cả comment') ?>
				<div class="space-4"></div>

				<?= $form->field($model, 'hidden_keyword')->checkbox()->label('Ẩn comment theo từ khóa') ?>
				<div class="space-4"></div>
				<?= $form->field($model, 'keyword')->textInput(['placeholder' => 'Từ khóa muốn ẩn'])->label(false) ?>
				<h3>Thông số KPI</h3>
				<?= $form->field($model, 'kpi_type')->dropDownList(PostForm::KPI_TYPE, [
					'prompt'     => 'Loại KPI',
					'data-range' => PostForm::TYPE_RANGE,
				])->label('KPI') ?>
				<div class="space-4"></div>
				<div class="date-time" style="display: none">
					<?php echo $form->field($model, 'from_date', [
						'template' => '<div class="col-sm-6 col-sm-offset-3"><i style="vertical-align: sub;" class="fa fa-calendar fa-2x" aria-hidden="true"></i><div style="display: inline-block">{input}</div></div>',
					])->widget(DateTimePicker::className(), [
						'options' => [
							'placeholder' => 'From date',
							'class'       => 'form-control',
						],
					]) ?>
					<?php echo $form->field($model, 'to_date', ['template' => '<div class="col-sm-6 col-sm-offset-3"><i style="vertical-align: sub;" class="fa fa-calendar fa-2x" aria-hidden="true"></i><div style="display: inline-block">{input}</div></div>'])->widget(DateTimePicker::className(), [
						'options' => [
							'placeholder' => 'To date',
							'class'       => 'form-control',
						],
					]) ?>
				</div>
				<?= $form->field($model, 'kpi_like', ['template' => '<div class="col-sm-offset-3  col-sm-6"><div style="display: inline-block">{input}</div>/like</div>'])->textInput([
					'class'       => 'form-control',
					'placeholder' => 'Like number',
				]) ?>
				<div class="space-4"></div>

				<?= $form->field($model, 'kpi_share', ['template' => '<div class="col-sm-offset-3 col-sm-6"><div style="display: inline-block">{input}</div>/share</div>'])->textInput([
					'class'       => 'form-control',
					'placeholder' => 'Share number',
				]) ?>
				<div class="space-4"></div>

				<?= $form->field($model, 'kpi_comment', ['template' => '<div class="col-sm-offset-3  col-sm-6"><div style="display: inline-block">{input}</div>/comment</div>'])->textInput([
					'class'       => 'form-control',
					'placeholder' => 'Comment number',
				]) ?>
			</div>
		</div>
		<div class="clearfix ">
			<?php echo Html::submitButton('<i class="icon-ok bigger-110"></i>Lưu', ['class' => 'btn btn-info']) ?>
		</div>

	</div><!-- ./span -->
	<?php ActiveForm::end(); ?>

</div><!-- ./row-fluid -->
