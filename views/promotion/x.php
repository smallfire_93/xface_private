<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Chương trình khuyến mại';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1>
        Chương trình khuyến mại:
        <small>
            <i class="icon-double-angle-right"></i>
            Tặng sản phẩm khi mua sản phẩm cùng loại
        </small>
    </h1>
</div>
<style>
    .promotionX {
        width: 840px;
        margin: 0 auto;
        min-height: 240px;
        overflow: hidden;
    }
    ._marginTop {
        margin-top: -20px;
    }
    .width_input {
        width: 110%;
    }
</style>
<div class="container" style="margin-top: 10px">
    <div class="panel panel-success">
        <div class="panel-heading">Chương trình khuyến mại tặng cùng sản phẩm</div>
        <div class="panel-body">
            <p>Tạo các chương trình khuyến mại cho khách hàng của bạn</p>
            <hr/>
            <div class="bor_promotion">
                <?php $form = ActiveForm::begin([
                    'options'=>['class'=>'form-horizontal promotionX']
                ])?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3">Tên chương trình tặng quà:</label>
                    <div class="col-sm-8">
                        <?= $form->field($model, 'name')->textInput(['class'=>'form-control width_input','id'=>'inputEmail3'])->label(false) ?>
                    </div>
                </div>
                <div class="form-group _marginTop">
                    <label for="inputEmail3" class="col-sm-3">Thời gian áp dụng:</label>
                    <label class="col-sm-1" style="margin-left: -10px">Từ: </label>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'start_time')->dropdownList(['1' => 'aaa', '2' => 'bbb'], ['prompt' => '---Select Data---'])->label(false) ?>
                    </div>
                    <label class="col-sm-1" style="margin-left: 70px">Đến: </label>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'start_time')->dropdownList(['1' => 'aaa', '2' => 'bbb'], ['prompt' => '---Select Data---'])->label(false) ?>
                    </div>
                </div>
                <div class="form-group _marginTop">
                    <label for="inputEmail3" class="col-sm-3" >Hình thức khuyến mại:</label>
                    <div class="col-sm-9">
                        <?= $form->field($model, 'promotion_type')->dropdownList(['1' => 'aaa', '2' => 'bbb'],['class'=>'col-sm-4'], ['prompt' => '---Select Data---'])->label(false) ?>
                    </div>
                </div>
                <div class="form-group _marginTop">
                    <label for="inputEmail3" class="col-sm-3">Sản phẩm tặng:</label>
                    <div class="col-sm-9">
                        <?= $form->field($model, 'product_id')->dropdownList(['1' => 'aaa', '2' => 'bbb'],['class'=>'col-sm-4'], ['prompt' => '---Select Data---'])->label(false) ?>
                    </div>
                </div>
                <div class="form-group _marginTop">
                    <label for="inputEmail3" class="col-sm-3">Số lượng mua:</label>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'product_quantity')->textInput(['class'=>'form-control'])->label(false) ?>
                    </div>
                    <label for="inputEmail3" class="col-sm-2" style="margin-left: 60px">Số lượng tặng:</label>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'promotion_quantity')->textInput(['class'=>'form-control'])->label(false) ?>
                    </div>
                </div>
                <div class="form-group _marginTop">
                    <label class="col-sm-3"></label>
                    <button class="col-sm-offset-9 col-sm-3 btn btn-success" type="button">
                        <i class="icon-ok bigger-110"></i>
                        Lưu
                    </button>
                </div>
            <?php ActiveForm::end() ?>

            </div>
        </div>
    </div>
</div>
