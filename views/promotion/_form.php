<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use phamxuanloc\jui\DateTimePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Promotion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-content">
    <div class="page-header">
        <h1>
            Chương trình khuyến mại:
<!--            <small>-->
<!--                <i class="icon-double-angle-right"></i>-->
<!--            </small>-->
        </h1>
    </div>
    <style>
        .promotionX {
            width: 840px;
            margin: 0 auto;
            min-height: 240px;
            overflow: hidden;
        }
        ._marginTop {
            margin-top: -20px;
        }
        .width_input {
            width: 110%;
        }
    </style>
    <div class="col-xs-12" style="margin-top: 20px">
        <div class="panel panel-success">

            <div class="panel-body">
                <div class="promotion-form">

                    <?php $form = ActiveForm::begin(
                        ['layout' => 'horizontal']
                    ); ?>
                    <div class="row">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Tên chương trình tặng quà:') ?>
                    </div>

                    <div class="row">
                        <?= $form->field($model, 'start_time')->widget(DateTimePicker::className(), [
                            'dateFormat' => 'yyyy-MM-dd',
                            'clientOptions' => [
                            ],
                            'options' => ['style' => 'width:200px',],
                        ]) ?>

                        <?= $form->field($model, 'end_time')->widget(DateTimePicker::className(), [
                            'dateFormat' => 'yyyy-MM-dd',
                            'clientOptions' => [
                            ],
                            'options' => ['style' => 'width:200px',],
                        ]) ?>

                    </div>

                    <div class="row">
                        <?php $arrType = [''=> '--Chọn--','1' => 'X', '2' => 'Y','3'=>'Z']; ?>
                        <?= $form->field($model, 'promotion_type')->dropDownList($arrType,['onchange' => 'leaveChange()'])->label('Hình thức khuyến mại:') ?>
                    </div>

                    <div id="hidden" style="display: none;">
                        <div class="row">
                            <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Product::find()->all(), 'id','name'),['prompt'=>'Chọn sản phẩm'])->label('Sản phẩm tặng') ?>
                            <?php ?>
                        </div>

                        <div class="row col-sm-offset-3 col-sm-6">
                            <?= $form->field($model, 'product_quantity')->textInput()->label('Số Lượng mua') ?>
                        </div>

                        <div class="row col-sm-offset-3 col-sm-6">
                            <?= $form->field($model, 'promotion_quantity')->textInput()->label('Số lượng tặng') ?>
                        </div>
                    </div>

                    <div id="hidden2" style="display: none">
                        <div class="row">
                            <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Product::find()->all(), 'id','name'),['prompt'=>'Chọn sản phẩm'])->label('Sản phẩm tặng') ?>
                            <?php ?>
                        </div>

                        <div class="row">
                            <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Product::find()->all(), 'id','name'),['prompt'=>'Chọn sản phẩm'])->label('Sản phẩm mua') ?>
                        </div>
                        <div class="row col-sm-offset-3 col-sm-6">
                            <?= $form->field($model, 'product_quantity')->textInput()->label('Số Lượng mua') ?>
                        </div>

                        <div class="row col-sm-offset-3 col-sm-6">
                            <?= $form->field($model, 'promotion_quantity')->textInput()->label('Số lượng tặng') ?>
                        </div>
                    </div>

                    <div id="hidden3" style="display: none;">
                        <div class="row">
                            <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Product::find()->all(), 'id','name'),['prompt'=>'Chọn sản phẩm'])->label('Sản phẩm giảm giá') ?>
                        </div>

                        <div class="row col-sm-offset-3 col-sm-6">
                            <?php $arrType = ['1' => 'VNĐ', '2' => '%']; ?>
                            <?= $form->field($model, 'discount_amount')->dropDownList($arrType)->label('Giảm giá') ?>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-offset-5 col-md-3">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function leaveChange(){
        var element = document.getElementById("promotion-promotion_type");
        if (element.value == 1) {
            var hiddenDiv = document.getElementById("hidden");
            hiddenDiv.style.display = "block";

        }else{
            var hiddenDiv = document.getElementById("hidden");
            hiddenDiv.style.display = "none";
        }
        if (element.value == 2) {

            var hiddenDiv = document.getElementById("hidden2");
            hiddenDiv.style.display = "block";
        }else{
            var hiddenDiv = document.getElementById("hidden2");
            hiddenDiv.style.display = "none";
        }
        if(element.value == 3){
            var hiddenDiv = document.getElementById("hidden3");
            hiddenDiv.style.display = "block";
        }else{
            var hiddenDiv = document.getElementById("hidden3");
            hiddenDiv.style.display = "none";
        }
    }
</script>
