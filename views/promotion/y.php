<style>
    .promotionX {
        width:850px;
        margin: 0 auto;
        min-height: 240px;
        overflow: hidden;
    }
    .bor_promotion {
        border: 1px solid grey;
        padding: 20px;
        background: #eee;
    }
</style>
<div class="container" style="margin-top: 10px">
    <div class="panel panel-success">
        <div class="panel-heading">Chương trình khuyến mại tặng khác loại sản phẩm</div>
        <div class="panel-body">
            <p>Tại các chương trình khuyến mại cho khách hàng của bạn</p>
            <hr/>

                <form class="form-horizontal promotionX">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Tên chương trình tặng quà:</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputEmail3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Thời gian áp dụng:</label>
                        <label class="col-sm-1 control-label">Từ: </label>
                        <div class="col-sm-3">
                            <!--<input type="email" class="form-control" id="inputEmail3" placeholder="Email">-->
                            <select>
                                <option>Ngày 01/02/2017</option>
                                <option>abc</option>
                            </select>
                        </div>
                        <label class="col-sm-1 control-label">Đến: </label>
                        <div class="col-sm-3">
                            <select>
                                <option>Ngày 01/05/2017</option>
                                <option>abc</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Hình thức khuyến mại:</label>
                        <div class="col-sm-8">
                            <select>
                                <option>Tặng khác sản phẩm</option>
                                <option>Tặng cùng sản phẩm</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Sản phẩm mua:</label>
                        <div class="col-sm-3">
                            <input type="email" class="form-control" id="inputEmail3">
                        </div>
                        <label for="inputEmail3" class="col-sm-2 control-label">Số lượng mua:</label>
                        <div class="col-sm-3">
                            <input type="email" class="form-control" id="inputEmail3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Sản phẩm tặng:</label>
                        <div class="col-sm-3">
                            <input type="email" class="form-control" id="inputEmail3">
                        </div>
                        <label for="inputEmail3" class="col-sm-2 control-label">Số lượng tặng:</label>
                        <div class="col-sm-3">
                            <input type="email" class="form-control" id="inputEmail3" >
                        </div>
                    </div>
                    <div class="form-group" style="margin-left: 100px">
                        <label class="col-sm-3"></label>
                        <button class="col-sm-offset-9 col-sm-3 btn btn-success" type="button">
                            <i class="icon-ok bigger-110"></i>
                            Lưu
                        </button>
                    </div>
                </form>

        </div>
    </div>
</div>
