<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PromotionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promotions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content">
    <div class="page-header">
        <h1>
            Khuyến mại
            <small>
                <i class="icon-double-angle-right"></i>
                Danh sách chương trình khuyến mại
            </small>
        </h1>
    </div><!-- /.page-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p style="margin-top: 20px">
        <?= Html::a('Thêm mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <div id="sample-table-2_wrapper" class="dataTables_wrapper" role="grid">
                    <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
//                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                [
                                    'header' => 'Tên chương trình',
                                    'value'  => 'name',
                                    'attribute'  => 'name'
                                ],
                                [
                                    'header' => 'Thời gian bắt đầu',
                                    'value'  => 'start_time',
                                    'attribute'  => 'start_time'
                                ],
                                [
                                    'header' => 'Thời gian kết thúc',
                                    'value'  => 'end_time',
                                    'attribute'  => 'end_time'
                                ],
                                [
                                    'header' => 'kiểu khuyến mãi',
                                    'value'  => 'promotion_type',
                                    'attribute'  => 'promotion_type'
                                ],
                                [
                                    'header' => 'Ngày tạo',
                                    'value'  => 'created_date',
                                    'attribute'  => 'created_date'
                                ],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
