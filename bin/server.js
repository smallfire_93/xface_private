/**
 * Created by luong on 03/30/2017.
 */
var http = require('http').Server();
var mysql = require('mysql');

var io = require('socket.io')(http).listen(5555);
var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'xface',
});

db.connect(function (err) {
    if (err) console.log(err);
    else console.log('Connect DB succes');
});

var isInitNodes = false;
var socketCount = 0;

io.sockets.on('connection', function (socket) {
    socketCount++;

    io.sockets.emit('users connected', socketCount);
    console.log(socketCount + 'connect');


    socket.on('disconnect', function () {
        socketCount--;
        io.sockets.emit('users connected', socketCount);
        console.log(socketCount + 'connect');
    });

    socket.on('news', function(data){
        var notes = [];
        db.query('select * from user').on('result', function (data) {
            notes.push(data)
        }).on('end', function () {
            socket.emit('initial notes', notes);
        });
    });

    // if (!isInitNodes) {
    //     db.query('select * from user').on('result', function (data) {
    //         notes.push(data)
    //     }).on('end', function () {
    //         socket.emit('initial notes', notes);
    //     });
    //     isInitNodes = true;
    // } else {
    //     socket.emit('initial notes', notes);
    // }
});