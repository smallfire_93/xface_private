<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 27-Mar-17
 * Time: 1:40 PM
 */
namespace app\components;

use app\models\Category;
use app\models\Fanpage;
use app\models\Post;
use app\models\Provider;
use app\models\User;
use Yii;
use yii\console\Application;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class Model extends ActiveRecord {

	/**@var User */
	public $user;

	/**
	 *  * Khởi tạo người dùng đã đăng nhập
	 */
	/**
	 * {@inheritDoc}
	 */
	public function __construct($config = []) {
		parent::__construct($config);
		if(!Yii::$app instanceof Application) {
			$this->user = Yii::$app->user->identity;
		}
	}

	public function uploadPicture($picture = '', $attribute) {
		// get the uploaded file instance. for multiple file uploads
		// the following data will return an array (you may need to use
		// getInstances method)
		$img = UploadedFile::getInstance($this, $attribute);
		// if no image was uploaded abort the upload
		if(empty($img)) {
			return false;
		}
		// generate a unique file name
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		if(!is_dir($dir)) {
			@mkdir($dir, 0777, true);
		}
		$ext            = $img->getExtension();
		$this->$picture = $this->getPrimaryKey() . '_' . "$picture" . ".{$ext}";
		// the uploaded image instance
		return $img;
	}

	public function getPictureUrl($picture = '') {
		Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/uploads/' . $this->tableName() . '/';
		$image                         = !empty($this->$picture) ? $this->$picture : Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif';
		clearstatcache();
		if(is_file(Yii::getAlias("@app/web") . '/uploads/' . $this->tableName() . '/' . $image)) {
			return Yii::$app->params['uploadUrl'] . $image;
		} else {
			return $image;
		}
	}

	public function getPictureFile($picture = '') {
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		return isset($this->$picture) ? $dir . $this->$picture : null;
	}

	public function getUserFanpage() {
		$fanpages = $this->user->fanpages;
		return $fanpages;
	}

	public function getCompanyFanpage() {
		if($this->user->id != 1) {
			$fanpage = $this->user->fanpageActive;
		} else {
			$fanpage = Fanpage::find()->where(['is_join' => Fanpage::JOINED])->all();
		}
		return $fanpage;
	}

	public function getCompanyProvider() {
		return $provider = Provider::find()->where(['company_id' => $this->user->company_id])->all();
	}

	public function getCompanyPost() {
		return $post = Fanpage::find()->where(['company_id' => $this->user->company_id])->joinWith('posts')->select('post.id')->all();
	}

	/**
	 *Trả về tất cả bài viết nằm trong fanpage mà người dùng hiện tại quản lý
	 */
	public function getAvailablePost() {
		$fanpages = $this->user->fanpageActive;
		$result   = [];
			foreach($fanpages as $fanpage) {
				$posts = $fanpage->posts;
				foreach($posts as $post) {
					$result[$post->id] = substr($post->content, 0, 20);
				}
			}
		return $result;
	}

	public function getCompanyCategory() {
		return Category::find()->where(['company_id' => $this->user->company_id])->all();
	}

	public function getCompanyProduct() {
		return $post = Fanpage::find()->joinWith('posts')->select('post.id')->all();
	}

	/**
	 *Hàm cắt chuỗi
	 */
	public function subtext($text, $num = 100) {
		if(strlen($text) <= $num) {
			return $text;
		}
		$text = substr($text, 0, $num);
		if($text[$num - 1] == ' ') {
			return trim($text) . "...";
		}
		$x  = explode(" ", $text);
		$sz = sizeof($x);
		if($sz <= 1) {
			return $text . "...";
		}
		$x[$sz - 1] = '';
		return trim(implode(" ", $x)) . "...";
	}
}