<?php
/**
 * Created by PhpStorm.
 * User: luong
 * Date: 03/02/2017
 * Time: 11:12 AM
 */
use yii\widgets\Menu;

?>

<?php
if (!Yii::$app->user->isGuest) {
    echo Menu::widget([
        'items' => [
            [
                'label' => 'Quản lý chung',
                'url' => ['/site/index'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-tachometer fa-lg"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Đơn hàng',
                'url' => '#',
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-shopping-cart fa-lg"></i> <span class="menu-text">{label}</span><b class="arrow fa fa-angle-down"></b></a>',
                'items' => [
                    [
                        'label' => 'Quản lý đơn hàng',
                        'url' => ['/order/list'],
                    ],
                    [
                        'label' => 'Chi tiết đơn hàng',
                        'url' => ['/order/view2'],
                    ],
                    [
                        'label' => 'Đơn hàng Tele Sale',
                        'url' => ['/order/view3'],
                    ],
                    [
                        'label' => 'Đơn hàng Kho vận',
                        'url' => ['/order/view4'],
                    ],
                    [
                        'label' => 'Đơn hàng CSKH',
                        'url' => ['/order/view5'],
                    ],
                ],
                'submenuTemplate' => '<ul class="submenu">{items}</ul>',
            ],
            [
                'label' => 'Bài viết',
                'url' => ['/post/index'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-picture-o fa-lg"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Sản phẩm',
                'url' => '#',
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-tags fa-lg"></i> <span class="menu-text">{label}</span><b class="arrow fa fa-angle-down"></b></a>',
                'items' => [
                    [
                        'label' => 'Nhóm sản phẩm',
                        'url' => ['/category'],
                    ],
                    [
                        'label' => 'Nhà cung cấp',
                        'url' => ['/provider'],
                    ],
                    [
                        'label' => 'Sản phẩm',
                        'url' => ['/product'],
                    ],
                ],
                'submenuTemplate' => '<ul class="submenu">{items}</ul>',
            ],
            [
                'label' => 'Khách hàng',
                'url' => ['customer/index'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-user fa-lg"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Vận chuyển',
                'url' => ['transport/index'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-truck fa-lg"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Nhân viên',
                'url' => ['user/index'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-users"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Báo cáo',
                'url' => '#',
                'submenuTemplate' => '<ul class="submenu">{items}</ul>',
                'items' => [
                    [
                        'label' => 'Báo cáo tổng quan',
                        'url' => ['/report/index'],
                    ],
                    [
                        'label' => 'Báo cáo comment',
                        'url' => ['/report-comment/index'],
                    ],
                    [
                        'label' => 'Báo cáo inbox',
                        'url' => ['/report-inbox/index'],
                    ],
                    [
                        'label' => 'Đơn hàng theo bài viết',
                        'url' => ['/report/report-orderpost'],
                    ],
                    [
                        'label' => 'Đơn hàng theo sản phẩm',
                        'url' => ['/report/report-orderproduct'],
                    ],
                    [
                        'label' => 'Báo cáo khách hàng',
                        'url' => ['/report/report-customer'],
                    ],
                    [
                        'label' => 'Báo cáo doanh số',
                        'url' => ['/report/report-revenue'],
                    ],
                    [
                        'label' => 'Báo cáo vận chuyển',
                        'url' => ['/report-shipping/index'],
                    ],
                    [
                        'label' => 'Báo cáo sản phẩm',
                        'url' => ['/report-product/index'],
                    ],
                    [
                        'label' => 'Báo cáo chốt sale',
                        'url' => ['/report-sale/index'],
                    ],
                ],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-bar-chart fa-lg"></i> <span class="menu-text">{label}</span></span><b class="arrow fa fa-angle-down"></b></a>',
            ],
            [
                'label' => 'Fanpage',
                'url' => '#',
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-facebook fa-lg"></i> <span class="menu-text">{label}</span><b class="arrow fa fa-angle-down"></b></a>',
                'items' => [
                    [
                        'label' => 'Chọn fanpage',
                        'url' => ['/fanpage/select-page'],
                    ],
                    [
                        'label' => 'Fanpage đã chọn',
                        'url' => ['/fanpage/fanpage'],
                    ],
                ],
                'submenuTemplate' => '<ul class="submenu">{items}</ul>',
            ],
            [
                'label' => 'Tương tác',
                'url' => ['/comment/contact'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-comments-o fa-lg"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Instagram',
                'url' => ['/site/report'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-instagram fa-lg"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Khuyến mại',
                'url' => ['promotion/index'],
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-gift fa-lg" aria-hidden="true"></i> <span class="menu-text">{label}</span></a>',
            ],
            [
                'label' => 'Cấu hình',
                'url' => '#',
                'template' => '<a href="{url}" class="dropdown-toggle"><i class="fa fa-cog fa-lg"></i> <span class="menu-text">{label}</span><b class="arrow fa fa-angle-down"></b></a>',
                'items' => [
                    [
                        'label' => 'Cấu hình chung',
                        'url' => ['//index'],
                    ],
                    [
                        'label' => 'Cấu hình comment',
                        'url' => ['page-config/comment'],
                    ],
                    [
                        'label' => 'Cấu hình inbox',
                        'url' => ['page-config/inbox'],
                    ],
                    [
                        'label' => 'Tài khoản',
                        'url' => ['/gateway/index'],
                    ],
                ],
                'submenuTemplate' => '<ul class="submenu">{items}</ul>',
            ],
        ],
        'options' => ['class' => 'nav nav-list'],
        'encodeLabels' => false,
        'activateParents' => true,
        'activeCssClass' => 'open active',
        'submenuTemplate' => '<ul class="submenu">{items}</ul>',
    ]);
}
?>