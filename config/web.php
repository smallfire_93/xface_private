<?php
$params = require(__DIR__ . '/params.php');
$config = [
	'id'         => 'basic',
	'basePath'   => dirname(__DIR__),
	'bootstrap'  => ['log'],
	'components' => [
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey'  => 'DAq_D2WZ6BpatzjhnegY9sp5cbndQa5-',
			'enableCsrfValidation' => false,
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer'       => [
			'class'            => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'warning',
					],
				],
			],
		],
		'db'           => require(__DIR__ . '/db.php'),
		/*
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
			],
		],
		*/
		'authClientCollection' => [
			'class'      => 'yii\authclient\Collection',
			'httpClient' => [
				'transport' => 'yii\httpclient\CurlTransport',
			],
			'clients'    => [
				'facebook' => [
					'class'          => 'yii\authclient\clients\Facebook',
					'clientId'       => '106939453186544',
					'clientSecret'   => '868d79544b3886b66efacd91b25e9941',
					'attributeNames' => [
						'name',
						'email',
						'picture',
						'id',
					],
					'scope'          => [
						'email',
						'publish_actions',
						'ads_management',
						'manage_pages',
						'publish_pages',
						'pages_show_list',
					],
				],
			],
		],
	],
	'modules'    => [
		'gridview' => [
			'class' => '\kartik\grid\Module'
			// enter optional module parameters below - only if you need to
			// use your own export download action or custom translation
			// message source
			// 'downloadAction' => 'gridview/export/download',
			// 'i18n' => []
		],
	],
	'params'     => $params,
];
if(YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	//    $config['bootstrap'][] = 'debug';
	//    $config['modules']['debug'] = [
	//        'class' => 'yii\debug\Module',
	//        // uncomment the following to add your IP if you are not connecting from localhost.
	//        //'allowedIPs' => ['127.0.0.1', '::1'],
	//    ];
	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}
return $config;
