$('#add-form').on('click', function() {
	var item    = $(".items");
	var number  = item.find(".content-items").length - 1;
	var context = $("<div class='content-items'><div class='item-detail col-sm-9 col-xs-9'>" + $(".item-detail").html() + "</div><div class='col-sm-3 col-xs-3 line-icon delete-form'><i class='fa fa-trash fa-lg' aria-hidden='true'></i></div></div>");
	context.find("input,select").val("");
	// var appendTxt = "<div class='content-items'><div class='item-detail col-sm-9'>" + $(".item-detail").html() + "</div></div>"
	// item.find('.content-items:last').after(appendTxt);
	context.appendTo(".items");
	var input = $('.items .content-items:last input');
	input.attr('name', 'PostForm[' + parseInt(number + 1) + '][schedule]');
	input.attr('id', 'postform-schedule-' + parseInt(number + 1));
	input.attr('class', 'border-form form-control datetime');
	input.datetimepicker();

});
//Do load xong trang rồi mới gọi datepicker nên khi add form cần thêm datepicker ngay bên trong phần addform
$(document).ready(function() {
	$('.datetime').datetimepicker();
});
$(document).ready(function() {
	$('.fa-calendar').datetimepicker();
});
$(document).on('click', '.delete-form', function() {
	var context = $(this).closest('.content-items');
	context.remove();
});
$(document).on('change', '#postform-kpi_type', function() {
	// alert($(this).data('range'));
	if($(this).val() == $(this).data('range')) {
		$('.date-time').show();
	} else {
		$('.date-time').hide();
	}
});