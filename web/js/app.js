jQuery(document).ready(function ($) {
    var id_comment = $('#hidden_parent_id').val();
    var getAllTag = $.ajax({
        type: "GET",
        url: "index.php?r=comment/get-all-tag",
        async: false
    }).responseText;

    var getTag = $.ajax({
        type: "GET",
        url: "index.php?r=comment/get-tag",
        data: {id_comment: id_comment},
        async: false
    }).responseText;

    var tags = $('#tags').inputTags({
        tags: $.parseJSON(getTag),
        autocomplete: {
            values: $.parseJSON(getAllTag),
        },
        init: function (elem) {
            $('span', '#events').text('init');
        },
        create: function (elem) {
            console.log('create ' + elem.tags);
            var tags = elem.tags;
            $.ajax({
                type: "POST",
                url: "index.php?r=comment/create-tag",
                data: {id_comment: id_comment, tags: elem.tags},
                success: function (data) {
                    console.log(data);
                    $('#fbTags').empty();
                    $('#fbTags').append(data);
                }
            });
        },
        update: function (elem) {
            console.log('update ' + elem.tags);
        },
        destroy: function (elem) {
            console.log('destroy ' + elem.tags);
            $.ajax({
                type: "POST",
                url: "index.php?r=comment/remove-tag",
                data: {id_comment: id_comment, tags: elem.tags},
                success: function (data) {
                    console.log(data);
                    $('#fbTags').empty();
                    $('#fbTags').append(data);
                }
            });
        },
        selected: function () {
            $('span', '#events').text('selected');
        },
        unselected: function () {
            $('span', '#events').text('unselected');
        },
        change: function (elem) {
            $('.results').empty().html('<strong>Tags:</strong> ' + elem.tags.join(' - '));
        },
        autocompleteTagSelect: function (elem) {
            console.log(elem.tags);
            // console.log(elem);
            console.info('autocompleteTagSelect');
        }
    });

    // $('#tags').inputTags('tags', 'flat', function(tags) {
    // $('.results').empty().html('<strong>Tags:</strong> ' + tags.join(' - '));
    // });

    // var autocomplete = $('#tags').inputTags('options', 'autocomplete');
    // $('span', '#autocomplete').text(autocomplete.values.join(', '));
});