$(document).on('click', '.delete-page', function() {
	if(confirm('Do you want to delete this fanpage?')) {
		$.ajax({
			type    : 'post',
			url     : $(this).data('href'),
			data    : {
				id: $(this).data("id")
			},
			dataType: "json",
			success : function(data) {
				location.reload()
			}
		});
		return false;
	}
});
$(document).on('change', '.post-type', function() {
	var type = $(this).val();
	$('.post-file').hide();

	if(type == 0) {
	}
	else if(type == 4) {
		$(".post-images").show();
	} else if(type == 3) {
		$('.post-url').show();
	} else if(type == 2) {
		$('.post-video').show();
	} else {
		$('.post-image').show();
	}
});
$(document).ready(function() {
	var type = $('.post-type').val();
	$('.post-file').hide();

	if(type == 0) {
	}
	else if(type == 4) {
		$(".post-images").show();
	} else if(type == 3) {
		$('.post-url').show();
	} else if(type == 2) {
		$('.post-video').show();
	} else {
		$('.post-image').show();
	}
});



