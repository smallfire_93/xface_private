/**
 * Created by luong on 03/28/2017.
 */

$(document).ready(function () {

    $('#create-order').validate({
        rules: {
            phone: {
                required: true
            },
        }
    });

    // $('#create-order input').on('keyup blur', function () {
    //     if ($('#create-order').valid()) {
    //         $('button.btn').prop('disabled', false);
    //     } else {
    //         $('button.btn').prop('disabled', 'disabled');
    //     }
    // });

    $(".direct-chat-messages").slimScroll({
        height: '410px',
        size: '5px',
        start: 'bottom'
    });
    $(".fb-chat-list").slimScroll({
        height: '465px',
        size: '5px',
        start: 'top'
    });
    $('#show_card').slimScroll({
        height: '180px',
    });
});

$(document).on('keypress', '#chatText', function () {
    if (event.keyCode == 13) {
        if ($('#chatText').val()) {
            $("#send_message").submit();
        }
        return false;
    }
});

$(document).on('submit', '#send_message', function (e) {
    NProgress.start();
    //Không cho reload lại trang
    e.preventDefault();
    var reply_message = $("#chatText").val();
    var conversation_id = $("#hidden_conversation_id").val();
    var object_id = $("#hidden_object_id_id").val();
    var hidden_post_id = $("#hidden_post_id").val();
    var hidden_parent_id = $("#hidden_parent_id").val();
    if (reply_message) {
        $.ajax({
            type: "POST",
            url: "index.php?r=comment/send-message",
            data: {
                content: reply_message,
                conversation_id: conversation_id,
                hidden_post_id: hidden_post_id,
                hidden_parent_id: hidden_parent_id
            },
            beforeSend: function () {
                $('.wait_load_messages').show();
            },
            success: function (data) {
                alert(data);
                show_detail_conversations(conversation_id, 1, object_id);
                $(function () {
                    $('.direct-chat-messages').slimScroll({
                        start: 'bottom'
                    });
                });
            }
        }).always(function () {
            $('.wait_load_messages').hide();
        }).done(function () {
            NProgress.done();
        });
        $('#chatText').val("");
        return false;
    }
    else {
        $('#write_content').text('Bạn chưa nhập nội dung');
        return false;
    }
});

$(document).on('submit', '#create-order', function (e) {
    NProgress.start();
    //Không cho reload lại trang
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: "index.php?r=order/create",
        data: $("#create-order").serialize(),
        beforeSend: function () {
            // $('.wait_load_messages').show();
        },
        success: function (data) {
            $("#show_card").load("index.php?r=order/load-cart-active");
            $("#total_money").load("index.php?r=order/total-money");
            NProgress.done();
            console.log(data);
            // show_detail_conversations(conversation_id, 1, object_id);
            // $(function () {
            //     $('.direct-chat-messages').slimScroll({
            //         start: 'bottom'
            //     });
            // });
        }
    });
});

function delete_comment(comment_id) {
    NProgress.start();
    var conversation_id = $("#hidden_conversation_id").val();
    var object_id = $("#hidden_object_id_id").val();
    $.ajax({
        method: "POST",
        url: "index.php?r=comment/delete-comment",
        data: {comment_id: comment_id},
        success: function (data) {
            show_detail_conversations(conversation_id, 1, object_id);
            NProgress.done();
        },
        error: function (XMLHttpRequest, textStatus) {
            if (XMLHttpRequest.status == 403 || XMLHttpRequest.status == 404) {
                window.location = encodeURI('/');
            }
        }
    });
}

function hidden_comment(comment_id) {
    NProgress.start();
    var conversation_id = $("#hidden_conversation_id").val();
    var object_id = $("#hidden_object_id_id").val();
    $.ajax({
        method: "POST",
        url: "index.php?r=comment/hidden-comment",
        data: {comment_id: comment_id},
        success: function (data) {
            show_detail_conversations(conversation_id, 1, object_id);
            NProgress.done();
        },
        error: function (XMLHttpRequest, textStatus) {
            if (XMLHttpRequest.status == 403 || XMLHttpRequest.status == 404) {
                window.location = encodeURI('/');
            }
        }
    });
}

function show_comment(comment_id) {
    NProgress.start();
    var conversation_id = $("#hidden_conversation_id").val();
    var object_id = $("#hidden_object_id_id").val();
    $.ajax({
        method: "POST",
        url: "index.php?r=comment/show-comment",
        data: {comment_id: comment_id},
        success: function (data) {
            show_detail_conversations(conversation_id, 1, object_id);
            NProgress.done();
        },
        error: function (XMLHttpRequest, textStatus) {
            if (XMLHttpRequest.status == 403 || XMLHttpRequest.status == 404) {
                window.location = encodeURI('/');
            }
        }
    });
}

function connect_fanpage(page_id) {
    NProgress.start();
    $.ajax({
        method: "POST",
        url: "index.php?r=comment/get-fanpage",
        // dataType : "json",
        data: {page_id: page_id},
        success: function (data) {
            if (data) {
                alert(data);
                // window.location.reload();
            }
            else {
                window.location.reload();
            }
            NProgress.done();
        },
        error: function (XMLHttpRequest, textStatus) {
            if (XMLHttpRequest.status == 403 || XMLHttpRequest.status == 404) {
                window.location = encodeURI('/');
            }
        }
    });
}

function show_detail_conversations(conversation_id, check_flag, object_id) {
    $('#please_select_inbox').hide();
    NProgress.start();
    if (check_flag == 0) {
        $('#detail_inbox').empty();
        $('#detail_inbox').show();
    }
    var totalHeight = 0;
    //Call ajax get content conversations
    $.ajax({
        method: "GET",
        url: "index.php?r=comment/conversation",
        // dataType : "json",
        data: {object_id: object_id, conversation_id: conversation_id, check_flag: check_flag},

        beforeSend: function () {
            if (check_flag == 0) {
                $("#max_loader").show();
            }
        },
        success: function (data) {
            $('#detail_inbox').empty();
            $('#detail_inbox').append(data);
            $("#card").show();
            $("#show_card").load("index.php?r=order/load-cart-active");
            $("#total_money").load("index.php?r=order/total-money");
            //slimscroll
            $(function () {
                $('.direct-chat-messages').slimScroll({
                    height: '370px',
                    size: '5px',
                    start: 'bottom'
                });
                $('#show_card').slimScroll({
                    height: '180px',
                });
            });
        },
        complete: function () {
            $("#max_loader").hide();
        }
    }).done(function (data) {
        NProgress.done();
        /*Load converstion mới về neu co*/
    }).fail(function (jqXHR, textStatus) {

    });
}

$(document).on('click', '.fb-chat-list li.item', function (e) {
    $(".fb-chat-list li.item").removeClass('item-active');
    $(this).addClass('item-active');
});

function select_product() {
    var active = document.getElementById('show_shopping');
    if(active.classList.contains('active') == true){
        $('#show_shopping').removeClass('active');
    }else{
        $('#show_shopping').addClass('active');
    }
}

function add_card(product_id) {
    NProgress.start();
    $.ajax({
        type: "POST",
        url: "index.php?r=order/add",
        data: {product_id : product_id}, // serializes the form's elements.
        success: function (data) {
            $("#show_card").load("index.php?r=order/load-cart-active");
            $('#show_shopping').removeClass('active');
            $("#total_money").load("index.php?r=order/total-money");
            NProgress.done();
        }
    });
}

function remove_card(id) {
    NProgress.start();
    $.ajax({
        type: "POST",
        url: "index.php?r=order/remove",
        data: {id : id}, // serializes the form's elements.
        success: function (data) {
            $("#show_card").load("index.php?r=order/load-cart-active");
            $("#total_money").load("index.php?r=order/total-money");
            NProgress.done();
        }
    });
}