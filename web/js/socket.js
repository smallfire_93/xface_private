/**
 * Created by luong on 03/31/2017.
 */
$(document).ready(function () {
    // Connect to our node/websockets server
    var socket = io.connect('http://localhost:5555');

    // Initial set of notes, loop through and add to list
    socket.on('initial notes', function (data) {
        var html = '';
        var pageId = $("#hidden_page_id").val();
        for (var i = 0; i < data.length; i++) {
            // We store html as a var then add to DOM after for efficiency
            if(pageId == data[i].fb_user_id) {
                html += '<div class="direct-chat-msg row right">' +
                    '<img class="direct-chat-img" src="https://graph.facebook.com/'+data[i].fb_user_id+'/picture?type=large" alt="">' +
                    '<div class="direct-chat-text-w">' +
                    '<div class="direct-chat-text">' +
                    '<p>' + data[i].content + '</p>' +
                    '</div>' +
                    '</div>' +
                    '<div class="direct-chat-info clearfix">' +
                    '<span class="direct-chat-timestamp">'+data[i].created_date+'</span>' +
                    '</div>' +
                    '</div>'
            }else{
                html += '<div class="direct-chat-msg row">' +
                    '<img class="direct-chat-img" src="https://graph.facebook.com/'+data[i].fb_user_id+'/picture?type=large" alt="">' +
                    '<div class="direct-chat-text-w">' +
                    '<div class="direct-chat-text">' +
                    '<p>' + data[i].content + '</p>' +
                    '</div>' +
                    '</div>' +
                    '<div class="direct-chat-info clearfix">' +
                    '<span class="direct-chat-timestamp">'+data[i].created_date+'</span>' +
                    '</div>' +
                    '</div>'
            }
        }
        if(html != ""){
            alert('sdasdas');
            $(function () {
                $('.direct-chat-messages').slimScroll({
                    start: 'bottom'
                });
            });
        }
        $('#loadConversation').append(html);
    });

    // New socket connected, display new count on page
    socket.on('users connected', function (data) {
        $('#usersConnected').html('Users connected: ' + data)
    });

    setInterval(function(){
        // var object_id = $("#hidden_object_id_id").val();
        // var post_id = $("#hidden_post_id").val();
        var parent_id = $("#hidden_parent_id").val();
        socket.emit('news', {parent_id: parent_id});
    }, 2000);
});